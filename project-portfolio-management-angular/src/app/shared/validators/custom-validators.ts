import { Injectable } from '@angular/core';
import { AbstractControl, FormGroup, ValidationErrors, ValidatorFn } from '@angular/forms';

@Injectable({ providedIn: 'root' })
export class CustomValidators {
  public PasswordMustMatch(formGroup: AbstractControl): ValidationErrors | null {
    const passwordControl = formGroup.get('password');
    const passwordRepeatControl = formGroup.get('passwordRepeat');

    if (!passwordControl?.touched || !passwordRepeatControl?.touched)
      return null;

    if (passwordControl.value === passwordRepeatControl.value)
      return null;

    return { passwordsMustMatch: true };
  }

  private rolesValidos: string[] = [
    'DESARROLLADOR',
    'RRHH',
    'MANAGER',
    'ADMINISTRADOR'
  ];
  public esRolValido(formGroup: AbstractControl): ValidationErrors | null {
    const rolControl = formGroup.get('rol');

    if (!rolControl?.touched) {
      return null;
    }

    const rol = rolControl.value;
    if (this.rolesValidos.indexOf(rol) >= 0) {
      return null;
    }

    return { esRolValido: true };
  }
}
