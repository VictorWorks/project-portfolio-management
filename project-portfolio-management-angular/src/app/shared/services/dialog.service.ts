import { Injectable } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { Observable } from 'rxjs';

import { ConfirmComponent } from '../components/confirm/confirm.component';
import { MessageBoxComponent } from '../components/message-box/message-box.component';

@Injectable({
  providedIn: 'root'
})
export class DialogService {

  constructor(private confirmDialog: MatDialog) { }

  abrirConfirmacion(mensaje: string):Observable<any>{
    const config = new MatDialogConfig();
    config.width = '390px';
    config.disableClose = true;
    config.panelClass = 'confirm-dialog-container';
    config.data = {message: mensaje};
    config.position= {top: '10px;'}
    return this.confirmDialog.open(ConfirmComponent, config).afterClosed();
  }

  abrirMensaje(mensaje: string): Observable<any> {
    const config = new MatDialogConfig();
    config.width = '390px';
    config.disableClose = true;
    config.panelClass = 'confirm-dialog-container';
    config.data = {message: mensaje};
    config.position= {top: '10px;'}
    return this.confirmDialog.open(MessageBoxComponent, config).afterClosed();
  }

}
