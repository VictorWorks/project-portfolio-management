import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { UrlQuery } from '../model/url-query';

@Injectable({
  providedIn: 'root'
})
export class CommunicationService {
  constructor(private http: HttpClient) { }

  private httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  }

  private obtenerUrlCompleta(baseUrl: string, query: UrlQuery): string {
    return baseUrl + '/' + query.toString();
  }

  public get<T>(url: string, query: UrlQuery): Observable<T> {
    const fullUrl = this.obtenerUrlCompleta(url, query);
    return this.http.get<T>(fullUrl);
  }

  public getAll<T>(url: string, query?: UrlQuery): Observable<T[]> {
    let fullUrl = url;
    if (query !== undefined)
      fullUrl = this.obtenerUrlCompleta(url, query);
    return this.http.get<T[]>(fullUrl);
  }

  public save<T>(url: string, elem: T): Observable<T> {
    return this.http.post<T>(url, elem, this.httpOptions);
  }

  public update<T>(url: string, query: UrlQuery, elem: T): Observable<T> {
    const fullUrl = this.obtenerUrlCompleta(url, query);
    return this.http.put<T>(fullUrl, elem, this.httpOptions);
  }

  public delete<T>(url: string, query: UrlQuery): Observable<T> {
    const fullUrl = this.obtenerUrlCompleta(url, query);
    return this.http.delete<T>(fullUrl);
  }

  public postWithResponse<T>(url: string, elem: T): Observable<HttpResponse<T>>{
    return this.http.post<T>(url, elem, {observe: 'response'});
  }

}
