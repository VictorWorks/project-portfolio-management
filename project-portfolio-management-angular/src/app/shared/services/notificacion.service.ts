import { Injectable } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})
export class NotificacionService {

  constructor(private snackBar: MatSnackBar) { }

  public notificarRespuestaError(respuesta: HttpErrorResponse): void {
    let mensaje = respuesta.message;
    if (respuesta.error != null && respuesta.error.message) {
      mensaje = respuesta.error.message;
    }
    this.error(mensaje);
  }

  public error(mensaje: string): void {
    const estilo: string = 'notificacion-error';
    this.notificar(mensaje, estilo);
  }

  public warning(mensaje: string): void {
    this.notificar(mensaje, 'notificacion-warning');
  }

  public info(mensaje: string): void {
    this.notificar(mensaje, 'notificacion-info');
  }

  private notificar(mensaje: string, estilo: string): void {
    const conf: MatSnackBarConfig<any> = {
      duration: 2000,
      verticalPosition: 'top',
      horizontalPosition: 'end',
      panelClass: `${estilo}`
    };
    this.snackBar.open(mensaje, undefined, conf);
  }

}
