import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { Subscription } from 'rxjs';
import { Persona } from 'src/app/personal/model/persona';
import { AutenticacionService } from 'src/app/seguridad/services/autenticacion.service';


@Component({
  selector: 'app-side-nav',
  templateUrl: './side-nav.component.html',
  styleUrls: ['./side-nav.component.css']
})
export class SideNavComponent implements OnInit, OnDestroy {

  mostrarAtras: boolean = false;

  @Input() titulo: string = '';
  @Input() mostrarBotonVolver: boolean = false;

  @Output() onVolverPulsado: EventEmitter<undefined> = new EventEmitter();

  constructor(private autenticacionService: AutenticacionService) { }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
  }

  public logout(): void {
    this.autenticacionService.logout();
  }

  public get puedeVerProyectos(): boolean {
    return this.autenticacionService.puedeVerProyecto;
  }

  public get puedeVerPersonal(): boolean {
    return this.autenticacionService.puedeVerUsuarios;
  }

  public volver(): void {
    this.onVolverPulsado.emit();
  }

  public get nombreUsuario() {
    const usuario = this.autenticacionService.obtenerUsuarioRegistrado();
    if (usuario) {
      return usuario.nombre;
    }

    return '';
  }
}
