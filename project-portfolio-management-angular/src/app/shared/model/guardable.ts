export class Guardable<T> {
  constructor(public readonly esNuevo: boolean, public readonly datos: T) {}
}
