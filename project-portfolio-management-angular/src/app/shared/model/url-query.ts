export class UrlQuery {
  readonly idElemento: string | undefined;
  private paresConsulta: any[] | undefined;

  constructor(id?: string) {
    if (id !== undefined)
      this.idElemento = id;
  }

  consultarRecurso(nombreRecurso: string, idRecurso?: string): UrlQuery {
    const elementoConsulta = {
      nombre: nombreRecurso,
      id: idRecurso
    };
    if (this.paresConsulta == undefined)
        this.paresConsulta = [];
    this.paresConsulta.push(elementoConsulta);
    return this;
  }

  toString(): string {
    let result = '';

    if (this.idElemento)
      result = result + this.idElemento;

    if (this.paresConsulta) {
      for (const par of this.paresConsulta) {
        if (par.nombre !== undefined)
            result = result + '/' + par.nombre;
        if (par.id !== undefined)
            result = result + '/' + par.id;
      }
    }

    return result;
  }
}
