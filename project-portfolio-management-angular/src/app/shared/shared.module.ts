import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { ConfirmComponent } from './components/confirm/confirm.component';
import { MaterialModule } from '../material/material.module';
import { MessageBoxComponent } from './components/message-box/message-box.component';
import { NotFoundComponent } from './pages/not-found/not-found.component';
import { PanelPrincipalComponent } from './pages/panel-principal/panel-principal.component';
import { SideNavComponent } from './components/side-nav/side-nav.component';
import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    ConfirmComponent,
    MessageBoxComponent,
    NotFoundComponent,
    PanelPrincipalComponent,
    SideNavComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    HttpClientModule,
    MaterialModule,
    RouterModule
  ],
  exports: [
    NotFoundComponent,
    PanelPrincipalComponent,
    SideNavComponent
  ]
})
export class SharedModule { }
