import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MaterialModule } from '../material/material.module';
import { ProyectoRoutingModule } from './proyecto-routing.module';
import { SharedModule } from '../shared/shared.module';
import { TaskboardModule } from '../taskboard/taskboard.module';

import { BacklogEditorComponent } from './components/backlog-editor/backlog-editor.component';
import { BacklogListComponent } from './components/backlog-list/backlog-list.component';
import { EquipoProyectoListComponent } from './components/equipo-proyecto-list/equipo-proyecto-list.component';
import { GraficaBurndownComponent } from './components/grafica-burndown/grafica-burndown.component';
import { GraficaBurnupComponent } from './components/grafica-burnup/grafica-burnup.component';
import { HistoriasSprintListComponent } from './components/historias-sprint-list/historias-sprint-list.component';
import { ListaProyectosComponent } from './pages/lista-proyectos/lista-proyectos.component';
import { PanelProyectoComponent } from './pages/panel-proyecto/panel-proyecto.component';
import { PanelSprintComponent } from './pages/panel-sprint/panel-sprint.component';
import { ProyectoEditorComponent } from './components/proyecto-editor/proyecto-editor.component';
import { ProyectoListComponent } from './components/proyecto-list/proyecto-list.component';
import { SprintEditorComponent } from './components/sprint-editor/sprint-editor.component';
import { SprintListComponent } from './components/sprint-list/sprint-list.component';


@NgModule({
  declarations: [
    BacklogEditorComponent,
    BacklogListComponent,
    EquipoProyectoListComponent,
    GraficaBurndownComponent,
    GraficaBurnupComponent,
    HistoriasSprintListComponent,
    ListaProyectosComponent,
    PanelProyectoComponent,
    PanelSprintComponent,
    ProyectoEditorComponent,
    ProyectoListComponent,
    SprintEditorComponent,
    SprintListComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    MaterialModule,
    ProyectoRoutingModule,
    ReactiveFormsModule,
    SharedModule,
    TaskboardModule
    ],
  exports: [
    ListaProyectosComponent,
    PanelProyectoComponent,
    PanelSprintComponent
  ]
})
export class ProyectoModule { }
