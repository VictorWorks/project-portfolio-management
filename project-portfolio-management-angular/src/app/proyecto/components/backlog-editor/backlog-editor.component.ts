import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { Guardable } from 'src/app/shared/model/guardable';
import { HistoriaUsuario } from '../../model/historia-usuario';

@Component({
  selector: 'app-backlog-editor',
  templateUrl: './backlog-editor.component.html',
  styleUrls: ['./backlog-editor.component.css']
})
export class BacklogEditorComponent implements OnInit {

  @Output() eventoHistoriaUsuarioBorrada = new EventEmitter<Guardable<HistoriaUsuario>>();

  decimalNumberFormat: string = '^\d+\.\d{0,1}$';
  form = new FormGroup({
    nombre: new FormControl('', Validators.required),
    descripcion: new FormControl('', []),
    estimacion: new FormControl('', [Validators.pattern('[0-9]+')]),
    prioridad: new FormControl('', [Validators.pattern('[0-9]+')])
  });
  constructor(
    private dialogRef: MatDialogRef<BacklogEditorComponent>,
    @Inject(MAT_DIALOG_DATA) public data: HistoriaUsuario
  ) {
    this.inicializarDatosFormulario();
    if (data) {
      this.form.reset(data);
    }
  }

  ngOnInit(): void { }

  limpiarForm(): void {
    this.form.reset();
  }

  inicializarDatosFormulario(): void {
    this.form.reset({
      nombre: '',
      descripcion: '',
      estimacion: '',
      prioridad: ''
    });
  }

  enviarDatos(): void {
    if (!this.formularioTieneDatosValidos()) return;

    const datosAGuardar = this.data ? this.data : new HistoriaUsuario();
    this.actualizarDatosHistoria(datosAGuardar);
    const historiaGuardable = new Guardable<HistoriaUsuario>(!this.data, datosAGuardar);
    this.eventoHistoriaUsuarioBorrada.emit(historiaGuardable);
    this.cerrarPopUp();
  }

  private formularioTieneDatosValidos(): boolean {
    return this.form.valid;
  }

  actualizarDatosHistoria(historia: HistoriaUsuario): void {
    historia.nombre = this.form.get('nombre')?.value;
    historia.descripcion = this.form.get('descripcion')?.value;
    historia.estimacion = this.obtenerEstimacionHistoria();
    historia.estadoActual = this.obtenerEstadoHistoria();
    if (!historia.fechaAlta) {
      historia.fechaAlta = this.obtenerFechaAlta();
    }
    historia.prioridad = this.form.get('prioridad')?.value;
    if (!historia.prioridad) {
      historia.prioridad = 100;
    }
  }

  private obtenerEstimacionHistoria(): number {
    let estimacion = this.form.get('estimacion')?.value;
    if (!estimacion) {
      estimacion = 10;
    }
    return estimacion;
  }

  private obtenerEstadoHistoria(): string {
    if (this.data?.estadoActual) {
      return this.data?.estadoActual;
    }
    return '';
  }

  obtenerFechaAlta(): string {
    const hoy = new Date();
    return hoy.toISOString().slice(0, 10);
  }

  cerrarPopUp(): void {
    this.dialogRef.close();
  }
}
