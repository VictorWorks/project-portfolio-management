import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BacklogEditorComponent } from './backlog-editor.component';

describe('BacklogEditorComponent', () => {
  let component: BacklogEditorComponent;
  let fixture: ComponentFixture<BacklogEditorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BacklogEditorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BacklogEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
