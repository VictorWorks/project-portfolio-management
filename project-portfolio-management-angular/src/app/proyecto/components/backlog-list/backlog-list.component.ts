import { AfterViewInit, Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Observable, of, Subscription } from 'rxjs';


import { AutenticacionService } from 'src/app/seguridad/services/autenticacion.service';
import { BacklogEditorComponent } from '../backlog-editor/backlog-editor.component';
import { DialogService } from 'src/app/shared/services/dialog.service';
import { Guardable } from 'src/app/shared/model/guardable';
import { HistoriaUsuario } from '../../model/historia-usuario';

@Component({
  selector: 'app-backlog-list',
  templateUrl: './backlog-list.component.html',
  styleUrls: ['./backlog-list.component.css']
})
export class BacklogListComponent implements OnInit, AfterViewInit, OnDestroy {

  @Input('listaHistoriasUsuario') listaHistoriasObservable: Observable<HistoriaUsuario[]> = of([]);
  @Output('onHistoriaBorrada') eventoHistoriaUsuarioBorrada = new EventEmitter<string>();
  @Output('onHistoriaGuardada') eventoHistoriaUsuarioGuardada = new EventEmitter<Guardable<HistoriaUsuario>>();

  @ViewChild(MatSort) sort: MatSort | null = null;
  dataSource!: MatTableDataSource<HistoriaUsuario>;
  clave: string = '';

  displayedColumns: string[] = [];
  private defaultColumns: string[] = ['nombre', 'estimacion', 'prioridad'];
  private actionsColumn: string = 'actions';


  subscription: Subscription | null = null ;

  constructor(
    private popup: MatDialog,
    private dialogService: DialogService,
    private autenticacionService: AutenticacionService
  ) { }

  ngOnInit(): void {
    this.subscription = this.listaHistoriasObservable.subscribe((lista) => {
      this.inicializarDataSource(lista)
    });

    if (this.puedeModificarProyecto) {
      this.displayedColumns = [...this.defaultColumns, this.actionsColumn];
    } else {
      this.displayedColumns = [...this.defaultColumns];
    }
  }

  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
  }

  ngOnDestroy(): void {
    this.subscription?.unsubscribe();
  }

  inicializarDataSource(datos: HistoriaUsuario[]): void {
    this.dataSource = new MatTableDataSource(datos);
    this.dataSource.sort = this.sort
    this.dataSource.filterPredicate = (data, filter) => {
      return this.displayedColumns.some(ele => {
        const dict = data as any;
        const value: string = dict[ele].toString().trim().toLowerCase();
        return ele != 'actions' && value.indexOf(filter) != -1;
      });
    }
  }

  filtrarEntradas(): void {
    this.dataSource.filter = this.clave.trim().toLowerCase();
  }

  limpiarFiltro(): void {
    this.clave = '';
    this.filtrarEntradas();
  }

  borrarEntrada(id: string): void {
    this.dialogService
      .abrirConfirmacion('Se va a borrar un registro. ¿Está seguro?')
      .subscribe((confirmacion) => {
        if (confirmacion) {
          this.eventoHistoriaUsuarioBorrada.emit(id);
        }
      });
  }

  nuevo(): void {
    this.lanzarFormulario(null);
  }

  editarEntrada(row: any): void {
    this.lanzarFormulario(row);
  }
  lanzarFormulario(dato: any): void {
    const config = new MatDialogConfig();
    config.disableClose = true;
    config.autoFocus = true;
    config.width = '60%';
    if (dato != null) config.data = dato;
    const ref = this.popup.open(BacklogEditorComponent, config);
    ref.componentInstance.eventoHistoriaUsuarioBorrada.subscribe((h) => this.eventoHistoriaUsuarioGuardada.emit(h));
    ref.afterClosed().subscribe((result) => {
    });
  }

  public get puedeModificarProyecto(): boolean {
    return this.autenticacionService.puedeAñadirProyecto;
  }

  public get puedeVerProyecto(): boolean {
    return this.autenticacionService.puedeVerProyecto;
  }

}
