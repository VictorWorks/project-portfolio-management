import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';

import { Observable, of, Subscription } from 'rxjs';
import { AutenticacionService } from 'src/app/seguridad/services/autenticacion.service';
import { Guardable } from 'src/app/shared/model/guardable';
import { DialogService } from 'src/app/shared/services/dialog.service';
import { Sprint } from '../../model/sprint';
import { SprintEditorComponent } from '../sprint-editor/sprint-editor.component';

@Component({
  selector: 'app-sprint-list',
  templateUrl: './sprint-list.component.html',
  styleUrls: ['./sprint-list.component.css']
})
export class SprintListComponent implements OnInit {

  @Input() listaSprints: Observable<Sprint[]> = of([]);
  @Output('onSprintBorrado') eventoBorrarSprint = new EventEmitter<string>();
  @Output('onSprintGuardado') eventoGuardarSprint = new EventEmitter<Guardable<Sprint>>();
  @Output('onSprintSeleccionado') eventoSeleccionarSprint = new EventEmitter<string>();

  @ViewChild(MatSort) sort: MatSort | null = null;
  displayedColumns = ['nombre', 'fechaInicio', 'fechaFin', 'actions'];
  dataSource!: MatTableDataSource<Sprint>;
  clave: string = '';

  subscriptionListaSprints: Subscription | null = null;

  constructor(
    private popup: MatDialog,
    private dialogService: DialogService,
    private autenticacionService: AutenticacionService
  ) { }

  ngOnInit(): void {
    this.subscriptionListaSprints = this.listaSprints.subscribe((datos) => this.inicializarDataSource(datos));
  }

  ngOnDestroy(): void {
    this.subscriptionListaSprints?.unsubscribe();
  }

  ngAfterViewInit(): void {
    if (this.dataSource !== undefined) this.dataSource.sort = this.sort;
  }

  inicializarDataSource(datos: Sprint[]): void {
    this.dataSource = new MatTableDataSource(datos);
    this.dataSource.sort = this.sort;
    this.dataSource.filterPredicate = (data, filter) => {
      const dict = data as any;
      return this.displayedColumns.some((ele) => {
        return (
          ele != 'actions' && dict[ele].toLowerCase().indexOf(filter) != -1
        );
      });
    };
  }

  filtrarEntradas(): void {
    this.dataSource.filter = this.clave.trim().toLowerCase();
  }

  limpiarFiltro(): void {
    this.clave = '';
    this.filtrarEntradas();
  }

  borrarEntrada(id: string): void {
    this.dialogService
      .abrirConfirmacion('Se va a borrar un registro. ¿Está seguro?')
      .subscribe((confirmacion) => {
        if (confirmacion) {
          this.eventoBorrarSprint.emit(id);
        }
      });
  }

  nuevo(): void {
    this.lanzarFormulario(null);
  }

  verEntrada(id: string): void {
    this.eventoSeleccionarSprint.emit(id);
  }

  editarEntrada(row: any): void {
    this.lanzarFormulario(row);
  }
  lanzarFormulario(dato: Sprint | null): void {
    const config = new MatDialogConfig();
    config.disableClose = true;
    config.autoFocus = true;
    config.width = '60%';
    if (dato != null) config.data = dato;
    const ref = this.popup.open(SprintEditorComponent, config);
    ref.componentInstance.eventoGuardarSprint.subscribe((dato) => this.eventoGuardarSprint.emit(dato));
    ref.afterClosed().subscribe((result) => { });
  }

  public get puedeVerProyecto(): boolean {
    return this.autenticacionService.puedeVerProyecto;
  }

  public get puedeModificarProyecto(): boolean {
    return this.autenticacionService.puedeAñadirProyecto;
  }

}
