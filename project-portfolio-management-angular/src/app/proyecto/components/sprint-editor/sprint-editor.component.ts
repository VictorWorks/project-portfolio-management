import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { DateAdapter } from '@angular/material/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Guardable } from 'src/app/shared/model/guardable';
import { Sprint } from '../../model/sprint';

@Component({
  selector: 'app-sprint-editor',
  templateUrl: './sprint-editor.component.html',
  styleUrls: ['./sprint-editor.component.css']
})
export class SprintEditorComponent implements OnInit {

  @Output() eventoGuardarSprint = new EventEmitter<Guardable<Sprint>>();
  form!: FormGroup;

  constructor(
    private dateAdapter: DateAdapter<Date>,
    private dialogRef: MatDialogRef<SprintEditorComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Sprint
  ) {
    this.crearFormulario();
    this.inicializarDatosFormulario();
    if (data != null) this.form.reset(data);
  }

  ngOnInit(): void { }

  crearFormulario(): void {
    this.dateAdapter.setLocale('es-ES');
    const hoy = new Date();
    const quincenaDesdeHoy = new Date(hoy.valueOf());
    quincenaDesdeHoy.setDate(quincenaDesdeHoy.getDate() + 15);

    this.form = new FormGroup({
      nombre: new FormControl('', Validators.required),
      fechaInicio: new FormControl(hoy),
      fechaFin: new FormControl(quincenaDesdeHoy),
    });
  }

  inicializarDatosFormulario(): void {
    const hoy = new Date();
    const quincenaDesdeHoy = new Date(hoy.valueOf());
    quincenaDesdeHoy.setDate(quincenaDesdeHoy.getDate() + 15);

    this.form.reset({
      nombre: '',
      fechaInicio: hoy,
      fechaFin: quincenaDesdeHoy,
    });
  }


  cerrarPopUp(): void {
    this.dialogRef.close();
  }

  enviarDatos(): void {
    if (!this.formularioTieneDatosValidos()) return;

    const datosAGuardar = this.data ? this.data : new Sprint();
    this.actualizarDatosSprint(datosAGuardar);

    const sprintAGuardar = new Guardable(!this.data, datosAGuardar);
    this.eventoGuardarSprint.emit(sprintAGuardar);

    this.cerrarPopUp();
  }

  private formularioTieneDatosValidos(): boolean {
    return this.form.valid;
  }

  actualizarDatosSprint(sprint: Sprint): void {
    sprint.nombre = this.form.get('nombre')?.value;
    sprint.fechaInicio = this.form.get('fechaInicio')?.value;
    sprint.fechaFin = this.form.get('fechaFin')?.value;
  }

}
