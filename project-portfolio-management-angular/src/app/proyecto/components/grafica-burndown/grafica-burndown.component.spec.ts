import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GraficaBurndownComponent } from './grafica-burndown.component';

describe('GraficaBurndownComponent', () => {
  let component: GraficaBurndownComponent;
  let fixture: ComponentFixture<GraficaBurndownComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GraficaBurndownComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GraficaBurndownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
