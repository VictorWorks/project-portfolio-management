import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { Observable, of, Subscription } from 'rxjs';

import { Chart, registerables } from 'chart.js';
Chart.register(...registerables);

import { AutenticacionService } from 'src/app/seguridad/services/autenticacion.service';
import { CabeceraBurndown } from '../../model/cabecera-burndown';

@Component({
  selector: 'app-grafica-burndown',
  templateUrl: './grafica-burndown.component.html',
  styleUrls: ['./grafica-burndown.component.css']
})
export class GraficaBurndownComponent implements OnInit {

  @Input('datosBurndown') datosBurndownObservable: Observable<CabeceraBurndown | null> = of(null);
  datosBurndownSubscription: Subscription | null = null;

  canvas: HTMLCanvasElement | null = null;
  context: CanvasRenderingContext2D | null = null;
  myChart!: Chart;

  constructor(private autenticacionService: AutenticacionService) { }

  ngOnInit(): void {

    this.datosBurndownSubscription = this.datosBurndownObservable
      .subscribe((datos) => {
        if (datos) {
          this.obtenerDatosGrafica(datos)
        }
      });
  }

  ngAfterViewInit(): void {
    this.obtenerComponentesGraficos();
  }

  ngOnDestroy(): void {
    this.datosBurndownSubscription?.unsubscribe();
  }

  private obtenerComponentesGraficos(): void {
    this.canvas = document.getElementById('burndown-chart') as HTMLCanvasElement;
    if (this.canvas == null)
      return;

    this.context = this.canvas.getContext('2d');
  }

  private obtenerDatosGrafica(datosBurndown: CabeceraBurndown): void {
    if (!datosBurndown) {
      return;
    }

    const inicial = new Date(datosBurndown.fechaInicio);
    const final = new Date(datosBurndown.fechaFin);
    const diasSprint = this.obtenerDiasSprint(inicial, final);
    const progresoIdeal = this.obtenerProgresoIdeal(diasSprint, datosBurndown.puntosHistoria);
    const progresoReal = datosBurndown.progreso.map(x => x.puntosFinalizados);
    this.crearGrafico(diasSprint, progresoIdeal, progresoReal);
  }

  private obtenerDiasSprint(inicial: Date, final: Date): Date[] {
    const result = [];
    let current = new Date(inicial.valueOf());
    while (current <= final) {
      result.push(current);
      current = new Date(current.valueOf())
      current.setDate(current.getDate() + 1);
    }
    return result;
  }

  private obtenerProgresoIdeal(fechasSprint: Date[], puntosHistoria: number): number[] {
    const result = [];
    const numeroDias = fechasSprint.length;
    const pendiente = -puntosHistoria / (numeroDias - 1);
    for (let i = 0; i < numeroDias; i++) {
      result.push(pendiente * i + puntosHistoria);
    }

    return result;
  }

  private crearGrafico(fechasSprint: Date[], progresoIdeal: number[], progresoReal: number[]): void {
    if (this.context == null || this.context == undefined)
      return;

    if (this.myChart) {
      this.myChart.destroy();
    }

    const fechasSprintFormateadas = fechasSprint.map(x => x.toLocaleDateString('es-ES'));
    const idealDataSet = this.construirDataSet('Progreso ideal', progresoIdeal, 'rgba(255, 0, 0, 0.3)');
    const realDataSet = this.construirDataSet('Progreso real', progresoReal, 'rgba(0, 0, 125, 0.9)', true, 'rgba(0, 0, 255, 0.2)');
    this.myChart = new Chart(this.context, {
      type: 'line',
      data: {
        labels: fechasSprintFormateadas,
        datasets: [idealDataSet, realDataSet],
      },
      options: {
        scales: {
          y: {
            beginAtZero: true,
          },
        },
      }
    });
  }

  private construirDataSet(etiqueta: string, datos: number[], colorLinea: string,
    rellenar: boolean = false, colorFondo: string = 'rgba(255, 255, 255, 0'): any {
    return {
      label: etiqueta,
      data: datos,
      borderWith: 2,
      fill: rellenar,
      borderColor: colorLinea,
      backgroundColor: colorFondo,
    }
  }

  public get puedeVerProyecto(): boolean {
    return this.autenticacionService.puedeVerProyecto;
  }
}

