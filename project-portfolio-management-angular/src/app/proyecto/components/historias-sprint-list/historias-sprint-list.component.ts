import { AfterViewInit, Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Observable, of, Subscription } from 'rxjs';
import { AutenticacionService } from 'src/app/seguridad/services/autenticacion.service';
import { DialogService } from 'src/app/shared/services/dialog.service';
import { HistoriaUsuario } from '../../model/historia-usuario';
import { HistoriaUsuarioService } from '../../services/historia-usuario.service';

@Component({
  selector: 'app-historias-sprint-list',
  templateUrl: './historias-sprint-list.component.html',
  styleUrls: ['./historias-sprint-list.component.css']
})
export class HistoriasSprintListComponent implements OnInit, AfterViewInit, OnDestroy {

  @Input('listaHistorias') listaHistoriasObservable: Observable<HistoriaUsuario[]> = of([]);
  @Input('idSprint') idSprintObservable: Observable<string> | undefined;
  @Output('onHistoriaAgregada') EventoAgregarHistoria = new EventEmitter<HistoriaUsuario>();
  @Output('onHistoriaEliminada') EventoEliminarHistoria = new EventEmitter<HistoriaUsuario>();

  @ViewChild(MatSort) sort: MatSort | null = null;
  dataSource!: MatTableDataSource<HistoriaUsuario>;
  nuevaHistoria = new FormControl();

  displayedColumns = ['nombre', 'actions'];
  private nameColumn: string = 'nombre';
  private actionColumn: string = 'actions';

  cargandoDatos: boolean = false;
  historiasDisponibles: HistoriaUsuario[] = [];
  historiasSprint: HistoriaUsuario[] = [];
  idSprintActual: string = '';

  listaHistoriasSubscription: Subscription | undefined = undefined;
  idSprintActualSubscription: Subscription | undefined = undefined;

  constructor(private historiaUsuarioService: HistoriaUsuarioService,
    private dialogService: DialogService,
    private autenticacionService: AutenticacionService) { }

  ngOnInit(): void {
    this.listaHistoriasSubscription = this.listaHistoriasObservable.subscribe((lista) => this.inicializarDataSource(lista));
    this.idSprintActualSubscription = this.idSprintObservable?.subscribe(id => this.idSprintActual = id);
    this.registrarBusquedaAutocompletado();

    if (this.puedeModificarProyecto) {
      this.displayedColumns = [this.nameColumn, this.actionColumn];
    } else {
      this.displayedColumns = [this.nameColumn];
    }
  }

  ngAfterViewInit(): void {
    if (this.dataSource === null || this.dataSource == undefined) return;
    this.dataSource.sort = this.sort;
  }

  ngOnDestroy(): void {
    this.listaHistoriasSubscription?.unsubscribe();
    this.idSprintActualSubscription?.unsubscribe();
  }

  inicializarDataSource(datos: HistoriaUsuario[]): void {
    if (datos === null || datos == undefined) return;
    this.dataSource = new MatTableDataSource(datos);
    this.dataSource.sort = this.sort;
    this.dataSource.filterPredicate = (data, filter) => {
      const dict = data as any;
      return this.displayedColumns.some((ele) => {
        return (
          ele != 'actions' && dict[ele].toLowerCase().indexOf(filter) != -1
        );
      });
    };

    this.historiasSprint = datos;
  }

  registrarBusquedaAutocompletado(): void {
    this.nuevaHistoria.valueChanges.subscribe(dato => {
      if (typeof (dato) === "string") {
        const coincidenciasObservable = this.historiaUsuarioService
          .obtenerHistoriaDisponibleFiltrado(this.idSprintActual, dato);
        coincidenciasObservable.subscribe(datos => {
          this.historiasDisponibles = datos;
        })
      } else {
        this.actualizarTextSinEvento(dato.nombre);
      }
    });
  }

  agregarASprint(): void {
    const introducido: string = this.nuevaHistoria.value;
    if (!introducido)
      return;

    const encontrado = this.historiasDisponibles
      .find((x) => x.nombre.toLowerCase() === introducido.toLowerCase());
    if (encontrado) {
      this.EventoAgregarHistoria.emit(encontrado);
      this.actualizarTextSinEvento('');
    } else {
      this.dialogService.abrirMensaje(
        'No se encuentra la historia: ' + introducido
      );
    }
  }

  quitarDeSprint(idHistoria: string): void {
    this.dialogService
      .abrirConfirmacion('Se va a eliminar la historia del sprint. ¿Está seguro?')
      .subscribe((confirmacion) => {
        if (confirmacion) {
          const historia = this.historiasSprint.find((x) => x.id === idHistoria);
          this.EventoEliminarHistoria.emit(historia);
        }
      });
  }

  public get puedeModificarProyecto(): boolean {
    return this.autenticacionService.puedeAñadirProyecto;
  }

  public get puedeVerProyecto(): boolean {
    return this.autenticacionService.puedeVerProyecto;
  }
  private actualizarTextSinEvento(texto: string): void {
    this.nuevaHistoria.setValue(texto, { emitEvent: false });
  }

}
