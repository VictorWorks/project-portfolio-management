import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoriasSprintListComponent } from './historias-sprint-list.component';

describe('HistoriasSprintListComponent', () => {
  let component: HistoriasSprintListComponent;
  let fixture: ComponentFixture<HistoriasSprintListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HistoriasSprintListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoriasSprintListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
