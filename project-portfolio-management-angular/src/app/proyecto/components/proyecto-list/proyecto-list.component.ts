import { ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';

import { Observable, of } from 'rxjs';


import { AutenticacionService } from 'src/app/seguridad/services/autenticacion.service';
import { Guardable } from 'src/app/shared/model/guardable';
import { DialogService } from 'src/app/shared/services/dialog.service';
import { Proyecto } from '../../model/proyecto';
import { ProyectoEditorComponent } from '../proyecto-editor/proyecto-editor.component';


@Component({
  selector: 'app-proyecto-list',
  templateUrl: './proyecto-list.component.html',
  styleUrls: ['./proyecto-list.component.css']
})
export class ProyectoListComponent implements OnInit {

  @Input('proyectos') proyectosObservable: Observable<Proyecto[]> = of([]);
  @Output('onBorrarProyecto') eventoBorrarProyecto = new EventEmitter<string>();
  @Output('onProyectoSeleccionado') eventoSeleccionarProyecto = new EventEmitter<string>();
  @Output('onProyectoGuardado') eventoProyectoGuardado = new EventEmitter<Guardable<Proyecto>>();

  @ViewChild(MatSort) sort: MatSort | null = null;
  displayedColumns = ['nombre', 'actions'];
  dataSource!: MatTableDataSource<Proyecto>;
  clave: string = '';

  constructor(private popup: MatDialog, private dialogService: DialogService,
    private autenticacionService: AutenticacionService) { }

  ngOnInit(): void {
    if (this.proyectosObservable)
      this.proyectosObservable.subscribe((x) => this.actualizarListado(x));
  }

  actualizarListado(datos: Proyecto[]): void {
    this.dataSource = new MatTableDataSource(datos);
    this.dataSource.sort = this.sort
    this.dataSource.filterPredicate = (data, filter) => {
      return this.displayedColumns.some(ele => {
        const dict = data as any;
        return ele != 'actions' && dict[ele].indexOf(filter) != -1;
      });
    }
  }

  filtrarEntradas(): void {
    if (this.dataSource) {
      this.dataSource.filter = this.clave.trim().toLowerCase();
    }
  }

  limpiarFiltro(): void {
    this.clave = '';
    this.filtrarEntradas();
  }

  borrarEntrada(id: string): void {
    this.dialogService
      .abrirConfirmacion('Se va a borrar un registro. ¿Está seguro?')
      .subscribe((confirmacion) => {
        if (confirmacion) {
          this.eventoBorrarProyecto.emit(id);
        }
      });
  }

  nuevo(): void {
    this.lanzarFormulario(null);
  }

  editarEntrada(row: any): void {
    this.lanzarFormulario(row);
  }
  lanzarFormulario(dato: any): void {
    const config = new MatDialogConfig();
    config.disableClose = true;
    config.autoFocus = true;
    config.width = '60%';
    if (dato != null) config.data = dato;
    const ref = this.popup.open(ProyectoEditorComponent, config);
    ref.componentInstance.eventoGuardarProyecto.subscribe((dato) => this.eventoProyectoGuardado.emit(dato));
    ref.afterClosed().subscribe((result) => {
    });
  }

  verProyecto(id: string): void {
    this.eventoSeleccionarProyecto.emit(id);
  }

  public get puedeModificarProyecto(): boolean {
    return this.autenticacionService.puedeAñadirProyecto;
  }

  public get puedeBorrarProyecto(): boolean {
    return this.autenticacionService.puedeBorrarProyecto;
  }

  public get puedeVerProyecto(): boolean {
    return this.autenticacionService.puedeVerProyecto;
  }


}
