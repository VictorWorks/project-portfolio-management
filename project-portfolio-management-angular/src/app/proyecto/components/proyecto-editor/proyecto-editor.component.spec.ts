import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProyectoEditorComponent } from './proyecto-editor.component';

describe('ProyectoEditorComponent', () => {
  let component: ProyectoEditorComponent;
  let fixture: ComponentFixture<ProyectoEditorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProyectoEditorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProyectoEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
