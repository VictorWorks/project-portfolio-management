import { Component, Inject, OnInit, Output, EventEmitter } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { Guardable } from 'src/app/shared/model/guardable';
import { Proyecto } from '../../model/proyecto';

@Component({
  selector: 'app-proyecto-editor',
  templateUrl: './proyecto-editor.component.html',
  styleUrls: ['./proyecto-editor.component.css']
})
export class ProyectoEditorComponent implements OnInit {

  @Output() eventoGuardarProyecto = new EventEmitter<Guardable<Proyecto>>();

  form = new FormGroup({
    nombre: new FormControl('', Validators.required),
  });
  constructor(
    private dialogRef: MatDialogRef<ProyectoEditorComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Proyecto
  ) {
    this.inicializarDatosFormulario();
    if (data != null) {
      this.form.reset(data);
    }
  }

  ngOnInit(): void {}

  limpiarForm(): void {
    this.form.reset();
  }

  inicializarDatosFormulario(): void {
    this.form.reset({
      nombre: '',
    });
  }

  enviarDatos(): void {
    if (this.form.invalid) return;
    const datosAGuardar = this.data ? this.data : new Proyecto();
    this.actualizarDatosProyecto(datosAGuardar);
    const proyectoGuardable = new Guardable(!this.data, datosAGuardar);
    this.eventoGuardarProyecto.emit(proyectoGuardable);

    this.cerrarPopUp();
  }

  actualizarDatosProyecto(proyecto: Proyecto): void {
    proyecto.nombre = this.form.get('nombre')?.value;
  }

  cerrarPopUp(): void {
    this.dialogRef.close();
  }

}
