import { AfterViewInit, Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Observable, of, Subscription } from 'rxjs';
import { Persona } from 'src/app/personal/model/persona';
import { PersonalService } from 'src/app/personal/services/personal.service';
import { AutenticacionService } from 'src/app/seguridad/services/autenticacion.service';
import { DialogService } from 'src/app/shared/services/dialog.service';

@Component({
  selector: 'app-equipo-proyecto-list',
  templateUrl: './equipo-proyecto-list.component.html',
  styleUrls: ['./equipo-proyecto-list.component.css']
})
export class EquipoProyectoListComponent implements OnInit, AfterViewInit, OnDestroy {

  @Input('listaMiembros') listaEquipoObservable: Observable<Persona[]> = of([]);
  @Input('idProyecto') idProyectoObservable: Observable<string> = of('');
  @Output('onPersonaAgregada') EventoAgregarPersona = new EventEmitter<Persona>();
  @Output('onPersonaEliminada') EventoEliminarPersona = new EventEmitter<Persona>();

  @ViewChild(MatSort) sort: MatSort | null = null;
  displayedColumns = ['nombre', 'actions'];
  dataSource!: MatTableDataSource<Persona>;
  nuevoMiembro = new FormControl();

  idProyectoActual: string = '';
  personalAsignado: Persona[] = [];
  personalDisponible: Persona[] = [];
  cargandoDatos: boolean = false;

  listaMiembrosSubscription: Subscription | null = null;
  idProyectoActualSubscription: Subscription | null = null;

  constructor(
    private personalService: PersonalService,
    private dialogService: DialogService,
    private autenticacionService: AutenticacionService
  ) {
  }

  ngOnInit(): void {
    this.listaMiembrosSubscription = this.listaEquipoObservable.subscribe((lista) => this.inicializarDataSource(lista));
    this.idProyectoActualSubscription = this.idProyectoObservable.subscribe(id => this.idProyectoActual = id);
    this.registrarBusquedaAutocompletado();
  }

  ngAfterViewInit(): void {
    if (this.dataSource === null || this.dataSource == undefined) return;
    this.dataSource.sort = this.sort;
  }

  ngOnDestroy(): void {
    this.listaMiembrosSubscription?.unsubscribe();
    this.idProyectoActualSubscription?.unsubscribe();
  }

  registrarBusquedaAutocompletado(): void {
    this.nuevoMiembro.valueChanges.subscribe(dato => {
      if (typeof (dato) === "string") {
        const coincidenciasObservable = this.personalService.obtenerPersonalDisponibleFiltrado(this.idProyectoActual, dato);
        coincidenciasObservable.subscribe((datos: Persona[]) => {
          this.personalDisponible = datos;
        })
      } else {
        this.actualizarTextSinEvento(dato.nombre);
      }
    });
  }

  inicializarDataSource(datos: any): void {
    if (datos === null || datos == undefined) return;
    this.dataSource = new MatTableDataSource(datos);
    this.dataSource.sort = this.sort;
    this.dataSource.filterPredicate = (data, filter) => {
      const dict = data as any;
      return this.displayedColumns.some((ele) => {
        const value = dict[ele].toString().trim().toLowerCase();
        return (
          ele != 'actions' && value.indexOf(filter) != -1
        );
      });
    };

    this.personalAsignado = datos;
  }

  agregarAEquipo(): void {
    const introducido: string = this.nuevoMiembro.value;

    if (!introducido)
      return;

    const encontrado = this.personalDisponible.find(
      (x) => x.nombre.toLocaleLowerCase() === introducido.toLocaleLowerCase());
    if (encontrado) {
      this.EventoAgregarPersona.emit(encontrado);
      this.actualizarTextSinEvento('');
    } else {
      this.dialogService.abrirMensaje(
        'No se encuentra la persona: ' + introducido
      );
    }
  }

  quitarDeEquipo(id: string): void {
    this.dialogService
      .abrirConfirmacion('Se va a borrar un registro. ¿Está seguro?')
      .subscribe((confirmacion) => {
        if (confirmacion) {
          const persona = this.personalAsignado.find((x) => x.id === id);
          this.EventoEliminarPersona.emit(persona);
        }
      });
  }

  private actualizarTextSinEvento(texto: string): void {
    this.nuevoMiembro.setValue(texto, { emitEvent: false });
  }

  // Solo se puede cambiar el equipo del proyecto si se puede
  // modificar el proyecto.
  get puedeModificarProyecto(): boolean {
    return this.autenticacionService.puedeAñadirProyecto;
  }

  get puedeVerProyecto(): boolean {
    return this.autenticacionService.puedeVerProyecto;
  }

}
