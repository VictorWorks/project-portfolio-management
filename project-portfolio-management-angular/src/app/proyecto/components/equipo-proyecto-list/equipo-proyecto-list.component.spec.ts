import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EquipoProyectoListComponent } from './equipo-proyecto-list.component';

describe('EquipoProyectoListComponent', () => {
  let component: EquipoProyectoListComponent;
  let fixture: ComponentFixture<EquipoProyectoListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EquipoProyectoListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EquipoProyectoListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
