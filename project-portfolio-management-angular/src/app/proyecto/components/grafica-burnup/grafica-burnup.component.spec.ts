import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GraficaBurnupComponent } from './grafica-burnup.component';

describe('GraficaBurnupComponent', () => {
  let component: GraficaBurnupComponent;
  let fixture: ComponentFixture<GraficaBurnupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GraficaBurnupComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GraficaBurnupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
