import { Component, Input, OnInit } from '@angular/core';
import { Observable, of } from 'rxjs';

import { Chart, registerables } from 'chart.js';
Chart.register(...registerables);

import { AutenticacionService } from 'src/app/seguridad/services/autenticacion.service';
import { DatoBurnUp } from '../../model/dato-burnup';

@Component({
  selector: 'app-grafica-burnup',
  templateUrl: './grafica-burnup.component.html',
  styleUrls: ['./grafica-burnup.component.css']
})
export class GraficaBurnupComponent implements OnInit {

  @Input("datosGrafica") datosGraficaObservable: Observable<DatoBurnUp[]> = of([]);
  canvas: HTMLCanvasElement | null = null;
  context: CanvasRenderingContext2D | null = null;
  myChart!: Chart;

  constructor(private autenticacionService: AutenticacionService) { }

  ngOnInit(): void {
    this.datosGraficaObservable.subscribe((listado) => this.actualizarGrafica(listado));
  }

  ngAfterViewInit() {
    this.obtenerComponentesGraficos();
  }

  public get puedeVerProyecto(): boolean {
    return this.autenticacionService.puedeVerProyecto;
  }

  private obtenerComponentesGraficos() {
    this.canvas = document.getElementById('burnup-chart') as HTMLCanvasElement;

    this.context = this.canvas.getContext('2d');
  }

  private actualizarGrafica(listado: DatoBurnUp[]): void {
    const nombresSprint = listado.map((dato) => dato.nombreSprint);
    const puntosHistoriaFinalizados = listado.map((dato) => dato.puntosHistoriasCompletados);
    const puntosHistoriaTotales = listado.map((dato) => dato.puntosHistoriaTotales);
    this.crearGrafica(nombresSprint, puntosHistoriaFinalizados, puntosHistoriaTotales);
  }

  private crearGrafica(sprints: string[], puntosCompletados: number[], puntosTotales: number[]): void {
    if (this.context == null || this.context == undefined)
      return;

    if (this.myChart) {
      this.myChart.destroy();
    }
    const dataSetHistoria = this.construirDataSet(
      'Puntos historia abordados',
      puntosCompletados,
      'rgba(255, 0, 0, 0.9)',
      true,
      'rgba(235, 0, 0, 0.3)'
    );

    const dataSetBacklog = this.construirDataSet(
      'Puntos historia en backlog',
      puntosTotales,
      'rgba(0, 0, 255, 0.9)',
      true,
      'rgba(0, 0, 235, 0.3)'
    );

    this.myChart = new Chart(this.context, {
      type: 'line',
      data: {
        labels: sprints,
        datasets: [dataSetHistoria, dataSetBacklog],
      },
      options: {
        scales: {
          y: {
            beginAtZero: true,
          },
        },
      },
    });
  }

  private construirDataSet(
    etiqueta: string,
    datos: number[],
    colorLinea: string,
    rellenar = false,
    colorFondo = 'rgba(255, 255, 255, 0)'
  ): any {
    return {
      label: etiqueta,
      data: datos,
      borderWith: 2,
      fill: rellenar,
      borderColor: colorLinea,
      backgroundColor: colorFondo,
    };
  }

}
