import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListaProyectosComponent } from './pages/lista-proyectos/lista-proyectos.component';
import { PanelProyectoComponent } from './pages/panel-proyecto/panel-proyecto.component';
import { PanelSprintComponent } from './pages/panel-sprint/panel-sprint.component';

const routes: Routes = [
  {
    path: '',
    children: [
      { path: 'proyecto', component: ListaProyectosComponent },
      { path: 'projectPanel', component: PanelProyectoComponent },
      { path: 'sprintPanel', component: PanelSprintComponent },
      { path: '', redirectTo: 'proyecto' },

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProyectoRoutingModule { }
