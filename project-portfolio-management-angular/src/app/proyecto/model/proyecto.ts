import { Entidad } from "src/app/model/entidad";
import { Persona } from "src/app/personal/model/persona";


export class Proyecto extends Entidad {
    nombre:string = '';
    equipo: Persona[] = [];

    constructor(){
        super();
    }
}
