import { Entidad } from "src/app/model/entidad";

export class Sprint extends Entidad {
    proyectoId: string = '';
    nombre: string = '';
    fechaInicio: string = '';
    fechaFin: string = '';
    puntosHistoria: number = 0;

    constructor() {
        super();
    }
}
