import { Entidad } from "src/app/model/entidad";

export class HistoriaUsuario extends Entidad{
    proyectoId: string = '';
    sprintId: string = '';
    nombre: string = '';
    descripcion: string = '';
    estimacion?: number;
    estadoActual?: string;
    prioridad:number = 0;
    fechaAlta: string = '';

    constructor() {
        super();
    }
}
