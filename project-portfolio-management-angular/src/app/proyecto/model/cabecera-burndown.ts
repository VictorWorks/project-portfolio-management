import { DatoBurndown } from "./dato-burndown";

export class CabeceraBurndown{
  fechaInicio: string = '';
  fechaFin: string = '';
  puntosHistoria: number = 0;
  progreso: DatoBurndown[] = [];
}
