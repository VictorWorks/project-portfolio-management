import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { Tablero } from 'src/app/taskboard/model/tablero';
import { CabeceraBurndown } from '../../model/cabecera-burndown';
import { HistoriaUsuario } from '../../model/historia-usuario';
import { PanelSprintService } from '../../services/panel-sprint.service';

@Component({
  selector: 'app-panel-sprint',
  templateUrl: './panel-sprint.component.html',
  styleUrls: ['./panel-sprint.component.css']
})
export class PanelSprintComponent implements OnInit, OnDestroy {

  nombreSprint: string = '';
  subscriptionNombreSprint: Subscription | null = null;

  constructor(private activatedRoute: ActivatedRoute,
    private panelSprintService: PanelSprintService) { }

  ngOnInit(): void {
    this.activatedRoute.queryParams.subscribe(params => {
      this.panelSprintService.establecerSprintId(params['idSprint'])
    });

    this.subscriptionNombreSprint = this.panelSprintService.nombreSprintObservable
      .subscribe((nombre: string) => this.nombreSprint = nombre);
  }

  ngOnDestroy(): void {
    this.subscriptionNombreSprint?.unsubscribe();
  }

  obtenerListaHistoriasSprint(): Observable<HistoriaUsuario[]> {
    return this.panelSprintService.listaHistoriasSprintObservable;
  }

  obtenerIdProyectoActual(): Observable<string> {
    return this.panelSprintService.sprintIdObservable;
  }

  agregarHistoriaASprint(historia: HistoriaUsuario): void {
    this.panelSprintService.agregarHistoriaASprint(historia);
  }

  quitarHistoriaDeSprint(historia: HistoriaUsuario): void {
    this.panelSprintService.quitarHistoriaDeSprint(historia)
  }

  obtenerDatosBurndown(): Observable<CabeceraBurndown> {
    return this.panelSprintService.datosBurndownObservable;
  }

  obtenerTableroTareas(): Observable<Tablero> {
    return this.panelSprintService.tableroTareasObservable;
  }

  cambiarEstadoTarea(datosCambio: any): void {
    return this.panelSprintService.cambiarEstadoTarea(datosCambio.tareaCambiada, datosCambio.nuevoEstado);
  }

  volver(): void {
    this.panelSprintService.volver();
  }

}
