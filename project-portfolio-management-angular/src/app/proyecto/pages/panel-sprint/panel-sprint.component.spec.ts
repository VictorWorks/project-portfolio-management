import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PanelSprintComponent } from './panel-sprint.component';

describe('PanelSprintComponent', () => {
  let component: PanelSprintComponent;
  let fixture: ComponentFixture<PanelSprintComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PanelSprintComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PanelSprintComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
