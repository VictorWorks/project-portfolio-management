import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import { Guardable } from 'src/app/shared/model/guardable';
import { Proyecto } from '../../model/proyecto';
import { ProyectosPageService } from '../../services/proyectos-page.service';

@Component({
  selector: 'app-lista-proyectos',
  templateUrl: './lista-proyectos.component.html',
  styleUrls: ['./lista-proyectos.component.css']
})
export class ListaProyectosComponent implements OnInit {

  constructor(private paginaService: ProyectosPageService) { }

  obtenerListaProyectos(): Observable<Proyecto[]> {
    return this.paginaService.proyectosActuales;
  }
  ngOnInit(): void {
    this.paginaService.inicializarVista();
  }

  borrarProyecto(idProyecto: string): void {
    this.paginaService.borrarProyecto(idProyecto);
  }

  seleccionarProyecto(idProyecto: string): void {
    this.paginaService.mostrarProyecto(idProyecto);
  }

  guardarProyecto(proyecto: Guardable<Proyecto>): void {
    this.paginaService.guardarProyecto(proyecto);
  }

}
