import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { Persona } from 'src/app/personal/model/persona';
import { Guardable } from 'src/app/shared/model/guardable';
import { DatoBurnUp } from '../../model/dato-burnup';
import { HistoriaUsuario } from '../../model/historia-usuario';
import { Sprint } from '../../model/sprint';
import { PanelProyectoService } from '../../services/panel-proyecto.service';

@Component({
  selector: 'app-panel-proyecto',
  templateUrl: './panel-proyecto.component.html',
  styleUrls: ['./panel-proyecto.component.css']
})
export class PanelProyectoComponent implements OnInit {

  nombreProyecto: string = '';
  subscriptionNombreProyecto: Subscription | null = null;

  constructor(private activatedRoute: ActivatedRoute,
    private panelProyectoService: PanelProyectoService,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.activatedRoute.queryParams.subscribe(params => {
      this.panelProyectoService.establecerProyectoId(params['idProyecto'])
    });

    this.subscriptionNombreProyecto = this.panelProyectoService.nombreProyecto.subscribe((nuevoNombre: string) =>
      this.actualizarNombreProyecto(nuevoNombre));
  }

  ngOnDestroy() {
    this.subscriptionNombreProyecto?.unsubscribe();
  }

  actualizarNombreProyecto(nombreProyecto: string) {
    if (nombreProyecto)
      this.nombreProyecto = nombreProyecto;
    else
      this.nombreProyecto = '';

  }
  idProyectoActual(): Observable<string> {
    return this.panelProyectoService.idProyecto;
  }

  sprintsProyectoActual(): Observable<Sprint[]> {
    return this.panelProyectoService.listaSprintsActuales;
  }

  borrarSprintPorId(id: string): void {
    this.panelProyectoService.borrarSprintPorId(id);
  }

  guardarSprint(sprint: Guardable<Sprint>) {
    this.panelProyectoService.guardarSprint(sprint);
  }

  seleccionarSprint(id: string): void {
    this.panelProyectoService.mostrarSprint(id);
  }

  historiasUsuarioProyectoActual(): Observable<HistoriaUsuario[]> {
    return this.panelProyectoService.listaHistoriasUsuarioActuales;
  }

  borrarHistoriaPorID(idHistoria: string): void {
    this.panelProyectoService.borrarHistoriaPorId(idHistoria);
  }

  guardarHistoria(historiaUsuario: Guardable<HistoriaUsuario>): void {
    this.panelProyectoService.guardarHistoria(historiaUsuario);
  }

  equipoProyectoActual(): Observable<Persona[]> {
    return this.panelProyectoService.listaEquipoProyectoActual;
  }

  agregarPersonaAEquipo(persona: Persona): void {
    this.panelProyectoService.agregarPersonaAEquipo(persona);
  }

  quitarPersonaDeEquipo(persona: Persona): void {
    this.panelProyectoService.quitarPersonaDeEquipo(persona);
  }

  obtenerBurnUp(): Observable<DatoBurnUp[]> {
    return this.panelProyectoService.listaDatosBurnUp;
  }

  volver(): void {
    this.router.navigate(['proyectos', 'proyecto']);
  }

}
