import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PanelProyectoComponent } from './panel-proyecto.component';

describe('PanelProyectoComponent', () => {
  let component: PanelProyectoComponent;
  let fixture: ComponentFixture<PanelProyectoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PanelProyectoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PanelProyectoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
