import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { backlogServiceUrl, proyectoServiceUrl } from 'src/app/config/url.config';
import { UrlQuery } from 'src/app/shared/model/url-query';
import { CommunicationService } from 'src/app/shared/services/communication.service';
import { HistoriaUsuario } from '../model/historia-usuario';

@Injectable({
  providedIn: 'root'
})
export class HistoriaUsuarioService {

  constructor(private communicationService: CommunicationService) { }

  obtenerHistoriasParaProyecto(idProyecto: string): Observable<HistoriaUsuario[]> {
    const query = new UrlQuery(idProyecto).consultarRecurso('historias');
    return this.communicationService.getAll<HistoriaUsuario>(proyectoServiceUrl, query);
  }

  borrarHistoriaPorId(idHistoria: string): Observable<HistoriaUsuario> {
    return this.communicationService.delete<HistoriaUsuario>(backlogServiceUrl, new UrlQuery(idHistoria));
  }

  guardarHistoria(historiaUsuario: HistoriaUsuario): Observable<HistoriaUsuario> {
    return this.communicationService.save(backlogServiceUrl, historiaUsuario);
  }

  actualizarHistoria(historiaUsuario: HistoriaUsuario): Observable<HistoriaUsuario> {
    return this.communicationService.update(backlogServiceUrl, new UrlQuery(historiaUsuario.id), historiaUsuario);
  }

  obtenerHistoriaDisponibleFiltrado(idSprint: string, mascara: string): Observable<HistoriaUsuario[]> {
    if (!mascara) {
      return of([]);
    }

    const query = new UrlQuery()
      .consultarRecurso('claveNombre', mascara)
      .consultarRecurso('noPerteneciente', idSprint);

    return this.communicationService.getAll<HistoriaUsuario>(backlogServiceUrl, query);
  }

}
