import { TestBed } from '@angular/core/testing';

import { PanelSprintService } from './panel-sprint.service';

describe('PanelSprintService', () => {
  let service: PanelSprintService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PanelSprintService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
