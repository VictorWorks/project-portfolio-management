import { BehaviorSubject } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { NotificacionService } from 'src/app/shared/services/notificacion.service';
import { Router } from '@angular/router';

import { Proyecto } from '../model/proyecto';
import { ProyectoService } from './proyecto.service';
import { Guardable } from 'src/app/shared/model/guardable';

@Injectable({
  providedIn: 'root'
})
export class ProyectosPageService {

  private proyectosSubject = new BehaviorSubject<Proyecto[]>([]);
  proyectosActuales = this.proyectosSubject.asObservable();

  constructor(private proyectoService: ProyectoService,
    private notificacionService: NotificacionService,
    private router: Router) { }

  inicializarVista(): void {
    this.actualizarListaProyectos();
  }

  private actualizarListaProyectos(): void {
    this.proyectoService.obtenerTodos().subscribe(
      (lista: Proyecto[]) => this.proyectosSubject.next(lista),
      (error: HttpErrorResponse) => { this.notificacionService.notificarRespuestaError(error) },
    );
  }

  guardarProyecto(proyecto: Guardable<Proyecto>) {
    const accionGuardado = proyecto.esNuevo ? this.proyectoService.guardarProyecto(proyecto.datos) :
      this.proyectoService.actualizarProyecto(proyecto.datos);
    accionGuardado.subscribe({
      next: (p: Proyecto) => this.actualizarListaProyectos(),
      error: (error: HttpErrorResponse) => { this.notificacionService.notificarRespuestaError(error) },
    });
  }

  borrarProyecto(idProyecto: string): void {
    this.proyectoService.borrarProyectoPorId(idProyecto).subscribe({
      next: (p: Proyecto) => this.actualizarListaProyectos(),
      error: (error: HttpErrorResponse) => { this.notificacionService.notificarRespuestaError(error) },
    });
  }

  mostrarProyecto(idProyecto: string): void {
    this.router.navigate(['proyectos', 'projectPanel'], { queryParams: { idProyecto } });
  }

}
