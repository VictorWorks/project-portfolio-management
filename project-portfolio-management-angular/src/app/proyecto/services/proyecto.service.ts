import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { CommunicationService } from 'src/app/shared/services/communication.service';
import { DatoBurnUp } from '../model/dato-burnup';
import { Proyecto } from '../model/proyecto';
import { proyectoServiceUrl, sprintServiceUrl } from 'src/app/config/url.config';
import { Sprint } from '../model/sprint';
import { UrlQuery } from 'src/app/shared/model/url-query';

@Injectable({
  providedIn: 'root'
})
export class ProyectoService {

  constructor(private communicationService: CommunicationService) { }

  obtenerTodos(): Observable<Proyecto[]> {
    return this.communicationService.getAll(proyectoServiceUrl);
  }

  obtenerProyectoPorId(idProyecto: string): Observable<Proyecto> {
    return this.communicationService.get(proyectoServiceUrl, new UrlQuery(idProyecto));
  }

  guardarProyecto(proyecto: Proyecto): Observable<Proyecto> {
    return this.communicationService.save(proyectoServiceUrl, proyecto);
  }

  actualizarProyecto(proyecto: Proyecto): Observable<Proyecto> {
    return this.communicationService.update(proyectoServiceUrl, new UrlQuery(proyecto.id), proyecto);
  }

  borrarProyectoPorId(id: string): Observable<Proyecto> {
    return this.communicationService.delete(proyectoServiceUrl, new UrlQuery(id));
  }

  obtenerSprintParaProyecto(idProyecto: string): Observable<Sprint[]> {
    const query = new UrlQuery().consultarRecurso('proyecto', idProyecto);
    return this.communicationService.getAll(sprintServiceUrl, query);
  }

  obtenerListaDatosBurnUpParaProyecto(idProyecto: string): Observable<DatoBurnUp[]> {
    const query = new UrlQuery(idProyecto).consultarRecurso('burnup');
    return this.communicationService.getAll(proyectoServiceUrl, query);
  }

}
