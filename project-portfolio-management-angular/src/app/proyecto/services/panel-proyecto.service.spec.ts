import { TestBed } from '@angular/core/testing';

import { PanelProyectoService } from './panel-proyecto.service';

describe('PanelProyectoService', () => {
  let service: PanelProyectoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PanelProyectoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
