import { TestBed } from '@angular/core/testing';

import { ProyectosPageService } from './proyectos-page.service';

describe('ProyectosPageService', () => {
  let service: ProyectosPageService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ProyectosPageService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
