import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { CabeceraBurndown } from '../model/cabecera-burndown';
import { HistoriaUsuario } from '../model/historia-usuario';
import { CommunicationService } from 'src/app/shared/services/communication.service';
import { Sprint } from '../model/sprint';
import { sprintServiceUrl } from 'src/app/config/url.config';
import { Tablero } from 'src/app/taskboard/model/tablero';
import { UrlQuery } from 'src/app/shared/model/url-query';

@Injectable({
  providedIn: 'root'
})
export class SprintService {

  constructor(private communicationService: CommunicationService) { }

  borrarSprintPorId(idSprint: string): Observable<Sprint> {
    return this.communicationService.delete(sprintServiceUrl, new UrlQuery(idSprint));
  }

  guardarSprint(sprint: Sprint): Observable<Sprint> {
    return this.communicationService.save(sprintServiceUrl, sprint);
  }

  actualizarSprint(sprint: Sprint): Observable<Sprint> {
    return this.communicationService.update(sprintServiceUrl, new UrlQuery(sprint.id), sprint);
  }

  obtenerSprintPorId(idSprint: string): Observable<Sprint> {
    return this.communicationService.get(sprintServiceUrl, new UrlQuery(idSprint));
  }

  obtenerHistoriasParaSprint(idSprint: string): Observable<HistoriaUsuario[]> {
    return this.communicationService.get(sprintServiceUrl,
      new UrlQuery(idSprint).consultarRecurso('historiasUsuario'));
  }

  agregarHistoriaASprint(idSprint: string, historia: HistoriaUsuario): Observable<HistoriaUsuario> {
    return this.communicationService.update<HistoriaUsuario>(sprintServiceUrl,
      new UrlQuery(idSprint).consultarRecurso('agregarHistoria'), historia);
  }

  quitarHistoriaDeSprint(idSprint: string, historia: HistoriaUsuario): Observable<HistoriaUsuario> {
    return this.communicationService.update<HistoriaUsuario>(sprintServiceUrl,
      new UrlQuery(idSprint).consultarRecurso('eliminarHistoria'), historia);
  }

  obtenerBurndownParaSprint(idSprint: string): Observable<CabeceraBurndown> {
    return this.communicationService.get<CabeceraBurndown>(sprintServiceUrl,
      new UrlQuery(idSprint).consultarRecurso('burndown'));
  }

  obtenerTareasParaSprint(idSprint: string): Observable<Tablero> {
    return this.communicationService.get<Tablero>(sprintServiceUrl,
      new UrlQuery(idSprint).consultarRecurso('kanban'));
  }

  cambiarEstadoTarea(idSprint: string, historia: HistoriaUsuario, nuevoEstado:string): Observable<HistoriaUsuario> {
    const query = new UrlQuery(idSprint)
      .consultarRecurso('moverHistoria', historia.id)
      .consultarRecurso('aEstado', nuevoEstado);
    return this.communicationService.update(sprintServiceUrl, query, historia);
  }
}
