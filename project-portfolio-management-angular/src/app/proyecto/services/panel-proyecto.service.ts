import { BehaviorSubject, Observable } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { DatoBurnUp } from '../model/dato-burnup';
import { Guardable } from 'src/app/shared/model/guardable';
import { HistoriaUsuario } from '../model/historia-usuario';
import { HistoriaUsuarioService } from './historia-usuario.service';
import { NotificacionService } from 'src/app/shared/services/notificacion.service';
import { Persona } from 'src/app/personal/model/persona';
import { PersonalService } from 'src/app/personal/services/personal.service';
import { Proyecto } from '../model/proyecto';
import { ProyectoService } from './proyecto.service';
import { Sprint } from '../model/sprint';
import { SprintService } from './sprint.service';

@Injectable({
  providedIn: 'root'
})
export class PanelProyectoService {

  private idProyectoSubject = new BehaviorSubject<string>('');
  idProyecto: Observable<string> = this.idProyectoSubject.asObservable();

  private nombreProyectoSubject = new BehaviorSubject<string>('');
  nombreProyecto: Observable<string> = this.nombreProyectoSubject.asObservable();

  private listaSprintsSubject = new BehaviorSubject<Sprint[]>([]);
  listaSprintsActuales = this.listaSprintsSubject.asObservable();

  private listaHistoriasUsuarioSubject = new BehaviorSubject<HistoriaUsuario[]>([]);
  listaHistoriasUsuarioActuales = this.listaHistoriasUsuarioSubject.asObservable();

  private listaEquipoProyectoSubject = new BehaviorSubject<Persona[]>([]);
  listaEquipoProyectoActual: Observable<Persona[]> = this.listaEquipoProyectoSubject.asObservable();

  private listaDatosBurnUpSubject = new BehaviorSubject<DatoBurnUp[]>([]);
  listaDatosBurnUp: Observable<DatoBurnUp[]> = this.listaDatosBurnUpSubject.asObservable();

  constructor(private proyectoService: ProyectoService,
    private sprintService: SprintService,
    private historiaUsuarioService: HistoriaUsuarioService,
    private personalService: PersonalService,
    private notificacionService: NotificacionService,
    private router: Router) { }

  establecerProyectoId(idProyecto: string) {
    this.idProyectoSubject.next(idProyecto);
    this.actualizarNombreProyecto();
    this.actualizarListaSprints();
    this.actualizarListaHistoriasUsuario();
    this.actualizarListaEquipoProyecto();
    this.actualizarListaDatosBurnUp();
  }

  private obtenerIdProyectoActual(): string {
    return this.idProyectoSubject.getValue();
  }

  actualizarNombreProyecto(): void {
    this.proyectoService.obtenerProyectoPorId(this.obtenerIdProyectoActual())
      .subscribe({
        next: (proyecto: Proyecto) => this.nombreProyectoSubject.next(proyecto.nombre),
        error: (error: HttpErrorResponse) => { this.notificacionService.notificarRespuestaError(error) }
      });
  }

  private actualizarListaSprints(): void {
    this.proyectoService.obtenerSprintParaProyecto(this.obtenerIdProyectoActual())
      .subscribe({
        next: (listaSprints: Sprint[]) => this.listaSprintsSubject.next(listaSprints),
        error: (error: HttpErrorResponse) => { this.notificacionService.notificarRespuestaError(error) }
      });
  }

  borrarSprintPorId(idSprint: string): void {
    this.sprintService.borrarSprintPorId(idSprint).subscribe({
      next: (_) => {
        this.actualizarListaSprints();
        this.actualizarListaDatosBurnUp();
      },
      error: (error: HttpErrorResponse) => { this.notificacionService.notificarRespuestaError(error) },
    });
  }

  guardarSprint(sprint: Guardable<Sprint>): void {
    if (!sprint.datos.proyectoId) {
      sprint.datos.proyectoId = this.obtenerIdProyectoActual();
    }
    const accionGuardado = sprint.esNuevo ? this.sprintService.guardarSprint(sprint.datos) : this.sprintService.actualizarSprint(sprint.datos);
    accionGuardado.subscribe({
      next: (s: Sprint) => {
        this.actualizarListaSprints();
        this.actualizarListaDatosBurnUp();
      },
      error: (error: HttpErrorResponse) => { this.notificacionService.notificarRespuestaError(error) },
    });
  }

  mostrarSprint(idSprint: string): void {
    this.router.navigate(['proyectos','sprintPanel'], { queryParams: { idSprint: idSprint } });
  }

  private actualizarListaHistoriasUsuario(): void {
    this.historiaUsuarioService.obtenerHistoriasParaProyecto(this.obtenerIdProyectoActual())
      .subscribe((listaHistorias: HistoriaUsuario[]) => this.listaHistoriasUsuarioSubject.next(listaHistorias));
  }

  borrarHistoriaPorId(idHistoria: string): void {
    this.historiaUsuarioService.borrarHistoriaPorId(idHistoria).subscribe({
      next:(h: HistoriaUsuario) => {
        this.actualizarListaHistoriasUsuario();
        this.actualizarListaDatosBurnUp();
      },
      error: (error: HttpErrorResponse) => { this.notificacionService.notificarRespuestaError(error) },
    });
  }

  guardarHistoria(historiaUsuario: Guardable<HistoriaUsuario>): void {
    if (!historiaUsuario.datos.proyectoId)
      historiaUsuario.datos.proyectoId = this.obtenerIdProyectoActual();

    const accionGuardado = historiaUsuario.esNuevo ? this.historiaUsuarioService.guardarHistoria(historiaUsuario.datos) : this.historiaUsuarioService.actualizarHistoria(historiaUsuario.datos);
    accionGuardado.subscribe({
      next: (_) => {
        this.actualizarListaHistoriasUsuario();
        this.actualizarListaDatosBurnUp();
      },
      error: (error: HttpErrorResponse) => { this.notificacionService.notificarRespuestaError(error) },
    });
  }

  actualizarListaEquipoProyecto(): void {
    this.personalService.obtenerEquipoProyecto(this.obtenerIdProyectoActual())
      .subscribe({
        next: (listaEquipo: Persona[]) => this.listaEquipoProyectoSubject.next(listaEquipo),
        error: (error: HttpErrorResponse) => this.notificacionService.notificarRespuestaError(error)
      });
  }

  agregarPersonaAEquipo(persona: Persona): void {
    this.personalService.agregarAEquipo(this.obtenerIdProyectoActual(), persona)
      .subscribe({
        next: (_) => { this.actualizarListaEquipoProyecto() },
        error: (error: HttpErrorResponse) => { this.notificacionService.notificarRespuestaError(error) }
      });
  }

  quitarPersonaDeEquipo(persona: Persona): void {
    this.personalService.quitarDeEquipo(persona)
      .subscribe({
        next: (_) => this.actualizarListaEquipoProyecto(),
        error: (error: HttpErrorResponse) => { this.notificacionService.notificarRespuestaError(error) },
      });
  }

  actualizarListaDatosBurnUp(): void {
    this.proyectoService.obtenerListaDatosBurnUpParaProyecto(this.obtenerIdProyectoActual())
      .subscribe({
        next: (datos: DatoBurnUp[]) => this.listaDatosBurnUpSubject.next(datos),
        error: (error: HttpErrorResponse) => { this.notificacionService.notificarRespuestaError(error) },
      });
  }

}
