import { TestBed } from '@angular/core/testing';

import { HistoriaUsuarioService } from './historia-usuario.service';

describe('HistoriaUsuarioService', () => {
  let service: HistoriaUsuarioService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HistoriaUsuarioService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
