import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { BehaviorSubject } from 'rxjs';
import { CabeceraBurndown } from '../model/cabecera-burndown';
import { HistoriaUsuario } from '../model/historia-usuario';
import { NotificacionService } from 'src/app/shared/services/notificacion.service';
import { Tablero } from 'src/app/taskboard/model/tablero';
import { Sprint } from '../model/sprint';
import { SprintService } from './sprint.service';

@Injectable({
  providedIn: 'root'
})
export class PanelSprintService {

  private sprintIdSubject = new BehaviorSubject<string>('');
  sprintIdObservable = this.sprintIdSubject.asObservable();

  private nombreSprintSubject = new BehaviorSubject<string>('');
  nombreSprintObservable = this.nombreSprintSubject.asObservable();

  private listaHistoriasSprintSubject = new BehaviorSubject<HistoriaUsuario[]>([]);
  listaHistoriasSprintObservable = this.listaHistoriasSprintSubject.asObservable();

  private datosBurndownSubject = new BehaviorSubject<CabeceraBurndown>(new CabeceraBurndown());
  datosBurndownObservable = this.datosBurndownSubject.asObservable();

  private tableroTareasSubject = new BehaviorSubject<Tablero>(new Tablero());
  tableroTareasObservable = this.tableroTareasSubject.asObservable();

  private sprintSubject = new BehaviorSubject<Sprint | null>(null);

  constructor(private sprintService: SprintService,
    private notificacionService: NotificacionService,
    private router: Router) { }

  establecerSprintId(sprintId: string): void {
    this.sprintIdSubject.next(sprintId);

    this.actualizarNombreSprint();
    this.actualizarListadoHistoriasUsuario();
  }

  private obtenerIdSprintActual(): string {
    return this.sprintIdSubject.getValue()
  }

  actualizarNombreSprint(): void {
    this.sprintService.obtenerSprintPorId(this.sprintIdSubject.getValue())
      .subscribe({
        next: (sprint: Sprint) => {
          this.nombreSprintSubject.next(sprint.nombre)
          this.sprintSubject.next(sprint);
        },
        error: (error: HttpErrorResponse) => { this.notificacionService.notificarRespuestaError(error) }
      });
  }

  actualizarListadoHistoriasUsuario(): void {
    this.sprintService.obtenerHistoriasParaSprint(this.sprintIdSubject.getValue())
      .subscribe({
        next: (lista: HistoriaUsuario[]) => this.listaHistoriasSprintSubject.next(lista),
        error: (error: HttpErrorResponse) => { this.notificacionService.notificarRespuestaError(error) },
      });

    this.actualizarDatosBurndown();
    this.actualizarTableroTareas();
  }

  agregarHistoriaASprint(historia: HistoriaUsuario): void {
    this.sprintService.agregarHistoriaASprint(this.obtenerIdSprintActual(), historia)
      .subscribe({
        next: (_) => this.actualizarListadoHistoriasUsuario(),
        error: (error: HttpErrorResponse) => { this.notificacionService.notificarRespuestaError(error) },
      });
  }

  quitarHistoriaDeSprint(historia: HistoriaUsuario): void {
    this.sprintService.quitarHistoriaDeSprint(this.obtenerIdSprintActual(), historia)
      .subscribe({
        next: (_) => this.actualizarListadoHistoriasUsuario(),
        error: (error: HttpErrorResponse) => { this.notificacionService.notificarRespuestaError(error) },
      });
  }

  actualizarDatosBurndown(): void {
    this.sprintService.obtenerBurndownParaSprint(this.sprintIdSubject.getValue())
      .subscribe({
        next: (dato: CabeceraBurndown) => this.datosBurndownSubject.next(dato),
        error: (error: HttpErrorResponse) => { this.notificacionService.notificarRespuestaError(error) },
      });
  }

  actualizarTableroTareas(): void {
    this.sprintService.obtenerTareasParaSprint(this.sprintIdSubject.getValue())
      .subscribe({
        next: (dato: Tablero) => this.tableroTareasSubject.next(dato),
        error: (error: HttpErrorResponse) => { this.notificacionService.notificarRespuestaError(error) },
      });
  }

  cambiarEstadoTarea(tarea: HistoriaUsuario, nuevoEstado: string): void {
    this.sprintService.cambiarEstadoTarea(this.sprintIdSubject.getValue(), tarea, nuevoEstado)
      .subscribe({
        next: (_) => {
          this.actualizarDatosBurndown();
          this.actualizarTableroTareas();
        },
        error: (error: HttpErrorResponse) => { this.notificacionService.notificarRespuestaError(error) }
      });
  }

  volver(): void {
    const idProyecto = this.sprintSubject.getValue()?.proyectoId;
    this.router.navigate(['proyectos', 'projectPanel'], { queryParams: { idProyecto } });
  }

}
