import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { JwtHelperService } from '@auth0/angular-jwt';
import { NotificacionService } from 'src/app/shared/services/notificacion.service';

import { Persona } from 'src/app/personal/model/persona';
import { CommunicationService } from 'src/app/shared/services/communication.service';
import { personalServiceUrl } from 'src/app/config/url.config';
import { BehaviorSubject, Observable } from 'rxjs';



@Injectable({
  providedIn: 'root'
})
export class AutenticacionService {

  private static readonly prefijoCabecera: string = "Bearer ";
  private token_actual: string = '';
  private jwtHelper = new JwtHelperService();

  constructor( private communicationService: CommunicationService,
    private notificacionService: NotificacionService,
    private router: Router) { }

  private get token(): string {
    if (!this.token_actual)
      this.token_actual = localStorage.getItem('token') || '';

    return this.token_actual;
  }

  public obtenerCabeceraAutenticacion(): string {
    return AutenticacionService.prefijoCabecera + this.token;
  }

  public logout(): void {
    this.token_actual = '';
    localStorage.removeItem('token');
    localStorage.removeItem('usuarioActual');
    this.navegarAPaginaLogin();
  }

  public login(persona: Persona) {
    this.communicationService.postWithResponse<Persona>(`${personalServiceUrl}/login`, persona).subscribe({
      next:(response: HttpResponse<Persona>) => {
        this.guardarUsuario(response.body);
        this.guardarToken(response.headers.get('Jwt_Token') || '');

        this.navegarAPaginaPrincipal();
      },
      error: (error: HttpErrorResponse) => {
         this.notificacionService.notificarRespuestaError(error);
      }
    });
  }

  public registrar(persona: Persona) {
    this.communicationService.save<Persona>(`${personalServiceUrl}/registrar`, persona).subscribe({
      next:(response: Persona) => {
        this.notificacionService.info('Usuario registrado correctamente');
      },
      error:(error: HttpErrorResponse) => {
        this.notificacionService.notificarRespuestaError(error);
      }
    });
  }


  private guardarToken(token: string): void {
    this.token_actual = token;
    localStorage.setItem('token', token);
  }

  private guardarUsuario(persona: Persona | null): void {
    if (persona) {
      localStorage.setItem('usuarioActual', JSON.stringify(persona));
    }
  }

  obtenerUsuarioRegistrado(): Persona {
    return JSON.parse(localStorage.getItem('usuarioActual') || '');
  }

  public hayUsuarioAutenticado(): boolean {
    if (this.token == null || this.token === '') {
      this.logout();
      return false;
    }

    const tokenDecodificado = this.jwtHelper.decodeToken(this.token);
    const esTokenEspirado = this.jwtHelper.isTokenExpired(this.token);

    if (tokenDecodificado.sub == null || tokenDecodificado.sub === '' || esTokenEspirado) {
      this.logout();
      return false;
    }
    return true;
  }

  public impedirMultiplesLogin() {
    if (this.hayUsuarioAutenticado())
      this.navegarAPaginaPrincipal();
  }

  private navegarAPaginaPrincipal(): void {
    this.router.navigateByUrl('/principal');
  }

  private navegarAPaginaLogin(): void {
    this.router.navigateByUrl('/login');
  }

  private obtenerPermisosDeToken(): string[] {
    const tokenDecodificado = this.jwtHelper.decodeToken(this.token);
    return tokenDecodificado.permisos;
  }

  static readonly PermisoVerUsuario = "personal:ver";
  static readonly PermisoAñadirUsuario = "personal:añadir";
  static readonly PermisoBorrarUsuario = "personal:borrar";

  static readonly PermisoVerProyecto = "proyecto:ver";
  static readonly PermisoAñadirProyecto = "proyecto:añadir";
  static readonly PermisoBorrarProyecto = "proyecto:borrar";

  public get puedeVerUsuarios(): boolean {
    return this.usuarioTienePermiso(AutenticacionService.PermisoVerUsuario);
  }

  public get puedeAñadirUsuarios(): boolean {
    return this.usuarioTienePermiso(AutenticacionService.PermisoAñadirUsuario);
  }

  public get puedeBorrarUsuarios(): boolean {
    return this.usuarioTienePermiso(AutenticacionService.PermisoBorrarUsuario);
  }

  public get puedeVerProyecto(): boolean {
    return this.usuarioTienePermiso(AutenticacionService.PermisoVerProyecto);
  }

  public get puedeAñadirProyecto(): boolean {
    return this.usuarioTienePermiso(AutenticacionService.PermisoAñadirProyecto);
  }

  public get puedeBorrarProyecto(): boolean {
    return this.usuarioTienePermiso(AutenticacionService.PermisoBorrarProyecto);
  }

  private usuarioTienePermiso(permiso: string): boolean {
    const permisos = this.obtenerPermisosDeToken();
    return permisos.indexOf(permiso) !== -1;
  }
}
