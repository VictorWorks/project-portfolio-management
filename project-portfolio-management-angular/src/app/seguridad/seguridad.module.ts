import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LogInComponent } from './components/log-in/log-in.component';
import { RegistroUsuarioComponent } from './components/registro-usuario/registro-usuario.component';
import { MaterialModule } from '../material/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { InicioComponent } from './pages/inicio/inicio.component';
import { HttpClientModule } from '@angular/common/http';




@NgModule({
  declarations: [
    InicioComponent,
    LogInComponent,
    RegistroUsuarioComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    HttpClientModule,
    MaterialModule,
    ReactiveFormsModule,
    RouterModule
  ],
  exports: [
    InicioComponent,
    LogInComponent,
    RegistroUsuarioComponent
  ]
})
export class SeguridadModule { }
