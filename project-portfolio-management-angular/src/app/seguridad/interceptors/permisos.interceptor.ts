import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';

import { AutenticacionService } from '../services/autenticacion.service';


@Injectable()
export class PermisosInterceptor implements HttpInterceptor {

  constructor(private autenticacionService: AutenticacionService) { }

  private rutasLibres: string[] = [
    '/personas/login',
    '/personas/registrar'
  ];

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    if (this.esRutaSinAutenticacion(request.url))
      return next.handle(request);

    const valorCabeceraAutenticacion = this.autenticacionService.obtenerCabeceraAutenticacion();
    const newRequest: HttpRequest<unknown> = request.clone({ setHeaders: { Authorization: valorCabeceraAutenticacion } });
    return next.handle(newRequest);
  }

  private esRutaSinAutenticacion(ruta: string): boolean {
    const found = this.rutasLibres.find(x => ruta.endsWith(x));
    return (found != null && found != undefined);
  }
}
