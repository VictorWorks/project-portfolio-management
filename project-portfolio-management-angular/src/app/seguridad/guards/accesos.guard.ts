import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router, CanLoad, Route, UrlSegment } from '@angular/router';
import { Injectable } from '@angular/core';

import { AutenticacionService } from '../services/autenticacion.service';
import { NotificacionService } from 'src/app/shared/services/notificacion.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AccesosGuard implements CanActivate, CanLoad {
  constructor(private autenticacionService: AutenticacionService,
    private router: Router,
    private notificacionService: NotificacionService) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot):  boolean {
    return this.hayUsuarioAutenticado();
  }

  canLoad(route: Route, segments: UrlSegment[]): boolean {
    return this.hayUsuarioAutenticado();
  }

  private hayUsuarioAutenticado(): boolean {
    if (this.autenticacionService.hayUsuarioAutenticado()) {
      return true;
    }

    this.notificacionService.error('Para acceder a la funcionalidad, debe iniciar sesión.')
    this.router.navigate(['/login']);
    return false;
  }

}
