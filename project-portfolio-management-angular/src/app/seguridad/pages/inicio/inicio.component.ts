import { Component, OnInit } from '@angular/core';

import { AutenticacionService } from 'src/app/seguridad/services/autenticacion.service';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css']
})
export class InicioComponent implements OnInit {

  constructor(private autenticacionService: AutenticacionService) { }

  ngOnInit(): void {
    this.autenticacionService.impedirMultiplesLogin();
  }

}
