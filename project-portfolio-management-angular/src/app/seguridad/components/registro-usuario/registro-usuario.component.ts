import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormGroupDirective, Validators } from '@angular/forms';

import { AutenticacionService } from '../../services/autenticacion.service';
import { CustomValidators } from 'src/app/shared/validators/custom-validators';
import { Persona } from 'src/app/personal/model/persona';

@Component({
  selector: 'app-registro-usuario',
  templateUrl: './registro-usuario.component.html',
  styleUrls: ['./registro-usuario.component.css']
})
export class RegistroUsuarioComponent implements OnInit {
  form: FormGroup = this.formBuilder.group({
    nombre: ['', Validators.required],
    username: ['', [Validators.required, Validators.minLength(5)]],
    password: ['', [Validators.required, Validators.minLength(4)]],
    passwordRepeat: ['', [Validators.required, Validators.minLength(4)]],
  },
    {
      validators: [this.customValidators.PasswordMustMatch],
      updateOn: 'blur',
    }
  );

  constructor(private formBuilder: FormBuilder,
    private customValidators: CustomValidators,
    private autenticacionService: AutenticacionService) { }

  ngOnInit(): void {

  }

  enviarDatos(formDirective: FormGroupDirective): void {
    if (this.form.invalid) {
      return;
    }

    const persona: Persona = new Persona(this.form.value);
    this.autenticacionService.registrar(persona);
    this.limpiarForm(formDirective);
  }

  public get password() {
    return this.form.controls['password'];
  }

  public get passwordRepeat() {
    return this.form.controls['passwordRepeat'];
  }

  private limpiarForm(formDirective: FormGroupDirective): void {
    formDirective.resetForm();
    this.form.reset();
  }
}
