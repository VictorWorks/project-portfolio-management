import { Component, OnInit } from '@angular/core';
import { Persona } from 'src/app/personal/model/persona';

import { AutenticacionService } from '../../services/autenticacion.service';

@Component({
  selector: 'app-log-in',
  templateUrl: './log-in.component.html',
  styleUrls: ['./log-in.component.css']
})
export class LogInComponent implements OnInit {
  constructor(private autenticacionService: AutenticacionService) { }

  ngOnInit(): void {
  }

  login(persona: Persona): void {
    this.autenticacionService.login(persona);
  }
}
