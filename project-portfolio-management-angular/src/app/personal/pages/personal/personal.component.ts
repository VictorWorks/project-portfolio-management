import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Guardable } from 'src/app/shared/model/guardable';
import { Persona } from '../../model/persona';
import { PersonalPageService } from '../../services/personal-page.service';


@Component({
  selector: 'app-personal',
  templateUrl: './personal.component.html',
  styleUrls: ['./personal.component.css']
})
export class PersonalComponent implements OnInit {

  constructor(private personalPageService: PersonalPageService) { }

  ngOnInit(): void {
    this.personalPageService.inicializarVista();
  }

  obtenerListaPersonal(): Observable<Persona[]> {
    return this.personalPageService.personalActual;
  }

  guardarPersona(persona: Guardable<Persona>): void {
    this.personalPageService.guardarPersona(persona);
  }

  borrarPersona(idPersona: string): void {
    this.personalPageService.borrarPersona(idPersona);
  }
}
