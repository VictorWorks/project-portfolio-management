import { Entidad } from "src/app/model/entidad";


export class Persona extends Entidad{
    nombre: string = '';
    proyectoId?:string = '';
    username: string = '';
    password: string = '';
    rol: string = '';

    constructor(json?: any){
        super();
        if (json == null) return;
        this.nombre = json.nombre;
        this.proyectoId = json.proyectoId;
        this.username = json.username;
        this.password = json.password;
    }
}
