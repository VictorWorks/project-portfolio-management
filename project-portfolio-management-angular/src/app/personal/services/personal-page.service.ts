import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

import { Guardable } from 'src/app/shared/model/guardable';
import { NotificacionService } from 'src/app/shared/services/notificacion.service';
import { Persona } from '../model/persona';
import { PersonalService } from '../services/personal.service';

@Injectable({
  providedIn: 'root'
})
export class PersonalPageService {

  private personalSubject = new BehaviorSubject<Persona[]>([]);
  personalActual = this.personalSubject.asObservable();

  constructor(private personalService: PersonalService,
    private notificacionService: NotificacionService
  ) { }

  inicializarVista(): void {
    this.actualizarListPersonal();
  }

  private actualizarListPersonal(): void {
    this.personalService.obtenerTodos().subscribe({
      next: (lista: Persona[]) => { this.personalSubject.next(lista); },
      error: (error: HttpErrorResponse) => { this.notificacionService.notificarRespuestaError(error) },
    });
  }

  guardarPersona(persona: Guardable<Persona>): void {
    const accionGuardado = persona.esNuevo ? this.personalService.guardarPersona(persona.datos) : this.personalService.actualizarPersona(persona.datos);
    accionGuardado.subscribe({
      next: (persona: Persona) => this.actualizarListPersonal(),
      error: (error: HttpErrorResponse) => { this.notificacionService.notificarRespuestaError(error) }
    });
  }

  borrarPersona(idPersona: string): void {
    this.personalService.borrarPersonaPorId(idPersona).subscribe({
      next: (persona: Persona) => this.actualizarListPersonal(),
      error: (error: HttpErrorResponse) => this.notificacionService.notificarRespuestaError(error)
    });
  }

}
