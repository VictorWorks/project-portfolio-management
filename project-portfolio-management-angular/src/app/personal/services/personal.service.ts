import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

import { CommunicationService } from 'src/app/shared/services/communication.service';
import { Persona } from '../model/persona';
import { personalServiceUrl, proyectoServiceUrl } from 'src/app/config/url.config';
import { UrlQuery } from 'src/app/shared/model/url-query';

@Injectable({
  providedIn: 'root'
})
export class PersonalService {

  constructor(private communicationService: CommunicationService) { }

  obtenerTodos(): Observable<Persona[]> {
    return this.communicationService.getAll(personalServiceUrl);
  }

  guardarPersona(persona: Persona): Observable<Persona> {
    return this.communicationService.save(personalServiceUrl, persona);
  }

  actualizarPersona(persona: Persona): Observable<Persona> {
    return this.communicationService.update(personalServiceUrl, new UrlQuery(persona.id), persona);
  }

  borrarPersonaPorId(id: string): Observable<Persona> {
    return this.communicationService.delete(personalServiceUrl, new UrlQuery(id));
  }

  obtenerEquipoProyecto(idProyecto: string): Observable<Persona[]> {
    const query = new UrlQuery(idProyecto).consultarRecurso('equipo');
    return this.communicationService.getAll(proyectoServiceUrl, query);
  }

  agregarAEquipo(idProyecto: string, persona: Persona): Observable<Persona> {
    persona.proyectoId = idProyecto;
    return this.actualizarPersona(persona);
  }

  quitarDeEquipo(persona: Persona): Observable<Persona> {
    persona.proyectoId = undefined;
    return this.actualizarPersona(persona);
  }

  obtenerPersonalDisponibleFiltrado(idProyecto: string, claveBusqueda: string): Observable<Persona[]> {
    if (!claveBusqueda) {
      return of([]);
    }

    const query = new UrlQuery()
      .consultarRecurso('claveNombre', claveBusqueda)
      .consultarRecurso('noPerteneciente', idProyecto);
    return this.communicationService.getAll(personalServiceUrl, query);
  }
}
