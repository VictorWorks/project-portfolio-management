import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { Sort } from '@angular/material/sort';
import { Observable, of } from 'rxjs';
import { AutenticacionService } from 'src/app/seguridad/services/autenticacion.service';
import { Guardable } from 'src/app/shared/model/guardable';
import { DialogService } from 'src/app/shared/services/dialog.service';
import { Persona } from '../../model/persona';
import { PersonalEditorComponent } from '../personal-editor/personal-editor.component';

@Component({
  selector: 'app-personal-list',
  templateUrl: './personal-list.component.html',
  styleUrls: ['./personal-list.component.css']
})
export class PersonalListComponent implements OnInit {

  @Input('listaPersonal') listaPersonalObservable: Observable<Persona[]> = of([]);
  @Output('onBorrarPersonal') eventoBorrarPersona = new EventEmitter<string>();
  @Output('onGuardarPersona') eventoGuardarPersona = new EventEmitter<Guardable<Persona>>();

  dataSource: Persona[] = [];
  personas: Persona[] = [];

  displayedColumns: string[] = ['nombre', 'actions'];
  clave: string = '';
  constructor(
    private popup: MatDialog,
    private dialogService: DialogService,
    private autenticacionService: AutenticacionService
  ) {
  }

  actualizarListaPersonas(lista: Persona[]): void {
    this.dataSource = [...lista];
    this.personas = lista;
  }

  ngOnInit(): void {
    this.listaPersonalObservable.subscribe((lista) => this.actualizarListaPersonas(lista));
  }

  sortData(sort: Sort): void {
    const data = this.dataSource.slice();
    if (!sort.active || sort.direction === '') {
      this.dataSource = data;
      return;
    }

    this.dataSource = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'nombre':
          return this.compare(a.nombre, b.nombre, isAsc);
        default:
          return 0;
      }
    });
  }

  private compare(a: string, b: string, isAsc: boolean): number {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }

  limpiarFiltro(): void {
    this.clave = '';
    this.filtrarEntradas();
  }

  filtrarEntradas(): void {
    if (this.clave) {
      this.dataSource = this.dataSource.filter(
        (x) => x.nombre.trim().toLocaleLowerCase().indexOf(this.clave.trim().toLocaleLowerCase()) >= 0
      );
    } else {
      this.dataSource = this.personas;
    }
  }
  nuevo(): void {
    this.lanzarFormulario(null);
  }

  editarEntrada(row: any): void {
    this.lanzarFormulario(row);
  }

  lanzarFormulario(dato: any): void {
    const config = new MatDialogConfig();
    config.disableClose = true;
    config.autoFocus = true;
    config.width = '60%';
    if (dato != null) config.data = dato;
    const ref = this.popup.open(PersonalEditorComponent, config);
    ref.componentInstance.eventoGuardarPersona.subscribe((dato) => this.eventoGuardarPersona.emit(dato));
    ref.afterClosed().subscribe((result) => {
    });
  }

  borrarEntrada(id: string): void {
    this.dialogService
      .abrirConfirmacion('Se va a borrar un registro. ¿Está seguro?')
      .subscribe((confirmacion) => {
        if (confirmacion) {
          this.eventoBorrarPersona.emit(id);
        }
      });
  }

  public get puedeAgregarUsuarios(): boolean {
    return this.autenticacionService.puedeAñadirUsuarios;
  }

  public get puedeBorrarUsuarios(): boolean {
    return this.autenticacionService.puedeBorrarUsuarios;
  }

  public get puedeVerUsuarios(): boolean {
    return this.autenticacionService.puedeVerUsuarios;
  }

}
