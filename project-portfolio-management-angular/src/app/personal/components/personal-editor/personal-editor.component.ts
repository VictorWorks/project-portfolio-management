import { Component, Inject, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { Guardable } from 'src/app/shared/model/guardable';
import { CustomValidators } from 'src/app/shared/validators/custom-validators';
import { Persona } from '../../model/persona';

@Component({
  selector: 'app-personal-editor',
  templateUrl: './personal-editor.component.html',
  styleUrls: ['./personal-editor.component.css']
})
export class PersonalEditorComponent implements OnInit {
  @Output('onGuardarPersona') eventoGuardarPersona = new EventEmitter<Guardable<Persona>>();

  form: FormGroup = this.formBuilder.group({
    nombre: ['', Validators.required],
    username: ['', [Validators.required, Validators.minLength(5)]],
    password: ['', [Validators.required, Validators.minLength(4)]],
    passwordRepeat: ['', [Validators.required, Validators.minLength(4)]],
    rol: ['DESARROLLADOR', [Validators.required, this.customValidators.esRolValido]]
  },
    {
      validators: [this.customValidators.PasswordMustMatch],
      updateOn: 'blur',
    }
  );

  constructor(
    public dialogRef: MatDialogRef<PersonalEditorComponent>,
    private formBuilder: FormBuilder,
    private customValidators: CustomValidators,
    @Inject(MAT_DIALOG_DATA) public data: Persona
  ) {
    this.inicializarDatosFormulario();
    if (data != null) {
      this.form.reset(data);
      this.form.get('password')?.disable();
      this.form.get('passwordRepeat')?.disable();
    }
  }


  ngOnInit(): void { }

  limpiarForm(): void {
    this.form.reset();
  }

  inicializarDatosFormulario(): void {
    this.form.reset({
      nombre: '',
      username: '',
      password: '',
      passwordRepeat: '',
      rol: 'DESARROLLADOR'
    });
  }

  private getAllValidationErrors(): any[] {
    const result: any[] = [];

    Object.keys(this.form.controls).forEach(key => {

      const controlErrors: ValidationErrors | null | undefined = this.form.get(key)?.errors;
      if (controlErrors) {
        Object.keys(controlErrors).forEach(keyError => {
          result.push({
            'control': key,
            'error': keyError,
            'value': controlErrors[keyError]
          })
        });
      }
    });
    return result;
  }

  enviarDatos(): void {
    const datosAGuardar = this.actualizarDatosPersona();
    const personaAGuardar = new Guardable(!this.data, datosAGuardar);
    this.eventoGuardarPersona.emit(personaAGuardar);

    this.cerrarPopUp();
  }

  actualizarDatosPersona(): Persona {
    let persona = this.data;
    if (!this.data) {
      persona = new Persona();
    }

    persona.nombre = this.form.get('nombre')?.value;
    persona.username = this.form.get('username')?.value
    persona.rol = this.form.get('rol')?.value;
    if (this.form.get('password')?.enabled) {
      persona.password = this.form.get('password')?.value;
    }
    return persona;
  }

  esNuevoUsuario(): boolean {
    return this.data == null;
  }

  cerrarPopUp(): void {
    this.dialogRef.close();
  }

}
