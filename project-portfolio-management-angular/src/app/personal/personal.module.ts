import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PersonalRoutingModule } from './personal-routing.module';
import { PersonalComponent } from './pages/personal/personal.component';
import { MaterialModule } from '../material/material.module';
import { SharedModule } from '../shared/shared.module';
import { PersonalEditorComponent } from './components/personal-editor/personal-editor.component';
import { PersonalListComponent } from './components/personal-list/personal-list.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    PersonalComponent,
    PersonalEditorComponent,
    PersonalListComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule,
    PersonalRoutingModule,
    ReactiveFormsModule,
    SharedModule
  ],
  exports: [
    PersonalComponent
  ]
})
export class PersonalModule { }
