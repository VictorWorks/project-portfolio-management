import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ColumnaListComponent } from './columna-list.component';

describe('ColumnaListComponent', () => {
  let component: ColumnaListComponent;
  let fixture: ComponentFixture<ColumnaListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ColumnaListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ColumnaListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
