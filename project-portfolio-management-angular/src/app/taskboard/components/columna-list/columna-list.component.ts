import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DialogService } from 'src/app/shared/services/dialog.service';

import { ColumnaTablero } from '../../model/columna-tablero';
import { TaskboardService } from '../../services/taskboard.service';

@Component({
  selector: 'app-columna-list',
  templateUrl: './columna-list.component.html',
  styleUrls: ['./columna-list.component.css']
})
export class ColumnaListComponent implements OnInit {
  estados: ColumnaTablero[] = [];
  proyectoId: string = '';

  constructor(
    private taskboardService: TaskboardService,
    private popup: MatDialog,
    private dialogService: DialogService
  ) {}
  ngOnInit(): void {
    this.actualizarListaColumnas();
  }

  actualizarListaColumnas(): void {
    this.taskboardService
      .obtenerColumnasProyecto(this.proyectoId)
      .subscribe((x) => (this.estados = x));
  }

  borrarEntrada(id: string): void {
    this.dialogService
      .abrirConfirmacion('Se va a borrar un registro. ¿Está seguro?')
      .subscribe((confirmacion: boolean) => {
        if (confirmacion) {
          this.taskboardService.borrarColumnaPorId(id);
          this.actualizarListaColumnas();
        }
      });
  }

  // nuevo(): void {
  //   this.lanzarFormulario(null);
  // }

  // editarEntrada(row): void {
  //   this.lanzarFormulario(row);
  // }

  // lanzarFormulario(dato): void {
  //   const config = new MatDialogConfig();
  //   config.disableClose = true;
  //   config.autoFocus = true;
  //   config.width = '60%';
  //   if (dato != null) config.data = dato;
  //   const ref = this.popup.open(ColumnaEditorComponent, config);
  //   ref.afterClosed().subscribe((result) => {
  //     this.actualizarListaColumnas();
  //   });
  // }

  drop(event: CdkDragDrop<ColumnaTablero[]>): void {
    if (event.previousContainer === event.container) {
      moveItemInArray(
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
      this.actualizarOrdenEstados();
    }
  }

  private actualizarOrdenEstados(): void {
     const pendienteActualizar = this.estados.filter(x => x.orden !== this.estados.indexOf(x) + 1);
     pendienteActualizar.forEach(x => {
      x.orden = this.estados.indexOf(x) + 1;
      this.taskboardService.actualizarColumna(x);
     });
  }
}
