import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import {
  CdkDragDrop,
  moveItemInArray,
  transferArrayItem,
} from '@angular/cdk/drag-drop';
import { Observable, of, Subscription } from 'rxjs';

import { ColumnaTablero } from '../../model/columna-tablero';
import { HistoriaUsuario } from 'src/app/proyecto/model/historia-usuario';
import { Tablero } from '../../model/tablero';

@Component({
  selector: 'app-taskboard',
  templateUrl: './taskboard.component.html',
  styleUrls: ['./taskboard.component.css']
})
export class TaskboardComponent implements OnInit {

  @Input('tablero') tableroObservable: Observable<Tablero | null> = of(null);
  @Output('onHistoriaMovida') eventoMoverHistoria = new EventEmitter<any>();
  susbcripcionTablero: Subscription | null = null;

  tablero: Tablero | null = null;
  nombreTablero: string = '';
  listaColumnas: ColumnaTablero[] = [];

  constructor() {
  }

  ngOnInit(): void {
    this.susbcripcionTablero = this.tableroObservable.subscribe(datos => {
      if (datos) {
        this.inicializarDatosTablero(datos);
      }
    });
  }

  ngOnDestroy(): void {
    this.susbcripcionTablero?.unsubscribe();

  }

  private inicializarDatosTablero(datos: Tablero): void {
    if (!datos)
      return;
    this.tablero = datos
    this.nombreTablero = datos.nombre;
    this.listaColumnas = datos.columnas;
  }

  drop(event: CdkDragDrop<HistoriaUsuario[]>, column: ColumnaTablero): void {
    if (event.previousContainer === event.container) {
      moveItemInArray(
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    } else {
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    }
    const datosEvento = {
      tareaCambiada: event.container.data[event.currentIndex],
      nuevoEstado: column.nombre
    }

    this.eventoMoverHistoria.emit(datosEvento);
  }

}
