import { Entidad } from "src/app/model/entidad";
import { HistoriaUsuario } from "src/app/proyecto/model/historia-usuario";


export class ColumnaTablero extends Entidad {
  idProyecto: string = '';
  nombre: string = '';
  esInicial: boolean = false;
  esFinal: boolean = false;
  orden: number = 0;
  tareas: HistoriaUsuario[] = [];
}
