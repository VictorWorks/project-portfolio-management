import { ColumnaTablero } from "./columna-tablero";

export class Tablero{
  nombre: string = '';
  columnas: ColumnaTablero[] = [];
}
