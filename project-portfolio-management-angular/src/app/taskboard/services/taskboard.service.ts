import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

import { ColumnaTablero } from '../model/columna-tablero';
import { HistoriaUsuario } from 'src/app/proyecto/model/historia-usuario';
import { Tablero } from '../model/tablero';

@Injectable({
  providedIn: 'root'
})
export class TaskboardService {
  board: Tablero | null = null;

  constructor() {
    this.buildBoard();
  }

  tasks = [
    this.createTarea('Tarea en Espera 3', 'A la espera', 3),
    this.createTarea('Tarea en Desarrollo 1', 'En desarrollo', 1),
    this.createTarea('Tarea terminada 2', 'Terminado', 2),
    this.createTarea('Tarea QA 1', 'QA', 1),
    this.createTarea('Tarea en Espera 2', 'A la espera', 2),
    this.createTarea('Tarea en Desarrollo 2', 'En desarrollo', 2),
    this.createTarea('Tarea QA 2', 'QA', 2),
    this.createTarea('Tarea terminada 1', 'Terminado', 1),
    this.createTarea('Tarea en espera 1', 'A la espera', 1),
  ];

  private createTarea(nombre: string, estado: string, orden: number): HistoriaUsuario {
    const result = new HistoriaUsuario();
    result.nombre = nombre;
    result.estadoActual = estado;
    result.prioridad = orden;
    return result;
  }

  private buildBoard() {
    // TODO: Si se van a poder definir columnas por tablero,
    // se tienen que poder asignar un orden
    const waitingColumn = this.getColumnForState(this.tasks, 'A la espera');
    const workingColumn = this.getColumnForState(this.tasks, 'En desarrollo');
    const qaColumn = this.getColumnForState(this.tasks, 'QA');
    const doneColumn = this.getColumnForState(this.tasks, 'Terminado');

    this.board = new Tablero();
    this.board.nombre = 'Test Board Component';
    this.board.columnas = [waitingColumn, workingColumn, qaColumn, doneColumn];
  }

  private getColumnForState(tasks: HistoriaUsuario[], status: string) {
    const columnTasks = tasks
      .filter((x) => x.estadoActual?.normalize() === status)
      .sort((y, z) => y.prioridad - z.prioridad);

    const result = new ColumnaTablero();
    result.nombre = status;
    result.tareas = columnTasks;
    return result;
  }

  getAll(): Observable<Tablero> {
    if (this.board)
      return of(this.board);

    return of(new Tablero());
  }

  updateBoard(
    updatedTask: HistoriaUsuario,
    origList: HistoriaUsuario[],
    origIndex: number,
    destList: HistoriaUsuario[],
    newIndex: number
  ) {
    throw new Error('Method not implemented.');
  }

  private columnas: ColumnaTablero[] = [
    this.crearColumna('1', 'Por hacer', 1, true),
    this.crearColumna('a', 'A la espera', 1, true),
    this.crearColumna('a', 'En desarrollo', 2),
    this.crearColumna('a', 'QA', 3),
    this.crearColumna('a', 'Terminado', 4, false, true),
    this.crearColumna('1', 'Hecho', 2, false, true),
  ];

  private crearColumna(
    idProyecto: string,
    nombre: string,
    orden: number,
    inicial: boolean = false,
    final: boolean = false
  ): ColumnaTablero {
    const columna = new ColumnaTablero();
    columna.esFinal = final;
    columna.esInicial = inicial;
    columna.idProyecto = idProyecto;
    columna.nombre = nombre;
    columna.orden = orden;
    return columna;
  }

  obtenerColumnasProyecto(idProyecto: string): Observable<ColumnaTablero[]> {
    const columnas = this.columnas
      .filter((x) => x.idProyecto === idProyecto)
      .sort((a, b) => a.orden - b.orden);
    return of(columnas);
  }

  guardarColumna(columna: ColumnaTablero) {
    this.columnas.push(columna);
  }

  private obtenerIndiceElemento(id: string): number {
    const existing = this.columnas.find((x) => x.id === id);

    if (existing)
      return this.columnas.indexOf(existing);

    return -1;
  }

  actualizarColumna(columna: ColumnaTablero) {
    const index = this.obtenerIndiceElemento(columna.id);
    if (index !== -1) {
      this.columnas[index] = columna;
    }
  }

  borrarColumnaPorId(id: string) {
    const index = this.obtenerIndiceElemento(id);
    if (index !== -1) {
      this.columnas.splice(index, 1);
    }
  }
}
