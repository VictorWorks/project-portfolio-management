import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ColumnaListComponent } from './components/columna-list/columna-list.component';
import { TaskboardComponent } from './components/taskboard/taskboard.component';
import { MaterialModule } from '../material/material.module';
import { DragDropModule } from '@angular/cdk/drag-drop';



@NgModule({
  declarations: [
    ColumnaListComponent,
    TaskboardComponent
  ],
  imports: [
    CommonModule,
    DragDropModule,
    MaterialModule
  ],
  exports: [
    TaskboardComponent
  ]
})
export class TaskboardModule { }
