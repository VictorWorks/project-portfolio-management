import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LogInComponent } from './seguridad/components/log-in/log-in.component';
import { NotFoundComponent } from './shared/pages/not-found/not-found.component';
import { PanelPrincipalComponent } from './shared/pages/panel-principal/panel-principal.component';
import { RegistroUsuarioComponent } from './seguridad/components/registro-usuario/registro-usuario.component';
import { AccesosGuard } from './seguridad/guards/accesos.guard';



const routes: Routes = [
  { path: 'login', component: LogInComponent },
  { path: 'registro', component: RegistroUsuarioComponent },
  { path: 'principal', component: PanelPrincipalComponent, canActivate:[AccesosGuard] },
  {
    path: 'personal',
    canActivate:[AccesosGuard],
    canLoad: [AccesosGuard],
    loadChildren: () => import('./personal/personal.module').then(m => m.PersonalModule)
  },
  {
    path: 'proyectos',
    canActivate:[AccesosGuard],
    canLoad: [AccesosGuard],
    loadChildren: () => import('./proyecto/proyecto.module').then(m => m.ProyectoModule)
  },
  { path: '', redirectTo: 'principal', pathMatch: 'full' },
  { path: '**', component: NotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
