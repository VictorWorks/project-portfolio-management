import { environment } from "src/environments/environment";

export const personalServiceUrl: string = `${environment.apiUrl}/${environment.apiVersion}/${environment.personalService}`;
export const proyectoServiceUrl = `${environment.apiUrl}/${environment.apiVersion}/${environment.proyectoService}`;
export const sprintServiceUrl = `${environment.apiUrl}/${environment.apiVersion}/${environment.sprintService}`;
export const backlogServiceUrl = `${environment.apiUrl}/${environment.apiVersion}/${environment.backlogService}`;
