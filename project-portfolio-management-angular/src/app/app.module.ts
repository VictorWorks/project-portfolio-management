import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MaterialModule } from './material/material.module';
import { PermisosInterceptor } from './seguridad/interceptors/permisos.interceptor';
import { SeguridadModule } from './seguridad/seguridad.module';
import { SharedModule } from './shared/shared.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    AppRoutingModule,
    HttpClientModule,
    MaterialModule,
    BrowserAnimationsModule,
    BrowserModule,
    RouterModule,
    SharedModule,
    SeguridadModule
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: PermisosInterceptor, multi: true}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
