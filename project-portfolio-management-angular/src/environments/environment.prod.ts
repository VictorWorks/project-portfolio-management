export const environment = {
  production: true,
  apiUrl: 'http://localhost:4200',
  apiVersion: 'v1',
  personalService:  'personas',
  proyectoService: 'proyectos',
  sprintService: 'sprints',
  backlogService: 'historias',
};
