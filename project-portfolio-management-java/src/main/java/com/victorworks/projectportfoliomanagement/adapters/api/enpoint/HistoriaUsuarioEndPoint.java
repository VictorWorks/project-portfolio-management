package com.victorworks.projectportfoliomanagement.adapters.api.enpoint;

import com.victorworks.projectportfoliomanagement.adapters.api.request.HistoriaUsuarioRequest;
import com.victorworks.projectportfoliomanagement.domain.model.entities.HistoriaUsuario;
import com.victorworks.projectportfoliomanagement.domain.model.entities.HistoriaUsuarioId;
import com.victorworks.projectportfoliomanagement.domain.model.entities.SprintId;
import com.victorworks.projectportfoliomanagement.domain.ports.servicio.HistoriaUsuarioService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("v1/historias")
public class HistoriaUsuarioEndPoint {
    private final HistoriaUsuarioService historias;

    public HistoriaUsuarioEndPoint(HistoriaUsuarioService historiaUsuarioService){
        historias = historiaUsuarioService;
    }

    @GetMapping()
    @PreAuthorize("hasAuthority('proyecto:ver')")
    public List<HistoriaUsuarioRequest> todos() {
        List<HistoriaUsuario> encontradas = historias.todos();
        return MapearARequest(encontradas);
    }

    @GetMapping("claveNombre/{claveNombre}/noPerteneciente/{idSprint}")
    @PreAuthorize("hasAuthority('proyecto:ver')")
    public List<HistoriaUsuarioRequest> obtenerHistoriaNoIncluidaEnSprintConNombreComo(@PathVariable("claveNombre") String claveNombre,
                                                                                       @PathVariable("idSprint") String idSprint) {
        List<HistoriaUsuario> encontradas = historias.obtenerHistoriaNoIncluidaEnSprintConNombreComo(claveNombre, SprintId.desde(idSprint));
        return MapearARequest(encontradas);
    }

    @PostMapping()
    @PreAuthorize("hasAuthority('proyecto:ver')")
    public HistoriaUsuarioRequest crear(@RequestBody HistoriaUsuarioRequest historia){
        HistoriaUsuario nueva = historias.crear(historia.haciaModelo());
        return HistoriaUsuarioRequest.desde(nueva);
    }

    @PutMapping("{historiaId}")
    @PreAuthorize("hasAuthority('proyecto:ver')")
    void actualizar(@PathVariable("historiaId") final String historiaId, @RequestBody final HistoriaUsuarioRequest historia) {
        historias.actualizar(HistoriaUsuarioId.desde(historiaId), historia.haciaModelo());
    }

    @DeleteMapping("{historiaId}")
    @PreAuthorize("hasAuthority('proyecto:ver')")
    void borrar(@PathVariable("historiaId") final String historiaId) {
        historias.borrar(HistoriaUsuarioId.desde(historiaId));
    }

    private List<HistoriaUsuarioRequest> MapearARequest(List<HistoriaUsuario> historias){
        return historias.stream()
                .map(x -> HistoriaUsuarioRequest.desde(x))
                .collect(Collectors.toList());
    }
}
