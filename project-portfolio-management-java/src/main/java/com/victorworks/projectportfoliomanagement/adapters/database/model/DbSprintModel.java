package com.victorworks.projectportfoliomanagement.adapters.database.model;


import com.victorworks.projectportfoliomanagement.domain.model.entities.ProyectoId;
import com.victorworks.projectportfoliomanagement.domain.model.entities.Sprint;
import com.victorworks.projectportfoliomanagement.domain.model.entities.SprintId;
import com.victorworks.projectportfoliomanagement.utils.Dates;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "Sprint")
public class DbSprintModel {

    @Id
    private String id;
    private String nombre;
    private String fechaInicio;
    private String fechaFin;
    private int puntosHistoria;
    private String proyectoId;

    @OneToMany(mappedBy = "sprint", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<DbHistoriaEnSprintModel> historiasEnSprint;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(String fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public String getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(String fechaFin) {
        this.fechaFin = fechaFin;
    }

    public int getPuntosHistoria() {
        return puntosHistoria;
    }

    public void setPuntosHistoria(int puntosHistoria) {
        this.puntosHistoria = puntosHistoria;
    }

    public String getProyectoId() {
        return proyectoId;
    }

    public void setProyectoId(String proyectoId) {
        this.proyectoId = proyectoId;
    }

    public  Set<DbHistoriaEnSprintModel> getHistoriasEnSprint(){
        return  this.historiasEnSprint;
    }

    public void setHistoriasEnSprint(Set<DbHistoriaEnSprintModel> historias) {
        this.historiasEnSprint = historias;
    }

    public DbSprintModel() {
    }

    public DbSprintModel(String id, String nombre, String fechaInicio,
                         String fechaFin, int puntosHistoria, String proyectoId) {
        this.id = id;
        this.nombre = nombre;
        this.fechaInicio = fechaInicio;
        this.fechaFin = fechaFin;
        this.puntosHistoria = puntosHistoria;
        this.proyectoId = proyectoId;
    }

    public DbSprintModel(String id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }

    public static DbSprintModel desde(Sprint sprint) {
        String proyectoId = null;
        if (sprint.getProyectoId() != null)
            proyectoId = sprint.getProyectoId().toString();
        return new DbSprintModel(sprint.getId().toString(), sprint.getNombre(),
                Dates.formatearFecha(sprint.getFechaInicio()), Dates.formatearFecha(sprint.getFechaFin()),
                sprint.getPuntosHistoria(), proyectoId);
    }

    public Sprint haciaModelo() {
        ProyectoId proyectoIdModelo = null;
        if (proyectoId != null)
            proyectoIdModelo = ProyectoId.desde(proyectoId);
        return new Sprint(SprintId.desde(id), nombre, Dates.parseFecha(fechaInicio),
                Dates.parseFecha(fechaFin), puntosHistoria, proyectoIdModelo);
    }
}
