package com.victorworks.projectportfoliomanagement.adapters.database.dao;

import com.victorworks.projectportfoliomanagement.adapters.database.model.DbColumnaTableroModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface ColumnaTableroDAO extends JpaRepository<DbColumnaTableroModel, String>,  JpaSpecificationExecutor<DbColumnaTableroModel> {
}
