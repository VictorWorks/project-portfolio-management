package com.victorworks.projectportfoliomanagement.adapters.database.model;

import com.victorworks.projectportfoliomanagement.domain.model.entities.ColumnaTablero;
import com.victorworks.projectportfoliomanagement.domain.model.entities.ProyectoId;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

@Entity
@Table(name = "ColumnaKanban")
public class DbColumnaTableroModel {
    @Id
    private String id;

    private String proyectoId;
    private String nombre;
    private boolean esInicial;
    private boolean esFinal;
    private int orden;

    public DbColumnaTableroModel() {}

    public DbColumnaTableroModel(String id, String proyectoId, String nombre, boolean esInicial,
                                 boolean esFinal, int orden) {
        if (id == null || id == "")
            id = UUID.randomUUID().toString();
        this.id = id;
        this.proyectoId = proyectoId;
        this.nombre = nombre;
        this.esInicial = esInicial;
        this.esFinal = esFinal;
        this.orden = orden;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProyectoId() {
        return proyectoId;
    }

    public void setProyectoId(String proyectoId) {
        this.proyectoId = proyectoId;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public boolean getEsInicial() {
        return esInicial;
    }

    public void setEsInicial(boolean esInicial) {
        this.esInicial = esInicial;
    }

    public boolean getEsFinal() {
        return esFinal;
    }

    public void setEsFinal(boolean esFinal) {
        this.esFinal = esFinal;
    }

    public int getOrden() {
        return orden;
    }

    public void setOrden(int orden) {
        this.orden = orden;
    }

    public ColumnaTablero haciaModelo() {
        ColumnaTablero columna = new ColumnaTablero(ProyectoId.desde(this.proyectoId), this.nombre,
                this.esInicial, this.esFinal, this.orden);

        return columna;
    }
}
