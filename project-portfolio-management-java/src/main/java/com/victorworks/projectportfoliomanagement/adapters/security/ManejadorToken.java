package com.victorworks.projectportfoliomanagement.adapters.security;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.victorworks.projectportfoliomanagement.adapters.security.constante.ConstantesSeguridad;
import com.victorworks.projectportfoliomanagement.adapters.security.entidad.UserPrincipal;
import com.victorworks.projectportfoliomanagement.utils.Strings;
import org.springframework.beans.factory.annotation.Value;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

import static com.auth0.jwt.algorithms.Algorithm.HMAC512;
import static java.util.Arrays.stream;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class ManejadorToken {
    @Value("${clave.token}")
    private String claveToken;

    public String generarToken(UserPrincipal userPrincipal) {
        String[] permisos = obtenerPermisosParaUsuario(userPrincipal);
        Date fechaEmision = new Date();
        Date fechaCaducidad = new Date(System.currentTimeMillis() + ConstantesSeguridad.TIEMPO_EXPIRACION_TOKEN);
        return JWT.create()
                .withIssuer(ConstantesSeguridad.EMISOR_TOKEN)
                .withAudience(ConstantesSeguridad.AUDIENCIA_TOKEN)
                .withIssuedAt(fechaEmision)
                .withExpiresAt(fechaCaducidad)
                .withSubject(userPrincipal.getUsername())
                .withArrayClaim(ConstantesSeguridad.PERMISOS, permisos)
                .sign(HMAC512(claveToken.getBytes()));
    }

    public List<GrantedAuthority> obtenerPermisos(String token) {
        String[] permisos = obternerPermisosDesdeToken(token);
        return stream(permisos).map(SimpleGrantedAuthority::new).collect(Collectors.toList());
    }

    public Authentication obtenerAutenticacion(String username, List<GrantedAuthority> authorities, HttpServletRequest request) {
        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken =
                new UsernamePasswordAuthenticationToken(username, null, authorities);
        usernamePasswordAuthenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
        return usernamePasswordAuthenticationToken;
    }

    public boolean esTokenValido(String username, String token) {
        JWTVerifier verificador = obtenerVerificador();
        return !Strings.esCadenaVacia(username) && !isTokenExpired(verificador, token);
    }

    public String obtenerUsuarioToken(String token) {
        JWTVerifier verificador = obtenerVerificador();
        DecodedJWT decoded = verificador.verify(token);
        return verificador.verify(token).getSubject();
    }

    private boolean isTokenExpired(JWTVerifier verificador, String token) {
        Date fechaExpiracion = verificador.verify(token).getExpiresAt();
        return fechaExpiracion.before(new Date());
    }

    private String[] obternerPermisosDesdeToken(String token) {
        JWTVerifier verificador = obtenerVerificador();
        return verificador.verify(token)
                .getClaim(ConstantesSeguridad.PERMISOS)
                .asArray(String.class);
    }

    private JWTVerifier obtenerVerificador() {
        JWTVerifier verificador;
        try{
            Algorithm algoritmo = HMAC512(claveToken.getBytes());
            verificador = JWT.require(algoritmo)
                    .withIssuer(ConstantesSeguridad.EMISOR_TOKEN)
                    .build();
        }catch (JWTVerificationException exception) {
            throw new JWTVerificationException(ConstantesSeguridad.ERROR_VERIFICANDO_TOKEN);
        }
        return verificador;
    }

    private String[] obtenerPermisosParaUsuario(UserPrincipal userPrincipal) {
        List<String> permisos = new ArrayList<>();
        for (GrantedAuthority permisoUsuario: userPrincipal.getAuthorities()) {
            permisos.add(permisoUsuario.getAuthority());
        }
        return permisos.toArray(new String[0]);
    }
}
