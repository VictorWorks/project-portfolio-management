package com.victorworks.projectportfoliomanagement.adapters.database.model;

import com.victorworks.projectportfoliomanagement.domain.model.entities.Proyecto;
import com.victorworks.projectportfoliomanagement.domain.model.entities.ProyectoId;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Proyecto")
public class DbProyectoModel {
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Id
    String id;
    String nombre;

    public DbProyectoModel() {

    }
    public DbProyectoModel(String id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }

    public static DbProyectoModel desde(Proyecto proyecto){
        return new DbProyectoModel(proyecto.getId().toString(), proyecto.getNombre());
    }

    public Proyecto haciaModelo()
    {
        return new Proyecto(ProyectoId.desde(id), nombre);
    }
}
