package com.victorworks.projectportfoliomanagement.adapters.database.repository;

import com.victorworks.projectportfoliomanagement.adapters.database.dao.HistoriaEnSprintDAO;
import com.victorworks.projectportfoliomanagement.adapters.database.dao.HistoriaUsuarioDAO;
import com.victorworks.projectportfoliomanagement.adapters.database.model.DbHistoriaUsuarioModel;
import com.victorworks.projectportfoliomanagement.domain.model.entities.*;
import com.victorworks.projectportfoliomanagement.domain.ports.repositorio.HistoriaUsuarioRepository;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.springframework.data.jpa.domain.Specification.where;

@Repository
public class DbHistoriaUsuarioRepository implements HistoriaUsuarioRepository {

    private final HistoriaUsuarioDAO historiaUsuarioDAO;
    private final HistoriaEnSprintDAO historiaEnSprintDAO;

    public DbHistoriaUsuarioRepository(HistoriaUsuarioDAO historiaUsuarioDAO,
                                       HistoriaEnSprintDAO historiaEnSprintDAO) {
        this.historiaUsuarioDAO = historiaUsuarioDAO;
        this.historiaEnSprintDAO = historiaEnSprintDAO;
    }

    @Override
    public List<HistoriaUsuario> todos() {
        List<DbHistoriaUsuarioModel> historiasUsuariosModel = historiaUsuarioDAO.findAll();
        return convertirListaHaciaModelo(historiasUsuariosModel);
    }

    @Override
    public List<HistoriaUsuario> porProyecto(ProyectoId proyectoId) {
        final String idProyecto = proyectoId == null ? null : proyectoId.toString();
        List<DbHistoriaUsuarioModel> historiasModel = historiaUsuarioDAO.findAll(
                Specification.where(proyectoEs(idProyecto))
        );
        return convertirListaHaciaModelo(historiasModel);
    }

    @Override
    public List<HistoriaUsuario> porSprint(SprintId sprintId) {
        final String idSprint = sprintId == null ? null : sprintId.toString();
        List<DbHistoriaUsuarioModel> historiasModel = historiaUsuarioDAO.findAll(
                Specification.where(sprintEs(idSprint))
        );
        return convertirListaHaciaModelo(historiasModel);
    }

    @Override
    public List<HistoriaUsuario> noIncluidosEnSprintYNombreComo(SprintId sprintId, String claveNombre) {
        final String idSprint = sprintId == null ? null : sprintId.toString();

        List<String> idHistoriasActuales = porSprint(sprintId).stream().map(h -> h.getId().toString()).collect(Collectors.toList());
        List<DbHistoriaUsuarioModel> historiasModel = historiaUsuarioDAO.findAll(
                where(nombreComo(claveNombre).and(noEstaCompletada()).and(idDistinto(idHistoriasActuales)))
        );


        return convertirListaHaciaModelo(historiasModel);
    }

    @Override
    public HistoriaUsuario obtenerPorId(HistoriaUsuarioId id) {
        DbHistoriaUsuarioModel dbHistoria = obtenerPorId(id.toString());
        if (dbHistoria != null) {
            return dbHistoria.haciaModelo();
        }
        return null;
    }

    @Override
    public HistoriaUsuario crear(HistoriaUsuario historia) {
        final DbHistoriaUsuarioModel dbPersona = DbHistoriaUsuarioModel.desde(historia);
        final DbHistoriaUsuarioModel guardado = historiaUsuarioDAO.save(dbPersona);
        return guardado.haciaModelo();
    }

    @Override
    public void actualizar(HistoriaUsuarioId historiaId, HistoriaUsuario historia) {
        if (obtenerPorId(historiaId.toString()) != null) {
            DbHistoriaUsuarioModel dbHistoria = DbHistoriaUsuarioModel.desde(historia);
            historiaUsuarioDAO.save(dbHistoria);
        }
    }

    @Override
    public void borrar(HistoriaUsuarioId historiaId) {
        if (obtenerPorId(historiaId.toString()) != null) {
            historiaUsuarioDAO.deleteById(historiaId.toString());
        }
    }

    private DbHistoriaUsuarioModel obtenerPorId(String id) {
        Optional<DbHistoriaUsuarioModel> dbHistoria = historiaUsuarioDAO.findById(id);
        return dbHistoria.orElse(null);
    }

    private Specification<DbHistoriaUsuarioModel> proyectoEs(String proyectoId) {
        return (root, query, cb) -> cb.equal(root.get("proyectoId"), proyectoId);
    }

    private Specification<DbHistoriaUsuarioModel> sprintEs(String sprintId) {
        return (root, query, cb) -> cb.equal(root.join("sprints").get("sprint").get("id"), sprintId);
    }

    private Specification<DbHistoriaUsuarioModel> noEstaCompletada() {
        return (root, query, cb) -> cb.notEqual(root.get("estadoActual"), EstadoTarea.Terminado.toString());
    }

    private Specification<DbHistoriaUsuarioModel> nombreComo(String claveNombre) {
        return (root, query, cb) -> cb.like(cb.lower(root.get("nombre")), "%" + claveNombre.toLowerCase(Locale.ROOT) + "%");
    }

    private Specification<DbHistoriaUsuarioModel> idDistinto(List<String> idsHistoria) {
        return (root, query, cb) -> cb.not(root.get("id").in(idsHistoria));
    }

    private List<HistoriaUsuario> convertirListaHaciaModelo(List<DbHistoriaUsuarioModel> listaModelo) {
        List<HistoriaUsuario> resultado = new ArrayList<>();
        for (DbHistoriaUsuarioModel p : listaModelo) {
            resultado.add(p.haciaModelo());
        }
        return resultado;
    }
}