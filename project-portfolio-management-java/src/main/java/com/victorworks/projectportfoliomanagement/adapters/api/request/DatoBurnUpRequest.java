package com.victorworks.projectportfoliomanagement.adapters.api.request;

import com.victorworks.projectportfoliomanagement.domain.model.entities.DatoBurnUp;

public class DatoBurnUpRequest {
    private String nombreSprint;
    private float puntosHistoriasCompletados;

    public String getNombreSprint() {
        return nombreSprint;
    }

    public float getPuntosHistoriasCompletados() {
        return puntosHistoriasCompletados;
    }

    public float getPuntosHistoriaTotales() {
        return puntosHistoriaTotales;
    }

    private float puntosHistoriaTotales;

    public DatoBurnUpRequest(String nombreSprint, float puntosHistoriasCompletados, float puntosHistoriaTotales) {
        this.nombreSprint = nombreSprint;
        this.puntosHistoriasCompletados = puntosHistoriasCompletados;
        this.puntosHistoriaTotales = puntosHistoriaTotales;
    }

    public static DatoBurnUpRequest desde(DatoBurnUp dato) {
        return new DatoBurnUpRequest(dato.getNombreSprint(), dato.getPuntosHistoriasCompletados(),
                dato.getPuntosHistoriaTotales());
    }
}
