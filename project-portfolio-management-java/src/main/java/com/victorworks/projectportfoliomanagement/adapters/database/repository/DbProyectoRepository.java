package com.victorworks.projectportfoliomanagement.adapters.database.repository;

import com.victorworks.projectportfoliomanagement.adapters.database.dao.ColumnaTableroDAO;
import com.victorworks.projectportfoliomanagement.adapters.database.dao.ProyectoDAO;
import com.victorworks.projectportfoliomanagement.adapters.database.model.DbColumnaTableroModel;
import com.victorworks.projectportfoliomanagement.adapters.database.model.DbProyectoModel;
import com.victorworks.projectportfoliomanagement.domain.model.entities.*;
import com.victorworks.projectportfoliomanagement.domain.ports.repositorio.ProyectoRepository;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.springframework.data.jpa.domain.Specification.where;

@Repository
public class DbProyectoRepository implements ProyectoRepository {
    private final ProyectoDAO proyectoDAO;
    private final ColumnaTableroDAO columnaTableroDAO;

    public DbProyectoRepository(ProyectoDAO proyectoDAO, ColumnaTableroDAO columnaTableroDAO) {
        this.proyectoDAO = proyectoDAO;
        this.columnaTableroDAO = columnaTableroDAO;
    }

    @Override
    public List<Proyecto> todos() {
        List<DbProyectoModel> proyectosDb = proyectoDAO.findAll();
        List<Proyecto> resultado = new ArrayList<>();
        for (DbProyectoModel proyectoDb : proyectosDb) {
            resultado.add(proyectoDb.haciaModelo());
        }
        return resultado;
    }

    @Override
    public Proyecto obtenerPorId(ProyectoId id) {
        Optional<DbProyectoModel> encontrado = proyectoDAO.findById(id.toString());
        if (encontrado.isPresent()) {
            return encontrado.get().haciaModelo();
        }
        return null;
    }

    @Override
    public Proyecto crear(Proyecto proyecto) {
        DbProyectoModel guardado = proyectoDAO.save(DbProyectoModel.desde(proyecto));
        return guardado.haciaModelo();
    }

    @Override
    public void actualizar(ProyectoId proyectoId, Proyecto proyecto) {
        if (obtenerPorId(proyectoId) != null) {
            proyectoDAO.save(DbProyectoModel.desde(proyecto));
        }
    }

    @Override
    public void borrar(ProyectoId proyectoId) {
        proyectoDAO.deleteById(proyectoId.toString());
    }

    @Override
    public List<ColumnaTablero> obtenerEstadosTareaParaProyecto(ProyectoId proyectoId) {
        String id = proyectoId == null ? null: proyectoId.toString();
        List<DbColumnaTableroModel> resultado = columnaTableroDAO.findAll(
                where(perteneceAProyecto(id))
        );

        if (resultado == null || resultado.size() == 0) {
            resultado = obtenerColumnasPorDefecto(id);
        }

        return convertirListaColumnasHaciaModelo(resultado);
    }

    private List<DbColumnaTableroModel> obtenerColumnasPorDefecto(String id) {
        List<DbColumnaTableroModel> resultado = new ArrayList<>();

        DbColumnaTableroModel columnaEspera = new DbColumnaTableroModel("", id, EstadoTarea.ALaEspera.toString(), true, false, 1);
        DbColumnaTableroModel columnaDesarrollo = new DbColumnaTableroModel("", id, EstadoTarea.EnDesarrollo.toString(), false, false, 2);
        DbColumnaTableroModel columnaQa = new DbColumnaTableroModel("", id, EstadoTarea.QA.toString(), false, false, 3);
        DbColumnaTableroModel columnaFinalizada = new DbColumnaTableroModel("", id, EstadoTarea.Terminado.toString(), false, true, 4);

        columnaTableroDAO.save(columnaEspera);
        columnaTableroDAO.save(columnaDesarrollo);
        columnaTableroDAO.save(columnaQa);
        columnaTableroDAO.save(columnaFinalizada);

        resultado.add(columnaEspera);
        resultado.add(columnaDesarrollo);
        resultado.add(columnaQa);
        resultado.add(columnaFinalizada);

        return resultado;
    }

    private <DbColumnaTableroModel> Specification perteneceAProyecto(String id) {
        return (root, query, cb) -> cb.equal(root.get("proyectoId"), id);
    }

    private List<ColumnaTablero> convertirListaColumnasHaciaModelo(List<DbColumnaTableroModel> listaModelo) {
        List<ColumnaTablero> resultado = new ArrayList<>();
        for (DbColumnaTableroModel p : listaModelo) {
            resultado.add(p.haciaModelo());
        }
        return resultado;
    }
}
