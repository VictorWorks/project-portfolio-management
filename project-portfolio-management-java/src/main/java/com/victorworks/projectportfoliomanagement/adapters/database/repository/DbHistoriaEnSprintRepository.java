package com.victorworks.projectportfoliomanagement.adapters.database.repository;

import com.victorworks.projectportfoliomanagement.adapters.database.dao.HistoriaEnSprintDAO;
import com.victorworks.projectportfoliomanagement.adapters.database.model.DbHistoriaEnSprintModel;
import com.victorworks.projectportfoliomanagement.domain.model.entities.HistoriaEnSprint;
import com.victorworks.projectportfoliomanagement.domain.ports.repositorio.HistoriaEnSprintRepository;
import org.springframework.stereotype.Repository;

@Repository
public class DbHistoriaEnSprintRepository implements HistoriaEnSprintRepository {
    private  final HistoriaEnSprintDAO historiaEnSprintDAO;

    public DbHistoriaEnSprintRepository(HistoriaEnSprintDAO historiaEnSprintDAO) {
        this.historiaEnSprintDAO = historiaEnSprintDAO;
    }

    @Override
    public void actualizar(HistoriaEnSprint historiaEnSprint) {
        DbHistoriaEnSprintModel paraGrabar = DbHistoriaEnSprintModel.desde(historiaEnSprint);
        this.historiaEnSprintDAO.save(paraGrabar);
    }
}
