package com.victorworks.projectportfoliomanagement.adapters.security;

import com.victorworks.projectportfoliomanagement.adapters.security.constante.ConstantesSeguridad;
import com.victorworks.projectportfoliomanagement.adapters.security.filtro.AuthenticationEntryPointJwt;
import com.victorworks.projectportfoliomanagement.adapters.security.filtro.FiltroAutorizacionJwt;
import com.victorworks.projectportfoliomanagement.adapters.security.filtro.ManejadorAccesoDenegadoJwt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class ConfiguracionSeguridad extends WebSecurityConfigurerAdapter {
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    private FiltroAutorizacionJwt filtroAutorizacionJwt;
    private UserDetailsService userDetailsService;
    private AuthenticationEntryPointJwt authenticationEntryPoint;
    private ManejadorAccesoDenegadoJwt manejadorAccesoDenegado;

    @Autowired
    public ConfiguracionSeguridad(BCryptPasswordEncoder bCryptPasswordEncoder,
                                  FiltroAutorizacionJwt filtroAutorizacionJwt,
                                  @Qualifier("UserDetailService") UserDetailsService userDetailsService,
                                  AuthenticationEntryPointJwt jwtAuthenticationEntryPoint,
                                  ManejadorAccesoDenegadoJwt jwtManejadorAccesoDenegado) {
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.filtroAutorizacionJwt = filtroAutorizacionJwt;
        this.userDetailsService = userDetailsService;
        this.authenticationEntryPoint = jwtAuthenticationEntryPoint;
        this.manejadorAccesoDenegado = jwtManejadorAccesoDenegado;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(this.userDetailsService)
                .passwordEncoder(this.bCryptPasswordEncoder);
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.csrf().disable().cors().and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
                .authorizeRequests().antMatchers(ConstantesSeguridad.RUTAS_ABIERTAS).permitAll()
                .anyRequest().authenticated().and()
                .exceptionHandling().accessDeniedHandler(this.manejadorAccesoDenegado)
                .authenticationEntryPoint(this.authenticationEntryPoint).and()
                .addFilterBefore(filtroAutorizacionJwt, UsernamePasswordAuthenticationFilter.class);

        // Sin esta línea no se muestra la consola de H2
        http.headers().frameOptions().disable();
    }
/*
    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

 */
}
