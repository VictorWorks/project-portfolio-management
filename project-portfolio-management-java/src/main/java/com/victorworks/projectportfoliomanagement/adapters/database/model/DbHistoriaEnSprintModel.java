package com.victorworks.projectportfoliomanagement.adapters.database.model;

import com.victorworks.projectportfoliomanagement.domain.model.entities.EstadoTarea;
import com.victorworks.projectportfoliomanagement.domain.model.entities.HistoriaEnSprint;
import com.victorworks.projectportfoliomanagement.utils.Dates;

import javax.persistence.*;

@Entity
@Table(name = "HistoriaEnSprint")
public class DbHistoriaEnSprintModel {

    @EmbeddedId
    private DbHistoriaEnSprintId id = new DbHistoriaEnSprintId();

    private String estado;
    private String fechaModificacion;

    @ManyToOne(fetch= FetchType.LAZY, optional = false)
    @MapsId("sprintId")
    private DbSprintModel sprint;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @MapsId("historiaUsuarioId")
    private DbHistoriaUsuarioModel historiaUsuario;

    public DbHistoriaEnSprintModel() {}

    public DbHistoriaEnSprintModel(DbSprintModel sprint, DbHistoriaUsuarioModel historiaUsuario,
                                   String estado, String fechaModificacion) {
        this.id = new DbHistoriaEnSprintId(sprint.getId(), historiaUsuario.getId());
        this.sprint = sprint;
        this.historiaUsuario = historiaUsuario;
        this.estado = estado;
        this.fechaModificacion = fechaModificacion;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(String fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public DbSprintModel getSprint() {
        return this.sprint;
    }

    public void setSprint(DbSprintModel sprint) {
        this.sprint = sprint;
    }

    public DbHistoriaUsuarioModel getHistoriaUsuario() {
        return this.historiaUsuario;
    }

    public void setHistoriaUsuario(DbHistoriaUsuarioModel historiaUsuario) {
        this.historiaUsuario = historiaUsuario;
    }

    public HistoriaEnSprint haciaModelo() {
        return new HistoriaEnSprint(this.sprint.haciaModelo(),
                this.historiaUsuario.haciaModelo(),
                EstadoTarea.obtenerEstadoDesde(estado), Dates.parseFecha(fechaModificacion));
    }

    public static DbHistoriaEnSprintModel desde(HistoriaEnSprint historiaEnSprint) {
        return new DbHistoriaEnSprintModel(DbSprintModel.desde(historiaEnSprint.getSprint()),
                DbHistoriaUsuarioModel.desde(historiaEnSprint.getHistoriaUsuario()),
                historiaEnSprint.getEstado().toString(),
                Dates.formatearFecha(historiaEnSprint.getFechaModificacion())
        );
    }


}
