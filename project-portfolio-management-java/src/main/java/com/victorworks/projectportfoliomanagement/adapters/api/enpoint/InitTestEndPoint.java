package com.victorworks.projectportfoliomanagement.adapters.api.enpoint;

import com.victorworks.projectportfoliomanagement.domain.model.entities.*;
import com.victorworks.projectportfoliomanagement.domain.ports.excepcion.UsuarioExisteException;
import com.victorworks.projectportfoliomanagement.domain.ports.servicio.HistoriaUsuarioService;
import com.victorworks.projectportfoliomanagement.domain.ports.servicio.PersonaService;
import com.victorworks.projectportfoliomanagement.domain.ports.servicio.ProyectoService;
import com.victorworks.projectportfoliomanagement.domain.ports.servicio.SprintService;
import com.victorworks.projectportfoliomanagement.utils.Dates;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/v1/test")
public class InitTestEndPoint {

    private static final String IdProyecto = "01a0348e-4c2b-4733-938b-eda2a1da98d8";

    private static final String IdHistoria1 = "bbeff4e9-618e-4dac-ad36-7f58a4136fe3";
    private static final String IdHistoria2 = "412c0aa3-a5ec-44d1-b41d-6f595f18369d";
    private static final String IdHistoria3 = "b8f782d4-bf58-42e8-9ffb-2cb26272bbeb";
    private static final String IdHistoria4 = "6a71ae83-9dcf-4c1a-bae0-e27ec070095d";

    private static final String IdPersona1 = "0f839507-9a44-41e6-b381-78dd48a955fa";
    private static final String IdPersona2 = "dfa36061-5382-434a-89bb-88707ca87011";
    private static final String IdPersona3 = "5c89175c-8c83-43fb-bba0-2723f34911c4";

    private static final String IdSprint1 = "e48eaec4-4f7c-42ba-925e-a819eea4dd7a";
    private static final String IdSprint2 = "342e3b56-c67f-4dd3-ac64-4df27cd43bf6";
    private static final String IdSprint3 = "47a2ed8f-7b7b-4019-835c-67bcd446123b";

    private final PersonaService personas;
    private final ProyectoService proyectos;
    private final SprintService sprints;
    private final HistoriaUsuarioService historias;

    public InitTestEndPoint(ProyectoService proyectos, PersonaService personas, SprintService sprints, HistoriaUsuarioService historias) {
        this.personas = personas;
        this.proyectos = proyectos;
        this.sprints = sprints;
        this.historias = historias;
    }

    @GetMapping()
    public void InicializarEscenarioTest() throws UsuarioExisteException {
        Proyecto p = crearNuevoProyecto();
        ProyectoId proyectoId = p.getId();
        crearListaPersonas(proyectoId);
        CrearListaSprints(proyectoId);

        List<HistoriaUsuario> historiasUsuario = CrearListasHistoriasUsuario(proyectoId);
        AgregarHistoriasASprints(historiasUsuario);

    }

    private List<HistoriaUsuario> CrearListasHistoriasUsuario(ProyectoId proyectoId) {
        Date fecha1 = Dates.parseFecha("2021-01-01");
        Date fecha2 = Dates.parseFecha("2021-02-01");

        HistoriaUsuario h1 = new HistoriaUsuario(HistoriaUsuarioId.desde(IdHistoria1),
                "Historia usuario pruebas 1", "Descripción historia 1", 10,
                1, proyectoId, EstadoTarea.ALaEspera, fecha1);

        HistoriaUsuario h2 = new HistoriaUsuario(HistoriaUsuarioId.desde(IdHistoria2),
                "Historia usuario pruebas 2", "Descripción historia 2", 20,
                2, proyectoId, EstadoTarea.ALaEspera, fecha1);
        HistoriaUsuario h3 = new HistoriaUsuario(HistoriaUsuarioId.desde(IdHistoria3),
                "Historia usuario pruebas 3", "Descripción historia 3", 30,
                3, proyectoId, EstadoTarea.ALaEspera, fecha2);
        HistoriaUsuario h4 = new HistoriaUsuario(HistoriaUsuarioId.desde(IdHistoria4),
                "Historia usuario pruebas 4", "Descripción historia 4", 40,
                4, proyectoId, EstadoTarea.ALaEspera, fecha2);
        historias.crear(h1);
        historias.crear(h2);
        historias.crear(h3);
        historias.crear(h4);

        List<HistoriaUsuario> resultado = new ArrayList<>();
        resultado.add(h1);
        resultado.add(h2);
        resultado.add(h3);
        resultado.add(h4);

        return resultado;
    }

    private Proyecto crearNuevoProyecto() {
        Proyecto insert = new Proyecto(ProyectoId.desde(IdProyecto), "Proyecto prueba 1");
        return proyectos.crear(insert);
    }

    private void crearListaPersonas(ProyectoId proyectoId) throws UsuarioExisteException {
        Persona p1 = new Persona(PersonaId.desde(IdPersona1), "Persona prueba 1", "usuario1", "password");
        personas.crear(p1);

        Persona p2 = new Persona(PersonaId.desde(IdPersona2), "Persona prueba 2", "usuario2",  "password");
        personas.crear(p2);

        Persona p3 = new Persona(PersonaId.desde(IdPersona3), "Persona prueba 3",  "usuario3",
                "password", RolUsuario.DESARROLLADOR, proyectoId);
        personas.crear(p3);
    }

    private void CrearListaSprints(ProyectoId proyectoId) {
        Sprint s1 = new Sprint(SprintId.desde(IdSprint1), "Sprint prueba 1",
                Dates.parseFecha("2021-02-01"), Dates.parseFecha("2021-02-07"), 10, proyectoId);
        sprints.crear(s1);

        Sprint s2 = new Sprint(SprintId.desde(IdSprint2), "Sprint prueba 2",
                Dates.parseFecha("2021-02-08"), Dates.parseFecha("2021-02-15"), 20, proyectoId);
        sprints.crear(s2);

        Sprint s3 = new Sprint(SprintId.desde(IdSprint3), "Sprint prueba 3",
                Dates.parseFecha("2021-02-16"), Dates.parseFecha("2021-02-23"), 30, proyectoId);
        sprints.crear(s3);
    }

    private void AgregarHistoriasASprints(List<HistoriaUsuario> historiasUsuario) {
        sprints.añadirHistoriaASprint(SprintId.desde(IdSprint1), historiasUsuario.get(0));
        sprints.añadirHistoriaASprint(SprintId.desde(IdSprint2), historiasUsuario.get(0));
        sprints.añadirHistoriaASprint(SprintId.desde(IdSprint2), historiasUsuario.get(1));
        sprints.añadirHistoriaASprint(SprintId.desde(IdSprint3), historiasUsuario.get(2));
    }

    @GetMapping("probarSprint")
    public void crearPruebasParaSprint() throws UsuarioExisteException {
        Proyecto proyecto = crearNuevoProyecto();
        crearListaPersonas(proyecto.getId());
        List<HistoriaUsuario> historias = crearListaHistoriasUsuario(proyecto.getId());
        int puntosTotales = calcularPuntosHistoria(historias);
        Sprint sprint = crearSprintParaPruebasSprint(proyecto.getId(), puntosTotales);
        asociarHistoriasASprint(sprint, historias);
    }

    private int calcularPuntosHistoria(List<HistoriaUsuario> historias) {
        return historias.stream().map(x -> x.getEstimacion()).reduce(0, (x, y) -> x + y);
    }

    private List<HistoriaUsuario> crearListaHistoriasUsuario(ProyectoId proyectoId) {
        List<HistoriaUsuario> resultado = new ArrayList<>();

        HistoriaUsuario espera1 = new HistoriaUsuario(HistoriaUsuarioId.desde(UUID.randomUUID()),
                "Historia Espera 1", "Historia Espera 1", 20, 90,
                proyectoId, EstadoTarea.ALaEspera, Dates.parseFecha("2021-09-01"));
        resultado.add(espera1);
        HistoriaUsuario espera2 = new HistoriaUsuario(HistoriaUsuarioId.desde(UUID.randomUUID()),
                "Historia Espera 2", "Historia Espera 2", 25, 85,
                proyectoId, EstadoTarea.ALaEspera, Dates.parseFecha("2021-09-01"));
        resultado.add(espera2);
        HistoriaUsuario espera3 = new HistoriaUsuario(HistoriaUsuarioId.desde(UUID.randomUUID()),
                "Historia Espera 3", "Historia Espera 3", 30, 80,
                proyectoId, EstadoTarea.ALaEspera, Dates.parseFecha("2021-09-01"));
        resultado.add(espera3);

        HistoriaUsuario progreso1 = new HistoriaUsuario(HistoriaUsuarioId.desde(UUID.randomUUID()),
                "Historia Desarrollo 1", "Historia Desarrollo 1", 40, 75,
                proyectoId, EstadoTarea.EnDesarrollo, Dates.parseFecha("2021-09-01"));
        resultado.add(progreso1);
        HistoriaUsuario progreso2 = new HistoriaUsuario(HistoriaUsuarioId.desde(UUID.randomUUID()),
                "Historia Desarrollo 2", "Historia Desarrollo 2", 45, 70,
                proyectoId, EstadoTarea.EnDesarrollo, Dates.parseFecha("2021-09-01"));
        resultado.add(progreso2);

        HistoriaUsuario qa1 = new HistoriaUsuario(HistoriaUsuarioId.desde(UUID.randomUUID()),
                "Historia QA 1", "Historia QA 1", 50, 60,
                proyectoId, EstadoTarea.QA, Dates.parseFecha("2021-09-01"));
        resultado.add(qa1);
        HistoriaUsuario qa2 = new HistoriaUsuario(HistoriaUsuarioId.desde(UUID.randomUUID()),
                "Historia QA 2", "Historia QA 2", 55, 65,
                proyectoId, EstadoTarea.QA, Dates.parseFecha("2021-09-01"));
        resultado.add(qa2);

        HistoriaUsuario terminado1 = new HistoriaUsuario(HistoriaUsuarioId.desde(UUID.randomUUID()),
                "Historia Terminada 1", "Historia Terminada 1", 60, 50,
                proyectoId, EstadoTarea.Terminado, Dates.parseFecha("2021-09-02"));
        resultado.add(terminado1);
        HistoriaUsuario terminado2 = new HistoriaUsuario(HistoriaUsuarioId.desde(UUID.randomUUID()),
                "Historia Terminada 2", "Historia Terminada 2", 65, 55,
                proyectoId, EstadoTarea.Terminado, Dates.parseFecha("2021-09-03"));
        resultado.add(terminado2);
        HistoriaUsuario terminado3 = new HistoriaUsuario(HistoriaUsuarioId.desde(UUID.randomUUID()),
                "Historia Terminada 3", "Historia Terminada 3", 70, 40,
                proyectoId, EstadoTarea.Terminado, Dates.parseFecha("2021-09-05"));
        resultado.add(terminado3);
        HistoriaUsuario terminado4 = new HistoriaUsuario(HistoriaUsuarioId.desde(UUID.randomUUID()),
                "Historia Terminada 4", "Historia Terminada 4", 75, 45,
                proyectoId, EstadoTarea.Terminado, Dates.parseFecha("2021-09-06"));
        resultado.add(terminado4);

        for(HistoriaUsuario historia: resultado) {
            historias.crear(historia);
        }
        return resultado;
    }

    private Sprint crearSprintParaPruebasSprint(ProyectoId proyectoId, int puntosHistoria) {
        Sprint sprint = new Sprint(SprintId.desde(IdSprint1),
                "Sprint prueba 1",
                Dates.parseFecha("2021-09-01"), Dates.parseFecha("2021-09-25"),
                puntosHistoria, proyectoId);
        return sprints.crear(sprint);
    }

    private void asociarHistoriasASprint(Sprint sprint, List<HistoriaUsuario> historiasUsuario) {
        for(HistoriaUsuario historiaUsuario: historiasUsuario) {
            HistoriaEnSprint historiaEnSprint = new HistoriaEnSprint(sprint, historiaUsuario,
                    historiaUsuario.getEstadoActual(), historiaUsuario.getFechaAlta());
            sprints.crearHistoriaEnSprint(historiaEnSprint);
        }
    }
}
