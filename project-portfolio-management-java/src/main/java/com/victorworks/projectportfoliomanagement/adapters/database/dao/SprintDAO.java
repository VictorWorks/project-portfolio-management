package com.victorworks.projectportfoliomanagement.adapters.database.dao;

import com.victorworks.projectportfoliomanagement.adapters.database.model.DbSprintModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface SprintDAO extends JpaRepository<DbSprintModel, String>, JpaSpecificationExecutor<DbSprintModel> {
}
