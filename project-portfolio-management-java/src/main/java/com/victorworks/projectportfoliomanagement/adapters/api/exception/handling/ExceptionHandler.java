package com.victorworks.projectportfoliomanagement.adapters.api.exception.handling;

import com.auth0.jwt.exceptions.TokenExpiredException;
import com.victorworks.projectportfoliomanagement.adapters.security.entidad.HttpResponse;
import com.victorworks.projectportfoliomanagement.domain.ports.excepcion.NombreUsuarioCambiadoException;
import com.victorworks.projectportfoliomanagement.domain.ports.excepcion.NombreUsuarioNoExisteException;
import com.victorworks.projectportfoliomanagement.domain.ports.excepcion.UsuarioExisteException;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.persistence.NoResultException;
import org.springframework.security.access.AccessDeniedException;
import java.util.Locale;
import java.util.Objects;

@RestControllerAdvice
public class ExceptionHandler {
    public static final String ERROR_METODO_HTTP = "El método http es incorrecto para la petición. Envía en su lugar con el método '%s'.";
    public static final String MENSAJE_ERROR_GENERICO = "Error procesando la petición.";
    public static final String LOGIN_CONTRASEÑA_INCORRECTO = "El nombre de usuario o contraseña no es correcto.";
    public static final String ERROR_PERMISOS = "El nivel de permisos no permite esta acción.";


    @org.springframework.web.bind.annotation.ExceptionHandler(BadCredentialsException.class)
    public ResponseEntity<HttpResponse> badCredentialsException() {
        return createHttpReponse(HttpStatus.BAD_REQUEST, LOGIN_CONTRASEÑA_INCORRECTO);
    }

    @org.springframework.web.bind.annotation.ExceptionHandler(AccessDeniedException.class)
    public ResponseEntity<HttpResponse> accessDeniedException() {
        return createHttpReponse(HttpStatus.FORBIDDEN, ERROR_PERMISOS);
    }

    @org.springframework.web.bind.annotation.ExceptionHandler(TokenExpiredException.class)
    public ResponseEntity<HttpResponse> tokenExpiredException(TokenExpiredException exception) {
        return createHttpReponse(HttpStatus.UNAUTHORIZED, exception.getMessage().toUpperCase(Locale.ROOT));
    }

    @org.springframework.web.bind.annotation.ExceptionHandler(UsuarioExisteException.class)
    public ResponseEntity<HttpResponse> usernameAlreadyExistException(UsuarioExisteException exception) {
        return createHttpReponse(HttpStatus.UNAUTHORIZED, exception.getMessage().toUpperCase(Locale.ROOT));
    }

    @org.springframework.web.bind.annotation.ExceptionHandler(NombreUsuarioCambiadoException.class)
    public ResponseEntity<HttpResponse> emailNotFoundException(NombreUsuarioCambiadoException exception) {
        return createHttpReponse(HttpStatus.BAD_REQUEST, exception.getMessage().toUpperCase(Locale.ROOT));
    }

    @org.springframework.web.bind.annotation.ExceptionHandler(NombreUsuarioNoExisteException.class)
    public ResponseEntity<HttpResponse> userNotFoundException(NombreUsuarioNoExisteException exception) {
        return createHttpReponse(HttpStatus.BAD_REQUEST, exception.getMessage().toUpperCase(Locale.ROOT));
    }

    @org.springframework.web.bind.annotation.ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    public ResponseEntity<HttpResponse> httpRequestMethodNotSupportedException(HttpRequestMethodNotSupportedException exception) {
        HttpMethod supportedMethod = Objects.requireNonNull(exception.getSupportedHttpMethods()).iterator().next();
        return createHttpReponse(HttpStatus.METHOD_NOT_ALLOWED, String.format(ERROR_METODO_HTTP, supportedMethod));
    }

    @org.springframework.web.bind.annotation.ExceptionHandler(Exception.class)
    public ResponseEntity<HttpResponse> internalServerErrorException(Exception exception) {
        exception.printStackTrace();
        return createHttpReponse(HttpStatus.INTERNAL_SERVER_ERROR, MENSAJE_ERROR_GENERICO);
    }

    @org.springframework.web.bind.annotation.ExceptionHandler(NoResultException.class)
    public ResponseEntity<HttpResponse> notFoundException(NoResultException exception) {
        return createHttpReponse(HttpStatus.NOT_FOUND, exception.getMessage());
    }

    private ResponseEntity<HttpResponse> createHttpReponse(HttpStatus httpStatus, String message) {
        HttpResponse httpResponse = new HttpResponse(httpStatus.value(), httpStatus,
                httpStatus.getReasonPhrase().toUpperCase(Locale.ROOT), message.toUpperCase(Locale.ROOT));
        return new ResponseEntity<>(httpResponse, httpStatus);
    }
}
