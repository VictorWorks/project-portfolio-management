package com.victorworks.projectportfoliomanagement.adapters.api.enpoint;

import com.victorworks.projectportfoliomanagement.adapters.api.request.PersonaRequest;
import com.victorworks.projectportfoliomanagement.adapters.security.ManejadorToken;
import com.victorworks.projectportfoliomanagement.adapters.security.constante.ConstantesSeguridad;
import com.victorworks.projectportfoliomanagement.adapters.security.entidad.UserPrincipal;
import com.victorworks.projectportfoliomanagement.domain.model.entities.Persona;
import com.victorworks.projectportfoliomanagement.domain.model.entities.PersonaId;
import com.victorworks.projectportfoliomanagement.domain.model.entities.ProyectoId;
import com.victorworks.projectportfoliomanagement.domain.ports.excepcion.NombreUsuarioCambiadoException;
import com.victorworks.projectportfoliomanagement.domain.ports.excepcion.UsuarioExisteException;
import com.victorworks.projectportfoliomanagement.domain.ports.servicio.AutenticacionService;
import com.victorworks.projectportfoliomanagement.domain.ports.servicio.PersonaService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("v1/personas")
//@CrossOrigin(origins = "*", allowedHeaders = "*")
public class PersonaEndPoint {

    private final PersonaService personas;
    private final AutenticacionService autenticacion;
    private final ManejadorToken manejadorToken;

    PersonaEndPoint(PersonaService personas, AutenticacionService autenticacion, ManejadorToken manejadorToken) {
        this.personas = personas;
        this.autenticacion = autenticacion;
        this.manejadorToken = manejadorToken;
    }

    @GetMapping
    @PreAuthorize("hasAuthority('personal:ver')")
    List<PersonaRequest> todos() {
        final List<Persona> personasModelo = personas.todos();
        return mapearDesdeModelo(personasModelo);
    }

    @GetMapping("{personaId}")
    @PreAuthorize("hasAuthority('personal:ver')")
    PersonaRequest obtenerPorId(@PathVariable("personaId") final String personaId) {
        final Persona persona = personas.obtenerPorId(PersonaId.desde(personaId));
        if (persona != null) {
            return PersonaRequest.desde(persona);
        }
        return null;
    }

    @GetMapping("disponibles")
    @PreAuthorize("hasAuthority('personal:ver')")
    List<PersonaRequest> obtenerDesasignados() {
        final List<Persona> personasModelo = personas.obtenerDesasignados();
        return mapearDesdeModelo(personasModelo);
    }

    @GetMapping("claveNombre/{claveNombre}/noPerteneciente/{idProyecto}")
    @PreAuthorize("hasAuthority('personal:ver')")
    List<PersonaRequest> obtenerPersonalNoIncluidoEnProyectoFiltradoPorNombre(@PathVariable("claveNombre") final String claveNombre,
                                                                              @PathVariable("idProyecto") final String idProyecto) {
        final List<Persona> personasModelo = personas.obtenerPersonalNoIncluidoEnProyectoFiltradoPorNombre(claveNombre,
                ProyectoId.desde(idProyecto));
        return mapearDesdeModelo(personasModelo);
    }

    @PostMapping
    @PreAuthorize("hasAuthority('personal:añadir')")
    PersonaRequest crear(@RequestBody final PersonaRequest persona) throws UsuarioExisteException {
        final Persona creada = personas.crear(persona.haciaModelo());
        return PersonaRequest.desde(creada);
    }

    @PostMapping("/login")
    public ResponseEntity<PersonaRequest> login(@RequestBody Persona persona) {
        Authentication auth = new UsernamePasswordAuthenticationToken(persona.getUsername(), persona.getPassword());
        autenticacion.authenticate(auth);
        Persona encontrado = personas.obtenerPorNombreUsuario(persona.getUsername());
        UserPrincipal userPrincipal = new UserPrincipal(encontrado);
        HttpHeaders jwtHeader = obtenerJwtHeader(userPrincipal);
        return new ResponseEntity<>(PersonaRequest.desde(encontrado), jwtHeader, HttpStatus.OK);
    }

    @PostMapping("registrar")
    PersonaRequest registrar(@RequestBody final PersonaRequest persona) throws UsuarioExisteException {
        final Persona creada = personas.registrar(persona.haciaModelo());
        return PersonaRequest.desde(creada);
    }


    @PutMapping("{personaId}")
    @PreAuthorize("hasAuthority('personal:ver')")
    void actualizar(@PathVariable("personaId") final String personaId, @RequestBody final PersonaRequest persona) throws NombreUsuarioCambiadoException {
        personas.actualizar(PersonaId.desde(personaId), persona.haciaModelo());
    }

    @DeleteMapping("{personaId}")
    @PreAuthorize("hasAuthority('personal:borrar')")
    void borrar(@PathVariable("personaId") final String personaId) {
        personas.borrar(PersonaId.desde(personaId));
    }

    private List<PersonaRequest> mapearDesdeModelo(List<Persona> listaModelo) {
        return listaModelo.stream()
                .map(x -> PersonaRequest.desde(x))
                .collect(Collectors.toList());
    }

    private HttpHeaders obtenerJwtHeader(UserPrincipal userPrincipal) {
        HttpHeaders header = new HttpHeaders();
        header.add(ConstantesSeguridad.CABECERA_HTTP_TOKEN, manejadorToken.generarToken(userPrincipal));
        return header;
    }
}