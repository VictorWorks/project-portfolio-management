package com.victorworks.projectportfoliomanagement.adapters.api.request;


import com.victorworks.projectportfoliomanagement.domain.model.entities.EstadoTarea;
import com.victorworks.projectportfoliomanagement.domain.model.entities.HistoriaUsuario;
import com.victorworks.projectportfoliomanagement.domain.model.entities.HistoriaUsuarioId;
import com.victorworks.projectportfoliomanagement.domain.model.entities.ProyectoId;
import com.victorworks.projectportfoliomanagement.utils.Dates;

public class HistoriaUsuarioRequest {
    private String id;
    private String nombre;
    private String descripcion;
    private int estimacion;
    private int prioridad;
    private String proyectoId;
    private String estadoActual;
    private final String fechaAlta;

    public HistoriaUsuarioRequest(String id, String nombre, String descripcion, int estimacion,
                                  int prioridad, String proyectoId, String estadoActual, String fechaAlta)
    {
        this.id = id;
        this.descripcion = descripcion;
        this.estimacion = estimacion;
        this.proyectoId = proyectoId;
        this.nombre = nombre;
        this.prioridad = prioridad;
        this.estadoActual = estadoActual;
        this.fechaAlta = fechaAlta;
    }

    public String getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public int getEstimacion() {
        return estimacion;
    }

    public int getPrioridad() {
        return prioridad;
    }

    public String getProyectoId() {
        return proyectoId;
    }

    public String getEstadoActual() { return estadoActual; }

    public String getFechaAlta(){
        return fechaAlta;
    }

    public HistoriaUsuario haciaModelo() {
        return new HistoriaUsuario(HistoriaUsuarioId.desde(id), nombre, descripcion,
                estimacion, prioridad, ProyectoId.desde(proyectoId), EstadoTarea.obtenerEstadoDesde(estadoActual),
                Dates.parseFecha(fechaAlta));
    }

    public static HistoriaUsuarioRequest desde(HistoriaUsuario historiaUsuario){
        String proyectoId = null;
        if (historiaUsuario.getProyectoId() != null)
            proyectoId = historiaUsuario.getProyectoId().toString();

        String fechaAlta = Dates.formatearFecha(historiaUsuario.getFechaAlta());
        return  new HistoriaUsuarioRequest(historiaUsuario.getId().toString(),
                historiaUsuario.getNombre(), historiaUsuario.getDescripcion(),
                historiaUsuario.getEstimacion(), historiaUsuario.getPrioridad(),
                proyectoId, historiaUsuario.getEstadoActual().toString(), fechaAlta);
    }
}
