package com.victorworks.projectportfoliomanagement.adapters.database.model;

import com.victorworks.projectportfoliomanagement.domain.model.entities.EstadoTarea;
import com.victorworks.projectportfoliomanagement.domain.model.entities.HistoriaUsuario;
import com.victorworks.projectportfoliomanagement.domain.model.entities.HistoriaUsuarioId;
import com.victorworks.projectportfoliomanagement.domain.model.entities.ProyectoId;
import com.victorworks.projectportfoliomanagement.utils.Dates;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "HistoriaUsuario")
public class DbHistoriaUsuarioModel {
    @Id
    private String id;
    private String nombre;
    private String descripcion;
    private int estimacion;
    private int prioridad;
    private String proyectoId;
    private String estadoActual;
    private String fechaAlta;

    @OneToMany(mappedBy = "historiaUsuario", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<DbHistoriaEnSprintModel> sprints;

    public DbHistoriaUsuarioModel(){}

    public  DbHistoriaUsuarioModel(String id, String nombre, String descripcion,
                                   int estimacion, int prioridad,
                                   String proyectoId, String estadoActual, String fechaAlta){
        this.id = id;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.estimacion = estimacion;
        this.prioridad = prioridad;
        this.proyectoId = proyectoId;
        this.estadoActual = estadoActual;
        this.fechaAlta = fechaAlta;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getEstimacion() {
        return estimacion;
    }

    public void setEstimacion(int estimacion) {
        this.estimacion = estimacion;
    }

    public int getPrioridad() {
        return prioridad;
    }

    public void setPrioridad(int prioridad) {
        this.prioridad = prioridad;
    }

    public String getProyectoId() {
        return proyectoId;
    }

    public void setProyectoId(String proyectoId) {
        this.proyectoId = proyectoId;
    }

    public String getEstadoActual() { return this.estadoActual; }

    public  void setEstadoActual(String estadoActual) { this.estadoActual = estadoActual; }

    public Set<DbHistoriaEnSprintModel> getSprints() {
        return this.sprints;
    }

    public void  setSprints(Set<DbHistoriaEnSprintModel> sprints) {
        this.sprints = sprints;
    }

    public HistoriaUsuario haciaModelo() {
        return new HistoriaUsuario(HistoriaUsuarioId.desde(id), nombre, descripcion,
                estimacion, prioridad, ProyectoId.desde( proyectoId), EstadoTarea.obtenerEstadoDesde(estadoActual),
                Dates.parseFecha(fechaAlta));
    }

    public static DbHistoriaUsuarioModel desde(HistoriaUsuario historia) {
        String proyectoId = null;
        if (historia.getProyectoId() != null)
            proyectoId = historia.getProyectoId().toString();

        return new DbHistoriaUsuarioModel(historia.getId().toString(), historia.getNombre(), historia.getDescripcion(),
                historia.getEstimacion(), historia.getPrioridad(),
                proyectoId, historia.getEstadoActual().toString(),  Dates.formatearFecha(historia.getFechaAlta()));
    }


}
