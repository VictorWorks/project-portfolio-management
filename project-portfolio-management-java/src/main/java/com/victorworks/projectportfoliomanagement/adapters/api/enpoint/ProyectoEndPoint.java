package com.victorworks.projectportfoliomanagement.adapters.api.enpoint;

import com.victorworks.projectportfoliomanagement.adapters.api.request.*;
import com.victorworks.projectportfoliomanagement.domain.model.entities.*;
import com.victorworks.projectportfoliomanagement.domain.ports.servicio.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("v1/proyectos")
public class ProyectoEndPoint {
    private final ProyectoService proyectos;
    private final PersonaService personas;
    private final HistoriaUsuarioService historias;
    private final SprintService sprints;
    private final ReportesService reportes;

    public ProyectoEndPoint(ProyectoService proyectoService, PersonaService personaService,
                            HistoriaUsuarioService historiasService, SprintService sprintService,
                            ReportesService reportesService) {
        this.proyectos = proyectoService;
        this.personas = personaService;
        this.historias = historiasService;
        this.sprints = sprintService;
        this.reportes = reportesService;
    }

    @GetMapping
    public List<ProyectoRequest> todos() {
        List<Proyecto> proyectosModelo = proyectos.todos();
        return proyectosModelo.stream()
                .map(p -> ProyectoRequest.desde(p))
                .collect(Collectors.toList());
    }

    @GetMapping("{proyectoId}")
    @PreAuthorize("hasAuthority('proyecto:ver')")
    public ProyectoRequest obtenerPorId(@PathVariable("proyectoId") final String proyectoId) {
        Proyecto encontrado = proyectos.obtenerPorId(ProyectoId.desde(proyectoId));
        return ProyectoRequest.desde(encontrado);
    }

    @PostMapping
    @PreAuthorize("hasAuthority('proyecto:añadir')")
    public ProyectoRequest crear(@RequestBody final ProyectoRequest proyecto) {
        Proyecto creado = proyectos.crear(proyecto.haciaModelo());
        return ProyectoRequest.desde(creado);
    }

    @PutMapping("{proyectoId}")
    @PreAuthorize("hasAuthority('proyecto:añadir')")
    public void actualizar(@PathVariable("proyectoId") final String proyectoId, @RequestBody final ProyectoRequest proyecto) {
        proyectos.actualizar(ProyectoId.desde(proyectoId), proyecto.haciaModelo());
    }

    @DeleteMapping("{proyectoId}")
    @PreAuthorize("hasAuthority('proyecto:borrar')")
    public void borrar(@PathVariable("proyectoId") final String proyectoId) {
        proyectos.borrar(ProyectoId.desde(proyectoId));
    }

    @GetMapping("{proyectoId}/equipo")
    @PreAuthorize("hasAuthority('proyecto:ver')")
    public List<PersonaRequest> obtenerEquipoProyecto(@PathVariable("proyectoId") final String proyectoId) {
        List<Persona> equipo = personas.obtenerEquipoProyecto(ProyectoId.desde(proyectoId));
        return mapearListaPersonasARequest(equipo);
    }

    @GetMapping("{proyectoId}/sprints")
    @PreAuthorize("hasAuthority('proyecto:ver')")
    public List<SprintRequest> obtenerSprintsProyecto(@PathVariable("proyectoId") final String proyectoId) {
        ProyectoId id = ProyectoId.desde(proyectoId);
        List<Sprint> paraProyecto = sprints.obtenerPorProyecto(id);
        return mapearListaSprintsARequest(paraProyecto);
    }

    @GetMapping("{proyectoId}/historias")
    @PreAuthorize("hasAuthority('proyecto:ver')")
    public List<HistoriaUsuarioRequest> obtenerHistoriasProyecto(@PathVariable("proyectoId") final String proyectoId) {
        List<HistoriaUsuario> historiasProyecto = historias.obtenerPorProyecto(ProyectoId.desde(proyectoId));
        return mapearListaHistoriasUsuarioARequest(historiasProyecto);
    }

    @GetMapping("{proyectoId}/burnup")
    @PreAuthorize("hasAuthority('proyecto:ver')")
    public List<DatoBurnUpRequest> obtenerDatosBurnup(@PathVariable("proyectoId") final String proyectoId){
        List<DatoBurnUp>  datosBurnUp = reportes.obtenerDatosBurnup(ProyectoId.desde(proyectoId));
        return mapearListaDatosBurnUpARequest(datosBurnUp);
    }

    private List<PersonaRequest> mapearListaPersonasARequest(List<Persona> equipo) {
        return equipo.stream()
                .map(p -> PersonaRequest.desde(p))
                .collect(Collectors.toList());
    }

    private List<SprintRequest> mapearListaSprintsARequest(List<Sprint> sprints) {
        return sprints.stream()
                .map(sprint -> SprintRequest.desde(sprint))
                .collect(Collectors.toList());
    }

    private List<HistoriaUsuarioRequest> mapearListaHistoriasUsuarioARequest(List<HistoriaUsuario> historias) {
        return historias.stream()
                .map(historia -> HistoriaUsuarioRequest.desde(historia))
                .collect(Collectors.toList());
    }

    private List<DatoBurnUpRequest> mapearListaDatosBurnUpARequest(List<DatoBurnUp> datosBurnUp) {
        return datosBurnUp.stream()
                .map(dato -> DatoBurnUpRequest.desde(dato))
                .collect(Collectors.toList());
    }
}
