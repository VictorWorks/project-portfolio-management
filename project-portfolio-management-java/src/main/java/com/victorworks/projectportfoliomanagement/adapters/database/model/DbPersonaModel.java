package com.victorworks.projectportfoliomanagement.adapters.database.model;

import com.victorworks.projectportfoliomanagement.domain.model.entities.Persona;
import com.victorworks.projectportfoliomanagement.domain.model.entities.PersonaId;
import com.victorworks.projectportfoliomanagement.domain.model.entities.ProyectoId;
import com.victorworks.projectportfoliomanagement.domain.model.entities.RolUsuario;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Persona")
public class DbPersonaModel {
    @Id
    String id;
    String nombre;
    String rol;
    String username;
    String password;
    String proyectoId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getProyectoId() {
        return proyectoId;
    }

    public void setProyectoId(String proyectoId) {
        this.proyectoId = proyectoId;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public DbPersonaModel(){}

    public DbPersonaModel(String id, String nombre, String nombreUsuario, String password, String rol, String proyectoId) {
        this.id = id;
        this.nombre = nombre;
        this.username = nombreUsuario;
        this.password = password;
        this.rol = rol;
        this.proyectoId = proyectoId;
    }

    public DbPersonaModel(String id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }
    public static DbPersonaModel desde(Persona persona) {
        String proyectoId = null;
        if (persona.getProyectoId() != null)
            proyectoId = persona.getProyectoId().toString();

        String rol = persona.getRol().toString();

        return new DbPersonaModel(persona.getId().toString(), persona.getNombre(),
                persona.getUsername(), persona.getPassword(), persona.getRol().toString(),  proyectoId);
    }

    public Persona haciaModelo() {
        ProyectoId proyectoIdModelo = null;
        if (proyectoId != null)
            proyectoIdModelo = ProyectoId.desde( proyectoId);
        RolUsuario rolUsuario = RolUsuario.parse(rol);
        return new Persona(PersonaId.desde(id), nombre, username, password,
                rolUsuario, proyectoIdModelo);
    }
}
