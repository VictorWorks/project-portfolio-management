package com.victorworks.projectportfoliomanagement.adapters.api.request;

import com.victorworks.projectportfoliomanagement.domain.model.entities.PuntoBurndown;
import com.victorworks.projectportfoliomanagement.domain.model.entities.ReporteBurndown;
import com.victorworks.projectportfoliomanagement.utils.Dates;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ReporteBurndownRequest {
    private String fechaInicio;
    private String fechaFin;
    private int puntosHistoria;
    private List<PuntoBurndownRequest> progreso;

    public ReporteBurndownRequest(String fechaInicio, String fechaFin, int puntosHistoria) {
        this.fechaInicio = fechaInicio;
        this.fechaFin = fechaFin;
        this.puntosHistoria = puntosHistoria;
        this.progreso = new ArrayList<>();
    }

    public String getFechaInicio() {
        return fechaInicio;
    }

    public String getFechaFin() {
        return fechaFin;
    }

    public int getPuntosHistoria() {
        return puntosHistoria;
    }

    public void setPuntosHistoria(int puntosHistoria) {
        this.puntosHistoria = puntosHistoria;
    }

    public List<PuntoBurndownRequest> getProgreso() {
        return progreso;
    }

    public ReporteBurndown haciaModelo() {
        ReporteBurndown resultado = new ReporteBurndown(Dates.parseFecha(fechaInicio),
                Dates.parseFecha(fechaFin), puntosHistoria);

        List<PuntoBurndown> progresoResultado = resultado.getProgreso();

        progresoResultado.addAll(
          progreso.stream()
                  .map(punto -> punto.haciaModelo())
                  .collect(Collectors.toList())
        );

        return resultado;
    }

    public static ReporteBurndownRequest desde(ReporteBurndown reporteBurndown) {
        ReporteBurndownRequest resultado = new ReporteBurndownRequest(
                Dates.formatearFecha(reporteBurndown.getFechaInicio()),
                Dates.formatearFecha(reporteBurndown.getFechaFin()),
                reporteBurndown.getPuntosHistoria()
        );

        List<PuntoBurndownRequest> progresoResultado = resultado.getProgreso();
        progresoResultado.addAll(
          reporteBurndown.getProgreso().stream()
                  .map(punto -> PuntoBurndownRequest.desde(punto))
                  .collect(Collectors.toList())
        );

        return  resultado;
    }
}
