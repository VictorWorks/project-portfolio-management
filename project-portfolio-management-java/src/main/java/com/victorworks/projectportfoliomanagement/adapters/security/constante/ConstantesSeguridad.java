package com.victorworks.projectportfoliomanagement.adapters.security.constante;

public class ConstantesSeguridad {
    public static final String METODO_HTTP_OPTIONS = "OPTIONS";

    public static final long TIEMPO_EXPIRACION_TOKEN = 54000000; // Quince minutos en milisegundos
    public static final String PREFIJO_BEARER = "Bearer ";
    public static final String CABECERA_HTTP_TOKEN = "Jwt_Token";

    public static final String EMISOR_TOKEN = "Victorworks";
    public static final String AUDIENCIA_TOKEN = "PROJECT PORTFOLIO MANAGEMENT API";

    public static final String PERMISOS = "permisos";

    // Mensajes de error
    public static final String ERROR_VERIFICANDO_TOKEN = "El token no se ha podido verificar.";
    public static final String ERROR_AUTENTICACION_NECESARIA = "Es necesario autenticarse para acceder a la página.";
    public static final String ERROR_ACCESO_DENEGADO = "No tienes permiso para acceder a la página.";

    // Rutas que no necesitan autenticación
    public static final String[] RUTAS_ABIERTAS = {"/v1/personas/login", "/v1/personas/registrar", "/v1/test","/h2-console/**"};

}
