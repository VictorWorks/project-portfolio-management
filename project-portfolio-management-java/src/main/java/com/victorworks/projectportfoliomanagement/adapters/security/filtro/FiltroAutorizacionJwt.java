package com.victorworks.projectportfoliomanagement.adapters.security.filtro;

import com.victorworks.projectportfoliomanagement.adapters.security.ManejadorToken;
import com.victorworks.projectportfoliomanagement.adapters.security.constante.ConstantesSeguridad;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@Component
public class FiltroAutorizacionJwt extends OncePerRequestFilter {
    private ManejadorToken manejadorToken;

    public FiltroAutorizacionJwt(ManejadorToken manejadorToken) {
        this.manejadorToken = manejadorToken;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        // Si la llamada es para descubir al servicio, no filtramos.
        if (esLlamadaHttpOptions(request)) {
            devolverRespuestaCorrecta(response);
            continuarProcesoFiltro(request, response, filterChain);
            return;
        }

        // La cabecera de autorización no va por token:
        // continuamos la cadena de filtros.
        String cabeceraAutorizacion = obtenerCaberaAutorization(request);
        if (!esCabeceraAutorizacionConToken(cabeceraAutorizacion)) {
            continuarProcesoFiltro(request, response, filterChain);
            return;
        }

        String token = obtenerTokenDeCabecera(cabeceraAutorizacion);
        String username = this.manejadorToken.obtenerUsuarioToken(token);

        // La cabecera contiene un token válido:
        // Se pasa la autenticación al proceso
        if (esTokenValidoConContextoAutenticacion(username, token)) {
            agregarAutenticacionAlContextoDeSeguridad(token, username, request, response, filterChain);
            return;
        }

        // Tenemos un token inválido,
        // así que se limpia el contexto de autenticación
        SecurityContextHolder.clearContext();
        continuarProcesoFiltro(request, response, filterChain);
    }

    private boolean esLlamadaHttpOptions(HttpServletRequest request) {
        return request.getMethod().equalsIgnoreCase(ConstantesSeguridad.METODO_HTTP_OPTIONS);
    }

    private void devolverRespuestaCorrecta(HttpServletResponse response) {
        response.setStatus(HttpStatus.OK.value());
    }

    private String obtenerCaberaAutorization(HttpServletRequest request) {
        return request.getHeader(HttpHeaders.AUTHORIZATION);
    }

    private boolean esCabeceraAutorizacionConToken(String cabeceraAutorizacion) {
        return (cabeceraAutorizacion != null && cabeceraAutorizacion.startsWith(ConstantesSeguridad.PREFIJO_BEARER));
    }

    private void continuarProcesoFiltro(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        filterChain.doFilter(request, response);
    }

    private String obtenerTokenDeCabecera(String cabecera) {
        if (cabecera.contains(ConstantesSeguridad.PREFIJO_BEARER))
        return cabecera.substring(ConstantesSeguridad.PREFIJO_BEARER.length());

        return cabecera;
    }

    private boolean esTokenValidoConContextoAutenticacion(String username, String token) {
        return this.manejadorToken.esTokenValido(username, token) &&
                SecurityContextHolder.getContext().getAuthentication() == null;
    }

    private void agregarAutenticacionAlContextoDeSeguridad(String token, String username,
                                                           HttpServletRequest request, HttpServletResponse response,
                                                           FilterChain filterChain) throws ServletException, IOException {
        List<GrantedAuthority> permisos = this.manejadorToken.obtenerPermisos(token);
        Authentication authenticacion = this.manejadorToken.obtenerAutenticacion(username, permisos, request);
        SecurityContextHolder.getContext().setAuthentication(authenticacion);
        continuarProcesoFiltro(request, response, filterChain);
    }
}
