package com.victorworks.projectportfoliomanagement.adapters.database.repository;

import com.victorworks.projectportfoliomanagement.adapters.database.dao.PersonaDAO;
import com.victorworks.projectportfoliomanagement.adapters.database.model.DbPersonaModel;
import com.victorworks.projectportfoliomanagement.domain.model.entities.Persona;
import com.victorworks.projectportfoliomanagement.domain.model.entities.PersonaId;
import com.victorworks.projectportfoliomanagement.domain.model.entities.ProyectoId;
import com.victorworks.projectportfoliomanagement.domain.ports.repositorio.PersonaRepository;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import static org.springframework.data.jpa.domain.Specification.where;

@Repository
public class DbPersonaRepository implements PersonaRepository {

    private final PersonaDAO personaDAO;

    public DbPersonaRepository(PersonaDAO personaDAO) {
        this.personaDAO = personaDAO;
    }

    @Override
    public Persona obtenerPorId(PersonaId id) {
        DbPersonaModel dbPersona = obtenerPorId(id.toString());
        if (dbPersona != null) {
            return dbPersona.haciaModelo();
        }
        return null;
    }

    @Override
    public Persona obtenerPorNombreUsuario(String nombreUsuario){
        DbPersonaModel dbPersona = personaDAO.findOne(
                where(nombreUsuarioEs(nombreUsuario))
        ).orElse(null);
        if (dbPersona != null) {
            return dbPersona.haciaModelo();
        }
        return null;
    }

    @Override
    public Persona crear(Persona persona) {
        final DbPersonaModel dbPersona = DbPersonaModel.desde(persona);
        final DbPersonaModel guardado = personaDAO.save(dbPersona);
        return guardado.haciaModelo();
    }

    private DbPersonaModel obtenerPorId(String id) {
        Optional<DbPersonaModel> dbPersona = personaDAO.findById(id);
        return dbPersona.orElse(null);
    }

    @Override
    public void actualizar(PersonaId personaId, Persona persona) {
        DbPersonaModel encontrado = obtenerPorId(personaId.toString());
        if (encontrado != null) {
            DbPersonaModel dbPersona = DbPersonaModel.desde(persona);
            dbPersona.setPassword(encontrado.getPassword());
            personaDAO.save(dbPersona);
        }
    }

    @Override
    public void borrar(PersonaId personaId) {
        if (obtenerPorId(personaId.toString()) != null) {
            personaDAO.deleteById(personaId.toString());
        }
    }

    @Override
    public List<Persona> todos() {
        List<DbPersonaModel> personasModel = personaDAO.findAll();
        return convertirListaHaciaModelo(personasModel);
    }

    @Override
    public List<Persona> porProyecto(ProyectoId proyectoId) {
        final String idProyecto = proyectoId == null ? null : proyectoId.toString();
        List<DbPersonaModel> personasModel = personaDAO.findAll(
                where(proyectoEs(idProyecto))
        );
        return convertirListaHaciaModelo(personasModel);
    }

    @Override
    public List<Persona> noIncluidosEnProyectoYNombreComo(ProyectoId proyectoId, String claveNombre){
        final String idProyectoAEmplear = proyectoId == null ? null: proyectoId.toString();
        List<DbPersonaModel> itemsObtenidos = personaDAO.findAll(
                where(nombreComo(claveNombre))
                        .and(
                                where(proyectoNoEs(idProyectoAEmplear)).or(proyectoEsNulo())
                        )
        );
        return convertirListaHaciaModelo(itemsObtenidos);
    }

    private List<Persona> convertirListaHaciaModelo(List<DbPersonaModel> listaModelo)
    {
        List<Persona> resultado = new ArrayList<>();
        for(DbPersonaModel p: listaModelo){
            resultado.add(p.haciaModelo());
        }
        return resultado;
    }

    public static Specification<DbPersonaModel> nombreComo(String mascaraNombre) {
        return (root, query, cb) -> (cb.like(cb.lower(root.get("nombre")), "%" + mascaraNombre.toLowerCase(Locale.ROOT) + "%"));
    }

    public static Specification<DbPersonaModel> proyectoEs(String proyectoId) {
        return (root, query, cb) -> cb.equal(root.get("proyectoId"), proyectoId);
    }

    public static Specification<DbPersonaModel> proyectoNoEs(String proyectoId) {
        return (root, query, cb) -> cb.notEqual(root.get("proyectoId"), proyectoId);
    }

    public static Specification<DbPersonaModel> proyectoEsNulo() {
        return (root, query, cb) -> cb.isNull(root.get("proyectoId"));
    }

    private static Specification<DbPersonaModel> nombreUsuarioEs(String nombreUsuario) {
        return (root, query, cb) -> cb.equal(root.get("username"), nombreUsuario);
    }
}
