package com.victorworks.projectportfoliomanagement.adapters.database.model;

import java.io.Serializable;
import java.util.Objects;

public class DbHistoriaEnSprintId implements Serializable {
    private static final long serialVersionUID = 1L;

    private String sprintId;
    private String historiaUsuarioId;

    public DbHistoriaEnSprintId() {

    }

    public DbHistoriaEnSprintId(String sprintId, String historiaUsuarioId){
        this.sprintId = sprintId;
        this.historiaUsuarioId = historiaUsuarioId;
    }

    public String getSprintId() {
        return this.sprintId;
    }

    public void setSprintId(String sprintId) {
        this.sprintId = sprintId;
    }

    public String getHistoriaUsuarioId() {
        return this.historiaUsuarioId;
    }

    public void setHistoriaUsuarioId(String historiaUsuarioId) {
        this.historiaUsuarioId = historiaUsuarioId;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((sprintId == null) ? 0 : sprintId.hashCode());
        result = prime * result
                + ((historiaUsuarioId == null) ? 0 : historiaUsuarioId.hashCode());
        return  result;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null)
            return false;
        if(getClass() != obj.getClass())
            return false;
        if (this == obj)
            return true;

        DbHistoriaEnSprintId otro = (DbHistoriaEnSprintId) obj;
        return Objects.equals(getSprintId(), otro.getSprintId()) &&
                Objects.equals(getHistoriaUsuarioId(), otro.getHistoriaUsuarioId());
    }
}
