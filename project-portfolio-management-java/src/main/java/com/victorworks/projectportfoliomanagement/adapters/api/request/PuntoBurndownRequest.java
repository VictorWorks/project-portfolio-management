package com.victorworks.projectportfoliomanagement.adapters.api.request;

import com.victorworks.projectportfoliomanagement.domain.model.entities.PuntoBurndown;
import com.victorworks.projectportfoliomanagement.utils.Dates;

public class PuntoBurndownRequest {
    private String fecha;
    private int puntosFinalizados;

    public PuntoBurndownRequest(String fecha, int puntosFinalizados) {
        this.fecha = fecha;
        this.puntosFinalizados = puntosFinalizados;
    }

    public String getFecha() {
        return fecha;
    }

    public int getPuntosFinalizados() {
        return puntosFinalizados;
    }

    public PuntoBurndown haciaModelo() {
        return new PuntoBurndown(Dates.parseFecha(this.fecha), this.puntosFinalizados);
    }

    public static PuntoBurndownRequest desde(PuntoBurndown punto) {
        return new PuntoBurndownRequest(Dates.formatearFecha(punto.getFecha()),
                punto.getPuntosFinalizados());
    }
}
