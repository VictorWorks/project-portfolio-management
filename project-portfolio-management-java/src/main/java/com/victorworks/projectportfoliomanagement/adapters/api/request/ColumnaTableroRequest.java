package com.victorworks.projectportfoliomanagement.adapters.api.request;

import com.victorworks.projectportfoliomanagement.domain.model.entities.ColumnaTablero;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ColumnaTableroRequest {
    private String proyectoId;
    private String nombre;
    private boolean esInicial;
    private boolean esFinal;
    private int orden;
    List<HistoriaUsuarioRequest> tareas;

    public ColumnaTableroRequest(String proyectoId, String nombre, boolean esInicial,
                          boolean esFinal, int orden) {
        this.proyectoId = proyectoId;
        this.nombre = nombre;
        this.esInicial = esInicial;
        this.esFinal = esFinal;
        this.orden = orden;
        this.tareas = new ArrayList<>();
    }

    public String getProyectoId() {
        return proyectoId;
    }

    public String getNombre() {
        return nombre;
    }

    public boolean getEsInicial() {
        return esInicial;
    }

    public boolean getEsFinal() {
        return esFinal;
    }

    public int getOrden() {
        return orden;
    }

    public void setOrden(int orden) {
        this.orden = orden;
    }

    public List<HistoriaUsuarioRequest> getTareas() {
        return tareas;
    }

    public static ColumnaTableroRequest desde(ColumnaTablero columna) {
        ColumnaTableroRequest resultado = new ColumnaTableroRequest(columna.getProyectoId().toString(),
                columna.getNombre(), columna.getEsInicial(), columna.getEsFinal(), columna.getOrden());

        List<HistoriaUsuarioRequest> historias = resultado.getTareas();
        historias.addAll(columna.getTareas().stream()
                .map(x -> HistoriaUsuarioRequest.desde(x))
                .collect(Collectors.toList()));

        return resultado;
    }
}
