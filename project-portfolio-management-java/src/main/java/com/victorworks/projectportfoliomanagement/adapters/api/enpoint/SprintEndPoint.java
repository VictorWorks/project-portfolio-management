package com.victorworks.projectportfoliomanagement.adapters.api.enpoint;

import com.victorworks.projectportfoliomanagement.adapters.api.request.HistoriaUsuarioRequest;
import com.victorworks.projectportfoliomanagement.adapters.api.request.ReporteBurndownRequest;
import com.victorworks.projectportfoliomanagement.adapters.api.request.SprintRequest;
import com.victorworks.projectportfoliomanagement.adapters.api.request.TableroTareasRequest;
import com.victorworks.projectportfoliomanagement.domain.model.entities.*;
import com.victorworks.projectportfoliomanagement.domain.ports.servicio.HistoriaUsuarioService;
import com.victorworks.projectportfoliomanagement.domain.ports.servicio.SprintService;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("v1/sprints")
public class SprintEndPoint {
    private final SprintService sprints;
    private final HistoriaUsuarioService historias;

    public SprintEndPoint(SprintService sprintService, HistoriaUsuarioService historiasService) {
        this.sprints = sprintService;
        this.historias = historiasService;
    }

    @GetMapping
    public List<SprintRequest> todos() {
        List<Sprint> sprintsModelo = sprints.todos();
        return MapearSprintsARequest(sprintsModelo);
    }

    @GetMapping("{sprintId}")
    public SprintRequest obtenerPorId(@PathVariable("sprintId") final String sprintId) {
        Sprint encontrado = sprints.obtenerPorId(SprintId.desde(sprintId));
        return SprintRequest.desde(encontrado);
    }

    @GetMapping("proyecto/{proyectoId}")
    public List<SprintRequest> obtenerSprintsProyecto(@PathVariable("proyectoId") final String proyectoId){
        ProyectoId id = ProyectoId.desde(proyectoId);
        List<Sprint> paraProyecto = sprints.obtenerPorProyecto(id);
        return MapearSprintsARequest(paraProyecto);
    }

    @GetMapping("{sprintId}/historiasUsuario")
    public List<HistoriaUsuarioRequest> obtenerHistoriasParaSprint(@PathVariable("sprintId") final String sprintId) {
        SprintId id = SprintId.desde(sprintId);
        List<HistoriaUsuario> paraSprint = historias.obtenerPorSprint(id);
        return MapearHistoriasARequest(paraSprint);
    }

    @GetMapping("{sprintId}/burndown")
    public ReporteBurndownRequest obtenerReporteBurndown(@PathVariable("sprintId") final String sprintId) {
        SprintId id = SprintId.desde(sprintId);
        ReporteBurndown reporte = sprints.obtenerReporteBurndown(id);
        return ReporteBurndownRequest.desde(reporte);
    }

    @GetMapping("{sprintId}/kanban")
    public TableroTareasRequest obtenerTableroTareas(@PathVariable("sprintId") final String sprintId) {
        SprintId id = SprintId.desde(sprintId);
        TableroTareas reporte = sprints.obtenerTableroTareas(id);
        return TableroTareasRequest.desde(reporte);
    }

    @PostMapping
    public SprintRequest crear(@RequestBody final SprintRequest sprint){
        Sprint creado = sprints.crear(sprint.haciaModelo());
        return SprintRequest.desde(creado);
    }

    @PutMapping("{sprintId}")
    public void actualizar(@PathVariable("sprintId") final String sprintId, @RequestBody final SprintRequest sprint) {
        sprints.actualizar(SprintId.desde(sprintId), sprint.haciaModelo());
    }

    @PutMapping("{sprintId}/agregarHistoria")
    public  void añadirHistoriaASprint(@PathVariable("sprintId") final String sprintId,
                                       @RequestBody final HistoriaUsuarioRequest historia) {
        SprintId idSprint = SprintId.desde(sprintId);
        HistoriaUsuario historiaModelo = historia.haciaModelo();
        sprints.añadirHistoriaASprint(idSprint, historiaModelo);
    }

    @PutMapping("{sprintId}/eliminarHistoria")
    public  void eliminarHistoriaDeSprint(@PathVariable("sprintId") final String sprintId,
                                       @RequestBody final HistoriaUsuarioRequest historia) {
        SprintId idSprint = SprintId.desde(sprintId);
        HistoriaUsuario historiaModelo = historia.haciaModelo();
        sprints.eliminarHistoriaDeSprint(idSprint, historiaModelo);
    }

    @PutMapping("{sprintId}/moverHistoria/{historiaId}/aEstado/{estadoId}")
    public void cambiarEstadoDeHistoria(@PathVariable("sprintId") final String sprintId,
                                        @PathVariable("historiaId") final String historiaId,
                                        @PathVariable("estadoId") final String estadoId,
                                        @RequestBody final HistoriaUsuarioRequest historia) {
        SprintId idSprint = SprintId.desde(sprintId);
        HistoriaUsuarioId idHistoria = HistoriaUsuarioId.desde(historiaId);
        EstadoTarea nuevoEstado = EstadoTarea.obtenerEstadoDesde(estadoId);
        sprints.cambiarEstadoDeHistoria(idSprint, idHistoria, nuevoEstado);
    }

    @DeleteMapping("{sprintId}")
    public void borrar(@PathVariable("sprintId")final String sprintId) {
        sprints.borrar(SprintId.desde(sprintId));
    }

    private List<SprintRequest> MapearSprintsARequest(List<Sprint> sprints){
        return sprints.stream()
                .map(sprint -> SprintRequest.desde(sprint))
                .collect(Collectors.toList());
    }

    private List<HistoriaUsuarioRequest> MapearHistoriasARequest(List<HistoriaUsuario> historias) {
        return historias.stream()
                .map(historia -> HistoriaUsuarioRequest.desde(historia))
                .collect(Collectors.toList());
    }
}
