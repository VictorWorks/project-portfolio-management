package com.victorworks.projectportfoliomanagement.adapters.api.request;

import com.victorworks.projectportfoliomanagement.domain.model.entities.ProyectoId;
import com.victorworks.projectportfoliomanagement.domain.model.entities.Sprint;
import com.victorworks.projectportfoliomanagement.domain.model.entities.SprintId;
import com.victorworks.projectportfoliomanagement.utils.Dates;

public class SprintRequest {
    private final String id;
    private final String proyectoId;
    private final String nombre;
    private final String fechaInicio;
    private final String fechaFin;
    private final int puntosHistoria;

    public String getId() {
        return id;
    }

    public String getProyectoId() {
        return proyectoId;
    }

    public String getNombre() {
        return nombre;
    }

    public String getFechaInicio() {
        return fechaInicio;
    }

    public String getFechaFin() {
        return fechaFin;
    }

    public int getPuntosHistoria() {
        return puntosHistoria;
    }

    public Sprint haciaModelo() {
        return new Sprint(SprintId.desde(id), nombre, Dates.parseFecha(fechaInicio),
                Dates.parseFecha(fechaFin), puntosHistoria, ProyectoId.desde(proyectoId));
    }

    public SprintRequest(String id, String nombre, String fechaInicio, String fechaFin,
                         int puntosHistoria, String proyectoId) {
        this.id = id;
        this.nombre = nombre;
        this.fechaFin = fechaFin;
        this.fechaInicio = fechaInicio;
        this.puntosHistoria = puntosHistoria;
        this.proyectoId = proyectoId;
    }

    public static SprintRequest desde(Sprint sprint) {
        String proyectoId = null;
        if (sprint.getProyectoId() != null)
            proyectoId = sprint.getProyectoId().toString();

        String fechaInicio = Dates.formatearFecha(sprint.getFechaInicio());
        String fechaFin = Dates.formatearFecha(sprint.getFechaFin());
        return new SprintRequest(sprint.getId().toString(),
                sprint.getNombre(), fechaInicio, fechaFin,
                sprint.getPuntosHistoria(), proyectoId);
    }


}
