package com.victorworks.projectportfoliomanagement.adapters.database.dao;

import com.victorworks.projectportfoliomanagement.adapters.database.model.DbHistoriaUsuarioModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface HistoriaUsuarioDAO extends JpaRepository<DbHistoriaUsuarioModel, String>,
        JpaSpecificationExecutor<DbHistoriaUsuarioModel> {
}
