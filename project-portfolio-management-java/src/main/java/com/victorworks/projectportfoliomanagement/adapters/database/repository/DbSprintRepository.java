package com.victorworks.projectportfoliomanagement.adapters.database.repository;

import com.victorworks.projectportfoliomanagement.adapters.database.dao.HistoriaEnSprintDAO;
import com.victorworks.projectportfoliomanagement.adapters.database.dao.HistoriaUsuarioDAO;
import com.victorworks.projectportfoliomanagement.adapters.database.dao.SprintDAO;
import com.victorworks.projectportfoliomanagement.adapters.database.model.DbHistoriaEnSprintModel;
import com.victorworks.projectportfoliomanagement.adapters.database.model.DbHistoriaUsuarioModel;
import com.victorworks.projectportfoliomanagement.adapters.database.model.DbSprintModel;
import com.victorworks.projectportfoliomanagement.domain.model.entities.*;
import com.victorworks.projectportfoliomanagement.domain.ports.repositorio.SprintRepository;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.springframework.data.jpa.domain.Specification.where;

@Component
public class DbSprintRepository implements SprintRepository {
    private final SprintDAO sprintDAO;
    private final HistoriaUsuarioDAO historiasDAO;
    private final HistoriaEnSprintDAO historiasEnSprintDAO;

    public DbSprintRepository(SprintDAO sprintDAO, HistoriaUsuarioDAO historiasDAO,
                              HistoriaEnSprintDAO historiasEnSprintDAO) {
        this.sprintDAO = sprintDAO;
        this.historiasDAO = historiasDAO;
        this.historiasEnSprintDAO = historiasEnSprintDAO;
    }

    @Override
    public Sprint obtenerPorId(SprintId id) {
        Optional<DbSprintModel> encontrado = sprintDAO.findById(id.toString());
        return encontrado.map(DbSprintModel::haciaModelo).orElse(null);
    }

    @Override
    public Sprint crear(Sprint sprint) {
        DbSprintModel guardado = sprintDAO.save(DbSprintModel.desde(sprint));
        return guardado.haciaModelo();
    }

    @Override
    public void actualizar(SprintId sprintId, Sprint sprint) {
        if (obtenerPorId(sprintId) != null) {
            sprintDAO.save(DbSprintModel.desde(sprint));
        }
    }

    @Override
    public void añadirHistoriaASprint(HistoriaEnSprint historia) {
        DbHistoriaEnSprintModel historiaModel = DbHistoriaEnSprintModel.desde(historia);
        historiasEnSprintDAO.save(historiaModel);
    }

    @Override
    public void eliminarHistoriaDeSprint(HistoriaEnSprint historia) {
        DbHistoriaEnSprintModel historiaModel = DbHistoriaEnSprintModel.desde(historia);
        historiasEnSprintDAO.delete(historiaModel);
    }

    @Override
    public void borrar(SprintId sprintId) {
        sprintDAO.deleteById(sprintId.toString());
    }

    @Override
    public List<Sprint> todos() {
        List<DbSprintModel> sprintsDb = sprintDAO.findAll();
        return convertirListaHaciaModelo(sprintsDb);
    }

    @Override
    public List<Sprint> porProyecto(ProyectoId proyectoId) {
        final String idProyecto = proyectoId == null ? null : proyectoId.toString();
        List<DbSprintModel> sprintsModel = sprintDAO.findAll(
                where(proyectoEs(idProyecto))
        );
        return convertirListaHaciaModelo(sprintsModel);
    }

    @Override
    public List<HistoriaEnSprint> historiasEnSprintParaSprint(SprintId id) {
        List<HistoriaEnSprint> resultado = new ArrayList<>();
        final String idSprint = id.toString();
        List<DbHistoriaEnSprintModel> encontrados = historiasEnSprintDAO.findAll(
                where(idSprintEs(idSprint))
        );

        for (DbHistoriaEnSprintModel encontrado : encontrados) {
            resultado.add(encontrado.haciaModelo());
        }

        return resultado;
    }

    @Override
    public HistoriaEnSprint historiaEnSprintParaHistoria(SprintId sprintId, HistoriaUsuarioId historiaId) {
        final String idSprint = sprintId == null ? null : sprintId.toString();
        final String idHistoria = historiaId == null ? null : historiaId.toString();

        DbHistoriaEnSprintModel encontrado = historiasEnSprintDAO.findOne(
                where(perteneceASprint(idSprint)).and(perteneceAHistoria(idHistoria))
        ).orElse(null);

        if (encontrado == null)
            return null;
        return encontrado.haciaModelo();
    }

    @Override
    public List<HistoriaUsuario> historiasUsuarioParaSprint(SprintId sprintId) {
        final String idSprint = sprintId == null ? null : sprintId.toString();
        List<DbHistoriaUsuarioModel> encontradas = historiasDAO.findAll(
                where(historiaUsuarioEstaEnSprint(idSprint))
        );

        return convertirListaHistoriasUsuarioHaciaModelo(encontradas);
    }

    @Override
    public List<HistoriaEnSprint> obtenerHistorasEnSprintParaSprints(SprintId sprintId) {
        final String idSprint = sprintId == null ? null : sprintId.toString();
        List<DbHistoriaEnSprintModel> encontradas = historiasEnSprintDAO.findAll(
                where(perteneceASprint(idSprint))
        );
        return convertirListaHistoriasEnSprintHaciaModelo(encontradas);
    }

    private Specification<DbHistoriaEnSprintModel> perteneceASprint(String idSprint) {
        return (root, query, cb) -> cb.equal(root.get("sprint").get("id"), idSprint);
    }

    private Specification<DbHistoriaEnSprintModel> perteneceAHistoria(String idHistoria) {
        return (root, query, cb) -> cb.equal(root.get("historiaUsuario").get("id"), idHistoria);
    }

    private Specification<DbHistoriaUsuarioModel> historiaUsuarioEstaEnSprint(String idSprint) {
        return (root, query, cb) -> cb.equal(root.join("sprints").get("sprint").get("id"), idSprint);
    }

    private Specification<DbSprintModel> proyectoEs(String proyectoId) {
        return (root, query, cb) -> cb.equal(root.get("proyectoId"), proyectoId);
    }

    private List<Sprint> convertirListaHaciaModelo(List<DbSprintModel> listaModelo) {
        List<Sprint> resultado = new ArrayList<>();
        for (DbSprintModel p : listaModelo) {
            resultado.add(p.haciaModelo());
        }
        return resultado;
    }

    private List<HistoriaUsuario> convertirListaHistoriasUsuarioHaciaModelo(List<DbHistoriaUsuarioModel> historias) {
        List<HistoriaUsuario> resultado = new ArrayList<>();
        for (DbHistoriaUsuarioModel historia : historias) {
            resultado.add(historia.haciaModelo());
        }
        return resultado;
    }

    private List<HistoriaEnSprint> convertirListaHistoriasEnSprintHaciaModelo(List<DbHistoriaEnSprintModel> historiasEnSprint) {
        List<HistoriaEnSprint> resultado = new ArrayList<>();
        for (DbHistoriaEnSprintModel historiaEnSprint : historiasEnSprint) {
            resultado.add(historiaEnSprint.haciaModelo());
        }
        return resultado;
    }

    private Specification<DbHistoriaEnSprintModel> idSprintEs(String idSprint) {
        return (root, query, cb) -> cb.equal(root.get("sprint").get("id"), idSprint);
    }
}
