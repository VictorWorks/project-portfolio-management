package com.victorworks.projectportfoliomanagement.adapters.database.dao;

import com.victorworks.projectportfoliomanagement.adapters.database.model.DbProyectoModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProyectoDAO extends JpaRepository<DbProyectoModel, String> {
}
