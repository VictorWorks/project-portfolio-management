package com.victorworks.projectportfoliomanagement.adapters.api.request;

import com.victorworks.projectportfoliomanagement.domain.model.entities.TableroTareas;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class TableroTareasRequest {
    private String nombre;
    private List<ColumnaTableroRequest> columnas;

    public TableroTareasRequest(String nombre) {
        this.nombre = nombre;
        this.columnas = new ArrayList<>();
    }

    public String getNombre() {
        return nombre;
    }

    public List<ColumnaTableroRequest> getColumnas() {
        return columnas;
    }

    public static TableroTareasRequest desde(TableroTareas tablero) {
        TableroTareasRequest resultado = new TableroTareasRequest(tablero.getNombre());

        List<ColumnaTableroRequest> columnas = resultado.getColumnas();
        columnas.addAll(
          tablero.getColumnas().stream()
                  .map(columna -> ColumnaTableroRequest.desde(columna))
                  .collect(Collectors.toList())
        );

        return resultado;
    }
}
