package com.victorworks.projectportfoliomanagement.adapters.api.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.victorworks.projectportfoliomanagement.domain.model.entities.Persona;
import com.victorworks.projectportfoliomanagement.domain.model.entities.PersonaId;
import com.victorworks.projectportfoliomanagement.domain.model.entities.ProyectoId;
import com.victorworks.projectportfoliomanagement.domain.model.entities.RolUsuario;

public class PersonaRequest {
    String id;
    String nombre;
    String username;
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    String password;
    String rol;
    String proyectoId;

    public String getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public String getProyectoId() {
        return proyectoId;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getRol() {
        return rol;
    }

    public Persona haciaModelo() {
        RolUsuario rolUsuario = RolUsuario.parse(rol);

        return new Persona(PersonaId.desde(id), nombre, username, password,
                rolUsuario, ProyectoId.desde(proyectoId));
    }

    public PersonaRequest(String id, String nombre, String username,  String password, String rol, String proyectoId) {
        this.id = id;
        this.nombre = nombre;
        this.username = username;
        this.password = password;
        this.rol = rol;
        this.proyectoId = proyectoId;
    }

    public static PersonaRequest desde(Persona persona) {
        String proyectoId = null;
        if (persona.getProyectoId() != null)
            proyectoId = persona.getProyectoId().toString();
        return new PersonaRequest(persona.getId().toString(), persona.getNombre(),
                persona.getUsername(), persona.getPassword(), persona.getRol().toString(), proyectoId);
    }
}
