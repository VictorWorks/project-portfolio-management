package com.victorworks.projectportfoliomanagement.adapters.database.dao;

import com.victorworks.projectportfoliomanagement.adapters.database.model.DbHistoriaEnSprintId;
import com.victorworks.projectportfoliomanagement.adapters.database.model.DbHistoriaEnSprintModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface HistoriaEnSprintDAO extends JpaRepository<DbHistoriaEnSprintModel, DbHistoriaEnSprintId>,
        JpaSpecificationExecutor<DbHistoriaEnSprintModel> {
}
