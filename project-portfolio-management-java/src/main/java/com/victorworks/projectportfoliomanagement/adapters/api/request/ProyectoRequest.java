package com.victorworks.projectportfoliomanagement.adapters.api.request;

import com.victorworks.projectportfoliomanagement.domain.model.entities.Proyecto;
import com.victorworks.projectportfoliomanagement.domain.model.entities.ProyectoId;

import java.util.List;

public class ProyectoRequest {
    private String id;
    private String nombre;
    private List<PersonaRequest> equipo;

    public String getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public List<PersonaRequest> getEquipo() {
        return equipo;
    }

    private ProyectoRequest(String id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }

    public static ProyectoRequest desde(Proyecto proyecto) {
        return new ProyectoRequest(proyecto.getId().toString(), proyecto.getNombre());
    }

    public Proyecto haciaModelo() {
        return new Proyecto(ProyectoId.desde(id), nombre);
    }
}
