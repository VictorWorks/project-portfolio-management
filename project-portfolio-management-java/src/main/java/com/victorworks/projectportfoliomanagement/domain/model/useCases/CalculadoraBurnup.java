package com.victorworks.projectportfoliomanagement.domain.model.useCases;

import com.victorworks.projectportfoliomanagement.domain.model.entities.*;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class CalculadoraBurnup {

    private final List<Sprint> sprints;
    private final List<HistoriaUsuario> historias;
    private final List<HistoriaEnSprint> historiasEnSprint;
    private final ProyectoId proyectoId;

    public CalculadoraBurnup(List<Sprint> sprints, List<HistoriaUsuario> historias,
                             List<HistoriaEnSprint> historiasEnSprint) {
        this.sprints = sprints;
        this.historias = historias;
        this.historiasEnSprint = historiasEnSprint;

        this.proyectoId = obtenerProyectoId();
    }

    private ProyectoId obtenerProyectoId() {
        if (!haySprints()) {
            return null;
        }
        return this.sprints.get(0).getProyectoId();
    }

    public List<DatoBurnUp> obtenerDatosBurnUp() {
        if (!haySprints()) {
            return obtenerDatosSinSprint();
        }

        return obtenerDatosBurnUpDeSprints();
    }

    private List<DatoBurnUp> obtenerDatosSinSprint() {
        List<DatoBurnUp> resultado = new ArrayList<>();
        resultado.add(obtenerDatoBurnUpSinSprint(obtenerSumaEstimacionHistorias()));
        return resultado;
    }

    private DatoBurnUp obtenerDatoBurnUpSinSprint(int puntosPendientes) {
        String nombreSprint = "Inicio";
        int puntosCompletados = 0;
        return new DatoBurnUp(nombreSprint, puntosCompletados, puntosPendientes);
    }

    private int obtenerSumaEstimacionHistorias() {
        return this.historias.stream()
                .map(y -> y.getEstimacion())
                .reduce(0, (z, w) -> z + w);
    }

    private List<DatoBurnUp> obtenerDatosBurnUpDeSprints() {
        insertarSprintFicticioDeFin();

        return crearDatosBurnUpParaSprints();
    }

    private List<DatoBurnUp> crearDatosBurnUpParaSprints() {
        List<DatoBurnUp> resultado = new ArrayList<>();
        int acumuladoTerminado = 0;

        List<Sprint> sprintsOrdenados = ordenarSprintsPorFechaInicio(this.sprints);
        for (Sprint s : sprintsOrdenados) {
            int pendientes = obtenerPuntosHistoriaEnBacklogDuranteSprint(s);
            insertarDatoPrevioSprints(resultado, sprintsOrdenados, s, pendientes);

            acumuladoTerminado += obtenerTotalPuntosHistoriaCompletados(s);
            DatoBurnUp dato = new DatoBurnUp(s.getNombre(), acumuladoTerminado, pendientes);
            resultado.add(dato);
        }
        return resultado;
    }

    private void insertarDatoPrevioSprints(List<DatoBurnUp> resultado, List<Sprint> sprintsOrdenados, Sprint s, int pendientes) {
        if (esPrimerSprintDelProyecto(sprintsOrdenados, s)) {
            DatoBurnUp datoInicial = obtenerDatoBurnUpSinSprint(pendientes);
            resultado.add(datoInicial);
        }
    }

    private boolean esPrimerSprintDelProyecto(List<Sprint> sprintsProyecto, Sprint sprint) {
        return sprintsProyecto.indexOf(sprint) == 0;
    }

    private List<Sprint> ordenarSprintsPorFechaInicio(List<Sprint> sprints) {
        return sprints.stream()
                .sorted(Comparator.comparing(Sprint::getFechaInicio))
                .collect(Collectors.toList());
    }

    private boolean haySprints() {
        return (this.sprints != null && this.sprints.size() > 0);
    }

    private boolean hayHistoriasUsuario() {
        return (this.historias != null && this.historias.size() > 0);
    }

    private Sprint obtenerUltimoSprintDelProyecto() {
        return this.sprints.stream()
                .max(Comparator.comparing(Sprint::getFechaFin))
                .orElse(null);
    }

    private HistoriaUsuario obtenerUltimaHistoriaUsuario() {
        return this.historias.stream().
                max(Comparator.comparing(HistoriaUsuario::getFechaAlta))
                .orElse(null);
    }

    private boolean hayHistoriasTrasUltimoSprint(Sprint ultimo) {
        long historiasTrasUltimoSprint = this.historias.stream()
                .filter(x -> x.getFechaAlta().after(ultimo.getFechaFin()))
                .count();
        return (historiasTrasUltimoSprint > 0);
    }

    private void insertarSprintFicticioDeFin() {
        if (!haySprints() || !hayHistoriasUsuario()) {
            return;
        }

        Sprint ultimoSprint = obtenerUltimoSprintDelProyecto();
        if (hayHistoriasTrasUltimoSprint(ultimoSprint)) {
            HistoriaUsuario ultimaHistoria = obtenerUltimaHistoriaUsuario();
            Sprint s = new Sprint(SprintId.desde(UUID.randomUUID()), "Pdte. sprint", ultimoSprint.getFechaFin(),
                    ultimaHistoria.getFechaAlta(), 0, this.proyectoId);
            this.sprints.add(s);
        }
    }

    private int obtenerPuntosHistoriaEnBacklogDuranteSprint(Sprint sprint) {
        return historias.stream()
                .filter(x -> x.getFechaAlta().before(sprint.getFechaFin()) ||
                        x.getFechaAlta().equals(sprint.getFechaFin()))
                .map(y -> y.getEstimacion())
                .reduce(0, (z, w) -> z + w);
    }

    private int obtenerTotalPuntosHistoriaCompletados(Sprint s) {
        List<HistoriaUsuario> terminadas = obtenerHistoriasTerminadasParaSprint(s);
        return terminadas.stream()
                .map(x -> x.getEstimacion())
                .reduce(0, (x, y) -> x + y);
    }

    private List<HistoriaUsuario> obtenerHistoriasTerminadasParaSprint(Sprint s) {
        List<HistoriaEnSprint> historiasSprintTerminadas = this.historiasEnSprint.stream()
                .filter(x -> esHistoriaTerminada(x) && esHistoriaDeSprint(x, s))
                .collect(Collectors.toList());

        List<HistoriaUsuario> resultado = new ArrayList<>();
        for (HistoriaEnSprint historiaSprintTerminada : historiasSprintTerminadas) {
            HistoriaUsuarioId idHistoria = historiaSprintTerminada.getHistoriaUsuario().getId();
            HistoriaUsuario historiaTerminada = historias.stream()
                    .filter(x -> x.getId().equals(idHistoria))
                    .findFirst().orElse(null);

            if (historiaTerminada != null) {
                resultado.add(historiaTerminada);
            }
        }

        return resultado;
    }

    private boolean esHistoriaTerminada(HistoriaEnSprint historia) {
        return historia.getEstado().equals(EstadoTarea.Terminado);
    }

    private boolean esHistoriaDeSprint(HistoriaEnSprint historia, Sprint sprint) {
        return historia.getSprint().getId().equals(sprint.getId());
    }
}
