package com.victorworks.projectportfoliomanagement.domain.model.entities;

import java.util.UUID;

public class PersonaId extends EntidadId{

    private PersonaId(final UUID value){
        super(value);
    }

    public static PersonaId desde(final UUID personaId){
        return new PersonaId(personaId);
    }

    public static PersonaId desde(final String rawId){
        if (rawId == null || rawId.isEmpty()) {
            return null;
        }

        UUID id = UUID.fromString(rawId);
        return new PersonaId(id);
    }
}
