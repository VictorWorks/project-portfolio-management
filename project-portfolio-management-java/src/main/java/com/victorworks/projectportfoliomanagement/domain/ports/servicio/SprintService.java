package com.victorworks.projectportfoliomanagement.domain.ports.servicio;

import com.victorworks.projectportfoliomanagement.domain.model.entities.*;
import com.victorworks.projectportfoliomanagement.domain.model.useCases.CalculadoraBurndown;
import com.victorworks.projectportfoliomanagement.domain.model.useCases.CalculadoraTableroTareas;
import com.victorworks.projectportfoliomanagement.domain.ports.repositorio.HistoriaEnSprintRepository;
import com.victorworks.projectportfoliomanagement.domain.ports.repositorio.HistoriaUsuarioRepository;
import com.victorworks.projectportfoliomanagement.domain.ports.repositorio.ProyectoRepository;
import com.victorworks.projectportfoliomanagement.domain.ports.repositorio.SprintRepository;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Date;

@Component
public class SprintService {
    private final SprintRepository sprintRepository;
    private final ProyectoRepository proyectoRepository;
    private final HistoriaUsuarioRepository historiaUsuarioRepository;
    private final HistoriaEnSprintRepository historiaEnSprintRepository;

    public SprintService(SprintRepository sprintRepository,
                         ProyectoRepository proyectoRepository,
                         HistoriaUsuarioRepository historiaUsuarioRepository,
                         HistoriaEnSprintRepository historiaEnSprintRepository) {
        this.sprintRepository = sprintRepository;
        this.proyectoRepository = proyectoRepository;
        this.historiaUsuarioRepository = historiaUsuarioRepository;
        this.historiaEnSprintRepository = historiaEnSprintRepository;
    }

    public List<Sprint> todos() {
        return this.sprintRepository.todos();
    }

    public Sprint obtenerPorId(SprintId SprintId) {
        return sprintRepository.obtenerPorId(SprintId);
    }

    public List<Sprint> obtenerPorProyecto(ProyectoId proyectoId) {
        return sprintRepository.porProyecto(proyectoId);
    }

    public Sprint crear(Sprint Sprint) {
        return sprintRepository.crear(Sprint);
    }

    public void actualizar(SprintId SprintId, Sprint Sprint) {
        sprintRepository.actualizar(SprintId, Sprint);
    }

    public void borrar(SprintId SprintId) {
        sprintRepository.borrar(SprintId);
    }

    public void añadirHistoriaASprint(SprintId idSprint, HistoriaUsuario historia) {
        Sprint sprint = this.sprintRepository.obtenerPorId(idSprint);
        if (sprint != null) {
            List<HistoriaEnSprint> historias = this.sprintRepository.historiasEnSprintParaSprint(sprint.getId());
            if (noHayHistoriaParaSprint(sprint, historia, historias)) {
                HistoriaEnSprint historiaEnSprint = new HistoriaEnSprint(sprint, historia,
                        EstadoTarea.ALaEspera, Calendar.getInstance().getTime());
                historia.setEstadoActual(EstadoTarea.ALaEspera);
                sprint.agregarHistoriaASprint(historiaEnSprint);

                sprintRepository.añadirHistoriaASprint(historiaEnSprint);
            }
        }
    }

    public void crearHistoriaEnSprint(HistoriaEnSprint historiaEnSprint) {
        sprintRepository.añadirHistoriaASprint(historiaEnSprint);
    }

    public void eliminarHistoriaDeSprint(SprintId idSprint, HistoriaUsuario historia) {
        Sprint sprint = this.sprintRepository.obtenerPorId(idSprint);
        if (sprint != null) {
            List<HistoriaEnSprint> historias = this.sprintRepository.historiasEnSprintParaSprint(sprint.getId());
            HistoriaEnSprint historiaEnSprint = obtenerHistoriaEnSprint(historias, sprint, historia);
            if (historiaEnSprint != null)
                this.sprintRepository.eliminarHistoriaDeSprint(historiaEnSprint);
        }
    }

    private boolean noHayHistoriaParaSprint(Sprint sprint, HistoriaUsuario historiaUsuario,
                                            List<HistoriaEnSprint> historias) {
        return obtenerHistoriaEnSprint(historias, sprint, historiaUsuario) == null;
    }

    private HistoriaEnSprint obtenerHistoriaEnSprint(List<HistoriaEnSprint> historias, Sprint sprint,
                                                     HistoriaUsuario historiaUsuario) {
        HistoriaEnSprint resultado = null;
        for (HistoriaEnSprint historiaSprint : historias) {
            if (sprint.getId().equals(historiaSprint.getSprint().getId()) &&
                    historiaUsuario.getId().equals(historiaSprint.getHistoriaUsuario().getId())) {
                resultado = historiaSprint;
            }
        }
        return resultado;
    }

    public ReporteBurndown obtenerReporteBurndown(SprintId idSprint) {

        Sprint sprint = this.sprintRepository.obtenerPorId(idSprint);
        if (sprint == null)
            return null;
        List<HistoriaUsuario> historias = this.sprintRepository.historiasUsuarioParaSprint(idSprint);
        List<HistoriaEnSprint> historiaEnSprints = new ArrayList<>();
        for (HistoriaUsuario historia : historias) {
            HistoriaEnSprint historiaEnSprint = this.sprintRepository.historiaEnSprintParaHistoria(idSprint, historia.getId());
            historiaEnSprints.add(historiaEnSprint);
        }

        CalculadoraBurndown calculadora = new CalculadoraBurndown(sprint, historias, historiaEnSprints);
        return calculadora.calcular();
    }

    public TableroTareas obtenerTableroTareas(SprintId idSprint) {
        Sprint sprint = this.sprintRepository.obtenerPorId(idSprint);
        if (sprint == null)
            return null;

        List<HistoriaUsuario> historias = this.sprintRepository.historiasUsuarioParaSprint(idSprint);
        List<HistoriaEnSprint> historiaEnSprints = new ArrayList<>();
        for (HistoriaUsuario historia : historias) {
            HistoriaEnSprint historiaEnSprint = this.sprintRepository.historiaEnSprintParaHistoria(idSprint,
                    historia.getId());
            historiaEnSprints.add(historiaEnSprint);
        }

        List<ColumnaTablero> columnas = this.proyectoRepository.obtenerEstadosTareaParaProyecto(sprint.getProyectoId());

        CalculadoraTableroTareas calculadora = new CalculadoraTableroTareas(columnas, historiaEnSprints);
        return calculadora.calcular();

    }

    public void cambiarEstadoDeHistoria(SprintId idSprint, HistoriaUsuarioId idHistoria,
                                        EstadoTarea nuevoEstado) {
        HistoriaUsuario historia = this.historiaUsuarioRepository.obtenerPorId(idHistoria);
        HistoriaEnSprint paraModificar = sprintRepository.historiaEnSprintParaHistoria(idSprint, idHistoria);

        if(historia == null || paraModificar == null)
            return;

        paraModificar.setEstado(nuevoEstado);
        final Date hoy = Calendar.getInstance().getTime();
        paraModificar.setFechaModificacion(hoy);
        this.historiaEnSprintRepository.actualizar(paraModificar);

        historia.setEstadoActual(nuevoEstado);
        this.historiaUsuarioRepository.actualizar(idHistoria, historia);
    }

}
