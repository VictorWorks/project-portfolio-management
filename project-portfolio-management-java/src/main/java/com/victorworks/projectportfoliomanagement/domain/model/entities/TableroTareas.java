package com.victorworks.projectportfoliomanagement.domain.model.entities;

import java.util.ArrayList;
import java.util.List;

public class TableroTareas {
    private String nombre;
    private List<ColumnaTablero> columnas;

    public  TableroTareas(String nombre) {
        this.nombre = nombre;
        this.columnas = new ArrayList<>();
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public List<ColumnaTablero> getColumnas() {
        return columnas;
    }

    public void setColumnas(List<ColumnaTablero> columnas) {
        this.columnas = columnas;
    }
}
