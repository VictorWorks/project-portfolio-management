package com.victorworks.projectportfoliomanagement.domain.model.useCases;

import com.victorworks.projectportfoliomanagement.domain.model.entities.ColumnaTablero;
import com.victorworks.projectportfoliomanagement.domain.model.entities.HistoriaEnSprint;
import com.victorworks.projectportfoliomanagement.domain.model.entities.HistoriaUsuario;
import com.victorworks.projectportfoliomanagement.domain.model.entities.TableroTareas;

import java.util.List;
import java.util.stream.Collectors;

public class CalculadoraTableroTareas {
    private List<ColumnaTablero> columnas;
    private List<HistoriaEnSprint> historiasEnSprint;

    public CalculadoraTableroTareas(List<ColumnaTablero> columnas, List<HistoriaEnSprint> historiasEnSprint) {
        this.columnas = columnas;
        this.historiasEnSprint = historiasEnSprint;
    }

    public TableroTareas calcular() {
        TableroTareas resultado = new TableroTareas("Estado de las historias de usuario");
        asignarColumnasATablero(resultado);
        filtrarTareasPorColumnas(resultado);
        return resultado;
    }

    private void asignarColumnasATablero(TableroTareas tablero) {
        tablero.getColumnas().addAll(this.columnas);
    }

    private void filtrarTareasPorColumnas(TableroTareas tablero) {
        for (ColumnaTablero columna: tablero.getColumnas()) {
            List<HistoriaUsuario> filtradas = this.historiasEnSprint.stream()
                    .filter(x -> x.getEstado().toString().equals(columna.getNombre()))
                    .map(y -> y.getHistoriaUsuario())
                    .collect(Collectors.toList());
            columna.setTareas(filtradas);
        }
    }

}
