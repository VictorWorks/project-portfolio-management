package com.victorworks.projectportfoliomanagement.domain.ports.excepcion;

public class NombreUsuarioCambiadoException extends Throwable {
    public NombreUsuarioCambiadoException(String mensaje){
        super(mensaje);
    }
}
