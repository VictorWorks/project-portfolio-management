package com.victorworks.projectportfoliomanagement.domain.model.entities;

import com.victorworks.projectportfoliomanagement.domain.model.constante.ConstantesPermisos;

public enum RolUsuario {
    ADMINISTRADOR(ConstantesPermisos.PERMISOS_ADMINISTRADOR),
    RRHH(ConstantesPermisos.PERMISOS_RRHH),
    MANAGER(ConstantesPermisos.PERMISOS_MANAGER),
    DESARROLLADOR(ConstantesPermisos.PERMISOS_DESARROLLADOR);

    private String[] permisos;

    RolUsuario(String[] permisos) {
        this.permisos = permisos;
    }

    public String[] getPermisos() {
        return this.permisos;
    }

    public static RolUsuario parse(String rol) {
        RolUsuario rolUsuario = RolUsuario.DESARROLLADOR;
        try {
            rolUsuario = RolUsuario.valueOf(rol.toUpperCase());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rolUsuario;
    }
}
