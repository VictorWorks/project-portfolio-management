package com.victorworks.projectportfoliomanagement.domain.ports.servicio;

import com.victorworks.projectportfoliomanagement.domain.model.entities.Proyecto;
import com.victorworks.projectportfoliomanagement.domain.model.entities.ProyectoId;
import com.victorworks.projectportfoliomanagement.domain.ports.repositorio.ProyectoRepository;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ProyectoService {
    private final ProyectoRepository repository;

    public ProyectoService(ProyectoRepository repository) {
        this.repository = repository;
    }

    public List<Proyecto> todos() {
        return this.repository.todos();
    }

    public Proyecto obtenerPorId(ProyectoId id){
        return this.repository.obtenerPorId(id);
    }

    public Proyecto crear (Proyecto proyecto){
        return this.repository.crear(proyecto);
    }

    public void actualizar(ProyectoId id, Proyecto proyecto){
        this.repository.actualizar(id, proyecto);
    }

    public void borrar(ProyectoId id){
        this.repository.borrar(id);
    }
}
