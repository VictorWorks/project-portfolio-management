package com.victorworks.projectportfoliomanagement.domain.ports.repositorio;

import com.victorworks.projectportfoliomanagement.domain.model.entities.ColumnaTablero;
import com.victorworks.projectportfoliomanagement.domain.model.entities.Proyecto;
import com.victorworks.projectportfoliomanagement.domain.model.entities.ProyectoId;

import java.util.List;

public interface ProyectoRepository {
    List<Proyecto> todos();

    Proyecto obtenerPorId(ProyectoId id);

    Proyecto crear(Proyecto proyecto);

    void actualizar(ProyectoId proyectoId, Proyecto proyecto);

    void borrar(ProyectoId proyectoId);

    List<ColumnaTablero> obtenerEstadosTareaParaProyecto(ProyectoId proyectoId);
}
