package com.victorworks.projectportfoliomanagement.domain.model.entities;

import java.util.UUID;

public class ProyectoId extends EntidadId{

    private ProyectoId(final UUID value){
        super(value);
    }

    public static ProyectoId desde(final UUID proyectoId){
        return new ProyectoId(proyectoId);
    }

    public static ProyectoId desde(final String rawId){
        if (rawId == null || rawId.isEmpty()) {
            return null;
        }

        UUID id = UUID.fromString(rawId);
        return new ProyectoId(id);
    }
}
