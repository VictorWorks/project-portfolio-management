package com.victorworks.projectportfoliomanagement.domain.ports.excepcion;

public class NombreUsuarioNoExisteException extends Throwable {
    public NombreUsuarioNoExisteException(String mensaje) {
        super(mensaje);
    }
}
