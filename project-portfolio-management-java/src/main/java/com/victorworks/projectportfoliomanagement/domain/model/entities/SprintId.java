package com.victorworks.projectportfoliomanagement.domain.model.entities;

import java.util.UUID;

public class SprintId extends EntidadId{

    public static SprintId desde(final UUID sprintId){
        return new SprintId(sprintId);
    }

    protected SprintId(UUID value) {
        super(value);
    }

    public static SprintId desde(final String rawId){
        if (rawId == null || rawId.isEmpty()) {
            return null;
        }

        UUID id = UUID.fromString(rawId);
        return new SprintId(id);
    }
}
