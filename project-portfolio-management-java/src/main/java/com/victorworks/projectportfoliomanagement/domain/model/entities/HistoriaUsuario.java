package com.victorworks.projectportfoliomanagement.domain.model.entities;

import java.util.Date;

public class HistoriaUsuario {
    private HistoriaUsuarioId id;
    private String nombre;
    private String descripcion;
    private int estimacion;
    private int prioridad;
    private ProyectoId proyectoId;
    private EstadoTarea estadoActual;
    private Date fechaAlta;

    public HistoriaUsuario(HistoriaUsuarioId id, String nombre, String descripcion,
                           int estimacion, int prioridad,
                           ProyectoId proyectoId, EstadoTarea estadoActual, Date fechaAlta) {
        this.descripcion = descripcion;
        this.nombre = nombre;
        this.id = id;
        this.estimacion = estimacion;
        this.proyectoId = proyectoId;
        this.prioridad = prioridad;
        this.estadoActual = estadoActual;
        this.fechaAlta = fechaAlta;
    }

    public HistoriaUsuarioId getId() {
        return id;
    }

    public void setId(HistoriaUsuarioId id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getEstimacion() {
        return estimacion;
    }

    public void setEstimacion(int estimacion) {
        this.estimacion = estimacion;
    }

    public int getPrioridad() {
        return prioridad;
    }

    public void setPrioridad(int prioridad) {
        this.prioridad = prioridad;
    }

    public ProyectoId getProyectoId() {
        return proyectoId;
    }

    public void setProyectoId(ProyectoId proyectoId) {
        this.proyectoId = proyectoId;
    }

    public EstadoTarea getEstadoActual() { return this.estadoActual; }

    public void setEstadoActual(EstadoTarea estadoActual) { this.estadoActual = estadoActual; }

    public Date getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Date fechaAlta) {
        this.fechaAlta = fechaAlta;
    }
}
