package com.victorworks.projectportfoliomanagement.domain.model.entities;

import java.util.ArrayList;
import java.util.List;

public class ColumnaTablero {
    private ProyectoId proyectoId;
    private String nombre;
    private boolean esInicial;
    private boolean esFinal;
    private int orden;
    List<HistoriaUsuario> tareas;

    public ColumnaTablero(ProyectoId proyectoId, String nombre, boolean esInicial,
                          boolean esFinal, int orden) {
        this.proyectoId = proyectoId;
        this.nombre = nombre;
        this.esInicial = esInicial;
        this.esFinal = esFinal;
        this.orden = orden;
        this.tareas = new ArrayList<>();
    }

    public ProyectoId getProyectoId() {
        return proyectoId;
    }

    public void setProyectoId(ProyectoId proyectoId) {
        this.proyectoId = proyectoId;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public boolean getEsInicial() {
        return esInicial;
    }

    public void setEsInicial(boolean esInicial) {
        this.esInicial = esInicial;
    }

    public boolean getEsFinal() {
        return esFinal;
    }

    public void setEsFinal(boolean esFinal) {
        this.esFinal = esFinal;
    }

    public int getOrden() {
        return orden;
    }

    public void setOrden(int orden) {
        this.orden = orden;
    }

    public List<HistoriaUsuario> getTareas() {
        return tareas;
    }

    public void setTareas(List<HistoriaUsuario> tareas) {
        this.tareas = tareas;
    }

}
