package com.victorworks.projectportfoliomanagement.domain.model.entities;

import lombok.EqualsAndHashCode;

import java.util.UUID;


@EqualsAndHashCode
public class EntidadId {
    private final UUID value;

    protected EntidadId(final UUID value){
        this.value = value;
    }

    public static EntidadId desde(final UUID entidadId){
        return new EntidadId(entidadId);
    }

    public UUID getValue() {
        return value;
    }

    public String toString() {
        return value.toString();
    }
}
