package com.victorworks.projectportfoliomanagement.domain.ports.excepcion;

public class UsuarioExisteException extends Throwable{
    public UsuarioExisteException(String mensaje){
        super(mensaje);
    }
}
