package com.victorworks.projectportfoliomanagement.domain.ports.servicio;

import com.victorworks.projectportfoliomanagement.domain.model.entities.*;
import com.victorworks.projectportfoliomanagement.domain.model.useCases.CalculadoraBurnup;
import com.victorworks.projectportfoliomanagement.domain.ports.repositorio.HistoriaUsuarioRepository;
import com.victorworks.projectportfoliomanagement.domain.ports.repositorio.SprintRepository;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ReportesService {
    private final SprintRepository sprintRepository;
    private final HistoriaUsuarioRepository historiaUsuarioRepository;

    public ReportesService(SprintRepository sprintRepository, HistoriaUsuarioRepository historiaUsuarioRepository) {
        this.historiaUsuarioRepository = historiaUsuarioRepository;
        this.sprintRepository = sprintRepository;
    }

    public List<DatoBurnUp> obtenerDatosBurnup(ProyectoId proyectoId) {
        List<Sprint> sprints = sprintRepository.porProyecto(proyectoId);
        List<HistoriaUsuario> historias = historiaUsuarioRepository.porProyecto(proyectoId);

        List<HistoriaEnSprint> historiasEnSprint = new ArrayList<>();
        for (Sprint s: sprints) {
            historiasEnSprint.addAll(sprintRepository.obtenerHistorasEnSprintParaSprints(s.getId()));
        }

        CalculadoraBurnup calculator = new CalculadoraBurnup(sprints, historias, historiasEnSprint);
        return calculator.obtenerDatosBurnUp();
    }
}
