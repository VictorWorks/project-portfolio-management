package com.victorworks.projectportfoliomanagement.domain.model.entities;

public class DatoBurnUp {
    private String nombreSprint;
    private float puntosHistoriasCompletados;
    private float puntosHistoriaTotales;

    public String getNombreSprint() {
        return nombreSprint;
    }

    public void setNombreSprint(String nombreSprint) {
        this.nombreSprint = nombreSprint;
    }

    public float getPuntosHistoriasCompletados() {
        return puntosHistoriasCompletados;
    }

    public void setPuntosHistoriasCompletados(float puntosHistoriasCompletados) {
        this.puntosHistoriasCompletados = puntosHistoriasCompletados;
    }

    public float getPuntosHistoriaTotales() {
        return puntosHistoriaTotales;
    }

    public void setPuntosHistoriaTotales(float puntosHistoriaTotales) {
        this.puntosHistoriaTotales = puntosHistoriaTotales;
    }

    public DatoBurnUp(String nombreSprint, float puntosHistoriasCompletados, float puntosHistoriaTotales) {
        this.nombreSprint = nombreSprint;
        this.puntosHistoriasCompletados = puntosHistoriasCompletados;
        this.puntosHistoriaTotales = puntosHistoriaTotales;
    }
}
