package com.victorworks.projectportfoliomanagement.domain.model.useCases;

import com.victorworks.projectportfoliomanagement.domain.model.entities.*;
import com.victorworks.projectportfoliomanagement.utils.Dates;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class CalculadoraBurndown {
    private Sprint sprint;
    private List<HistoriaUsuario> historias;
    private List<HistoriaEnSprint> historiasEnSprint;

    public CalculadoraBurndown(Sprint sprint, List<HistoriaUsuario> historias, List<HistoriaEnSprint> historiaEnSprints) {
        this.sprint = sprint;
        this.historias = historias;
        this.historiasEnSprint = historiaEnSprints;
    }

    public ReporteBurndown calcular() {
        List<ResumenHistoriaUsuarioSprint> resumen = obtenerResumenDatos();
        int horasTotales = obtenerPuntosTotalesSprint(resumen);
        List<PuntoBurndown> listaPuntos = obtenerListaPuntosBurndown(resumen, horasTotales);
        return crearReporteBurndown(horasTotales, listaPuntos);
    }

    private ReporteBurndown crearReporteBurndown(int horasTotales, List<PuntoBurndown> listaPuntos) {
        ReporteBurndown resultado = new ReporteBurndown(obtenerFechaInicialBurndown(),this.sprint.getFechaFin(),
                horasTotales);
        resultado.setProgreso(listaPuntos);
        return resultado;
    }

    private int obtenerPuntosTotalesSprint(List<ResumenHistoriaUsuarioSprint> resumen) {
        return resumen.stream()
                .map(x -> x.getPuntosHistoria())
                .reduce(0, (x, y) -> x + y);
    }

    private List<PuntoBurndown> obtenerListaPuntosBurndown(List<ResumenHistoriaUsuarioSprint> resumen, int horasTotales) {
        List<PuntoBurndown> listaPuntos = new ArrayList<>();
        Date fechaActual = obtenerFechaInicialBurndown();
        Date fechaFinal = obtenerFechaFinalBurndown();
        int pendienteActual = horasTotales;

        while (fechaActual.before(fechaFinal)) {
            pendienteActual -= obtenerPuntosFinalizadosEnFecha(resumen, fechaActual);
            añadirPuntoBurndown(listaPuntos, fechaActual, pendienteActual);

            fechaActual = obtenerDiaSiguiente(fechaActual);
        }

        return listaPuntos;
    }

    private Date obtenerFechaInicialBurndown() {
        Date inicialTeorica = sprint.getFechaInicio();
        return sumarDiasAFecha(inicialTeorica, -1);
    }

    private Date obtenerFechaFinalBurndown() {
        Date finSprint = this.sprint.getFechaFin();
        Date hoy = Calendar.getInstance().getTime();
        if (hoy.before(finSprint))
            return hoy;

        return  finSprint;
    }

    private Date obtenerDiaSiguiente(Date fecha) {
        return sumarDiasAFecha(fecha, 1);
    }

    private Date sumarDiasAFecha(Date fecha, int dias) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(fecha);
        calendar.add(Calendar.DATE, dias);
        return calendar.getTime();
    }

    private void añadirPuntoBurndown(List<PuntoBurndown> lista, Date fechaActual, int puntosPendientes) {
        PuntoBurndown puntoActual = new PuntoBurndown(fechaActual, puntosPendientes);
        lista.add(puntoActual);
    }

    private List<ResumenHistoriaUsuarioSprint> obtenerResumenDatos() {
        List<ResumenHistoriaUsuarioSprint> historiaEnSprints = new ArrayList<>();
        for (HistoriaUsuario historia : historias) {
            HistoriaUsuarioId historiaId = historia.getId();
            HistoriaEnSprint historiaEnSprint = this.historiasEnSprint.stream()
                    .filter(x -> tieneMismaHistoriaId(x.getHistoriaUsuario().getId(), historiaId))
                    .findFirst()
                    .orElse(null);
            historiaEnSprints.add(new ResumenHistoriaUsuarioSprint(historia, historiaEnSprint));
        }

        return historiaEnSprints.stream()
                .sorted((x, y) -> ordenarPorFechas(x, y))
                .collect(Collectors.toList());
    }

    private int ordenarPorFechas(ResumenHistoriaUsuarioSprint primerItem, ResumenHistoriaUsuarioSprint segundoItem) {
        Date fechaPrimerItem = obtenerFechaFin(primerItem);
        Date fechaSegundoItem = obtenerFechaFin(segundoItem);
        return fechaPrimerItem.compareTo(fechaSegundoItem);
    }

    private Date obtenerFechaFin(ResumenHistoriaUsuarioSprint elem) {
        if (elem.getFechaFinalizacion() == null)
            return sprint.getFechaFin();

        return elem.getFechaFinalizacion();
    }

    private boolean tieneMismaHistoriaId(HistoriaUsuarioId primera, HistoriaUsuarioId segunda) {
        return primera.equals(segunda);
    }

    private int obtenerPuntosFinalizadosEnFecha(List<ResumenHistoriaUsuarioSprint> resumen, Date fechaActual) {
        return resumen.stream()
                .filter(x -> x.getEstadoTarea() == EstadoTarea.Terminado && tareaTerminaElDia(x, fechaActual))
                .map(y -> y.getPuntosHistoria())
                .reduce(0, (x, y) -> x + y);
    }

    private boolean tareaTerminaElDia(ResumenHistoriaUsuarioSprint tarea, Date fecha) {
        return Dates.esMismaFecha(fecha, tarea.getFechaFinalizacion());
    }

    private class ResumenHistoriaUsuarioSprint {
        private HistoriaUsuarioId id;
        private int puntosHistoria;
        private EstadoTarea estadoTarea;
        private Date fechaFinalizacion;

        public HistoriaUsuarioId getId() {
            return id;
        }

        public int getPuntosHistoria() {
            return puntosHistoria;
        }

        public EstadoTarea getEstadoTarea() {
            return estadoTarea;
        }

        public Date getFechaFinalizacion() {
            return fechaFinalizacion;
        }

        public ResumenHistoriaUsuarioSprint(HistoriaUsuario historia, HistoriaEnSprint historiaEnSprint) {
            this.id = historia.getId();
            this.puntosHistoria = historia.getEstimacion();
            this.estadoTarea = historiaEnSprint.getEstado();
            this.fechaFinalizacion = historiaEnSprint.getFechaModificacion();
        }
    }
}
