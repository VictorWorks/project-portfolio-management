package com.victorworks.projectportfoliomanagement.domain.model.entities;

import java.util.UUID;

public class HistoriaUsuarioId extends EntidadId{
    private HistoriaUsuarioId(final UUID value){
        super(value);
    }

    public static HistoriaUsuarioId desde(final UUID historiaId){
        return new HistoriaUsuarioId(historiaId);
    }

    public static HistoriaUsuarioId desde(final String rawId){
        UUID id = UUID.fromString(rawId);
        return new HistoriaUsuarioId(id);
    }
}
