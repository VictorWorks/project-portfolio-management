package com.victorworks.projectportfoliomanagement.domain.ports.repositorio;

import com.victorworks.projectportfoliomanagement.domain.model.entities.Persona;
import com.victorworks.projectportfoliomanagement.domain.model.entities.PersonaId;
import com.victorworks.projectportfoliomanagement.domain.model.entities.ProyectoId;

import java.util.List;

public interface PersonaRepository {

    Persona obtenerPorId(PersonaId id);

    Persona obtenerPorNombreUsuario(String nombreUsuario);

    Persona crear(Persona persona);

    void actualizar(PersonaId personaId, Persona persona);

    void borrar(PersonaId personaId);

    List<Persona> todos();

    List<Persona> porProyecto(ProyectoId proyectoId);

    List<Persona> noIncluidosEnProyectoYNombreComo(ProyectoId proyectoId, String claveNombre);
}
