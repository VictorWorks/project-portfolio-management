package com.victorworks.projectportfoliomanagement.domain.model.entities;

public class Proyecto {
    public ProyectoId getId() {
        return id;
    }

    public void setId(ProyectoId id) {
        this.id = id;
    }

    private ProyectoId id;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    private String nombre;

    public Proyecto(ProyectoId id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }
}
