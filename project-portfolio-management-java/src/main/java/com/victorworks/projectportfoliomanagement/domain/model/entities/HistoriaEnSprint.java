package com.victorworks.projectportfoliomanagement.domain.model.entities;

import java.util.Date;

public class HistoriaEnSprint {
    private Sprint sprint;
    private HistoriaUsuario historiaUsuario;
    private EstadoTarea estado;
    private Date fechaModificacion;

    public HistoriaEnSprint(Sprint sprint, HistoriaUsuario historiaUsuario,
                            EstadoTarea estado, Date fechaModificacion) {
        this.sprint = sprint;
        this.historiaUsuario = historiaUsuario;
        this.fechaModificacion = fechaModificacion;

        // No permitir generar un objeto en un estado inválido
        if (estado == null)
            this.estado = EstadoTarea.ALaEspera;
        else
            this.estado = estado;
    }

    public Sprint getSprint() {
        return sprint;
    }

    public void setSprint(Sprint sprint) {
        this.sprint = sprint;
    }

    public HistoriaUsuario getHistoriaUsuario() {
        return historiaUsuario;
    }

    public void setHistoriaUsuario(HistoriaUsuario historiaUsuario) {
        this.historiaUsuario = historiaUsuario;
    }

    public EstadoTarea getEstado() {
        return estado;
    }

    public void setEstado(EstadoTarea estado) {
        this.estado = estado;
    }

    public  Date getFechaModificacion() {
        return this.fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }
}
