package com.victorworks.projectportfoliomanagement.domain.ports.servicio;

import com.victorworks.projectportfoliomanagement.domain.model.entities.Persona;
import com.victorworks.projectportfoliomanagement.domain.ports.repositorio.PersonaRepository;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class AutenticacionService implements AuthenticationManager {
    private final PersonaRepository personas;
    private final BCryptPasswordEncoder encoder;

    public AutenticacionService(PersonaRepository personas, BCryptPasswordEncoder encoder) {
        this.personas = personas;
        this.encoder = encoder;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String username = (String) authentication.getPrincipal();
        String password = (String) authentication.getCredentials();

        Persona persona = personas.obtenerPorNombreUsuario(username);
        if (persona == null){
            throw new BadCredentialsException("No existe el nombre de usuario " + username);
        }

        if (!encoder.matches(password, persona.getPassword())) {
            throw new BadCredentialsException("Contraseña no válida");
        }

        List<GrantedAuthority> authorities = Arrays.stream(persona.getRol().getPermisos())
                .map(SimpleGrantedAuthority::new)
                .collect(Collectors.toList());

        return new UsernamePasswordAuthenticationToken(username,null, authorities);
    }
}
