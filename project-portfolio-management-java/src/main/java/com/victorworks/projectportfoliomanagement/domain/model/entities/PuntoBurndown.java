package com.victorworks.projectportfoliomanagement.domain.model.entities;

import java.util.Date;

public class PuntoBurndown {
    private Date fecha;
    private int puntosFinalizados;

    public PuntoBurndown(Date fecha, int puntosFinalizados) {
        this.fecha = fecha;
        this.puntosFinalizados = puntosFinalizados;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public int getPuntosFinalizados() {
        return puntosFinalizados;
    }

    public void setPuntosFinalizados(int puntosFinalizados) {
        this.puntosFinalizados = puntosFinalizados;
    }
}
