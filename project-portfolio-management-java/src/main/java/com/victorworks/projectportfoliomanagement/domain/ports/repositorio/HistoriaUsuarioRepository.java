package com.victorworks.projectportfoliomanagement.domain.ports.repositorio;


import com.victorworks.projectportfoliomanagement.domain.model.entities.HistoriaUsuario;
import com.victorworks.projectportfoliomanagement.domain.model.entities.HistoriaUsuarioId;
import com.victorworks.projectportfoliomanagement.domain.model.entities.ProyectoId;
import com.victorworks.projectportfoliomanagement.domain.model.entities.SprintId;

import java.util.List;

public interface HistoriaUsuarioRepository {
    List<HistoriaUsuario> todos();

    List<HistoriaUsuario> porProyecto(ProyectoId proyectoId);

    List<HistoriaUsuario> porSprint(SprintId sprintId);

    List<HistoriaUsuario> noIncluidosEnSprintYNombreComo(SprintId sprintId, String claveNombre);

    HistoriaUsuario obtenerPorId(HistoriaUsuarioId id);

    HistoriaUsuario crear(HistoriaUsuario historia);

    void actualizar(HistoriaUsuarioId historiaId, HistoriaUsuario historia);

    void borrar(HistoriaUsuarioId historiaId);
}
