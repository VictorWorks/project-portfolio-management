package com.victorworks.projectportfoliomanagement.domain.model.entities;

public enum EstadoTarea {
    ALaEspera {
        public String toString() {
            return "A la espera";
        }
    },
    EnDesarrollo {
        public  String toString() {
            return "En desarrollo";
        }
    },
    QA {
        public String toString() {
            return "QA";
        }
    },
    Terminado {
        public String toString() {
            return "Terminado";
        }
    };

    public static EstadoTarea obtenerEstadoDesde(String estado) {
        EstadoTarea resultado = EstadoTarea.ALaEspera;
        if ( EstadoTarea.EnDesarrollo.toString().equals(estado))
            resultado = EstadoTarea.EnDesarrollo;

        if (EstadoTarea.QA.toString().equals(estado))
            resultado = EstadoTarea.QA;

        if (EstadoTarea.Terminado.toString().equals(estado))
            resultado = EstadoTarea.Terminado;

        return resultado;
    }
}
