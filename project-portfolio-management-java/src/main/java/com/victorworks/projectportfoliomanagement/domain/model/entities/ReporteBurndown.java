package com.victorworks.projectportfoliomanagement.domain.model.entities;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ReporteBurndown {
    private Date fechaInicio;
    private Date fechaFin;
    private int puntosHistoria;
    private List<PuntoBurndown> progreso;

    public  ReporteBurndown(Date fechaInicio, Date fechaFin, int puntosHistoria) {
        this.fechaInicio = fechaInicio;
        this.fechaFin = fechaFin;
        this.puntosHistoria = puntosHistoria;
        this.progreso = new ArrayList<>();
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public int getPuntosHistoria() {
        return puntosHistoria;
    }

    public void setPuntosHistoria(int puntosHistoria) {
        this.puntosHistoria = puntosHistoria;
    }

    public List<PuntoBurndown> getProgreso() {
        return progreso;
    }

    public void setProgreso(List<PuntoBurndown> progreso) {
        this.progreso = progreso;
    }
}
