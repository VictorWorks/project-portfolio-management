package com.victorworks.projectportfoliomanagement.domain.model.entities;

import java.util.UUID;

public class TareaId extends EntidadId{
    public static TareaId desde(final UUID tareaId){
        return new TareaId(tareaId);
    }

    protected TareaId(UUID value) {
        super(value);
    }

    public static TareaId desde(final String rawId){
        if (rawId == null || rawId.isEmpty()) {
            return null;
        }

        UUID id = UUID.fromString(rawId);
        return new TareaId(id);
    }
}
