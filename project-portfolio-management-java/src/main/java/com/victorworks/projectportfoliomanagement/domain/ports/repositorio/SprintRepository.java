package com.victorworks.projectportfoliomanagement.domain.ports.repositorio;

import com.victorworks.projectportfoliomanagement.domain.model.entities.*;

import java.util.List;

public interface SprintRepository {
    Sprint obtenerPorId(SprintId id);

    Sprint crear(Sprint sprint);

    void actualizar(SprintId sprintId, Sprint sprint);

    void añadirHistoriaASprint(HistoriaEnSprint historia);

    void eliminarHistoriaDeSprint(HistoriaEnSprint historia);

    void borrar(SprintId sprintId);

    List<Sprint> todos();

    List<Sprint> porProyecto(ProyectoId proyectoId);

    List<HistoriaEnSprint> historiasEnSprintParaSprint(SprintId sprintId);

    HistoriaEnSprint historiaEnSprintParaHistoria(SprintId sprintId, HistoriaUsuarioId historiaId);

    List<HistoriaUsuario> historiasUsuarioParaSprint(SprintId sprintId);

    List<HistoriaEnSprint> obtenerHistorasEnSprintParaSprints(SprintId sprintId);
}
