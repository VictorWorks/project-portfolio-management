package com.victorworks.projectportfoliomanagement.domain.ports.servicio;

import com.victorworks.projectportfoliomanagement.domain.model.entities.HistoriaUsuario;
import com.victorworks.projectportfoliomanagement.domain.model.entities.HistoriaUsuarioId;
import com.victorworks.projectportfoliomanagement.domain.model.entities.ProyectoId;
import com.victorworks.projectportfoliomanagement.domain.model.entities.SprintId;
import com.victorworks.projectportfoliomanagement.domain.ports.repositorio.HistoriaUsuarioRepository;
import org.springframework.stereotype.Component;


import java.util.List;

@Component
public class HistoriaUsuarioService {
    private final HistoriaUsuarioRepository repository;

    public HistoriaUsuarioService(HistoriaUsuarioRepository repository) {
        this.repository = repository;
    }

    public List<HistoriaUsuario> todos() {
        return this.repository.todos();
    }

    public HistoriaUsuario obtenerPorId(HistoriaUsuarioId historiaId) {
        return repository.obtenerPorId(historiaId);
    }

    public List<HistoriaUsuario> obtenerPorProyecto(ProyectoId proyectoId) {
        return repository.porProyecto(proyectoId);
    }

    public List<HistoriaUsuario> obtenerHistoriaNoIncluidaEnSprintConNombreComo(String claveNombre, SprintId sprintId) {
        return repository.noIncluidosEnSprintYNombreComo(sprintId, claveNombre);
    }

    public  List<HistoriaUsuario> obtenerPorSprint(SprintId sprintId) {
        return repository.porSprint(sprintId);
    }
    public HistoriaUsuario crear(HistoriaUsuario historiaUsuario) {
        return repository.crear(historiaUsuario);
    }

    public void actualizar(HistoriaUsuarioId historiaId, HistoriaUsuario historiaUsuario) {
        repository.actualizar(historiaId, historiaUsuario);
    }

    public void borrar(HistoriaUsuarioId historiaId) {
        repository.borrar(historiaId);
    }
}
