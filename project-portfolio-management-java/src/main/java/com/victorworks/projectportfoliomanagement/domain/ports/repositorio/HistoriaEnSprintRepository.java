package com.victorworks.projectportfoliomanagement.domain.ports.repositorio;

import com.victorworks.projectportfoliomanagement.domain.model.entities.HistoriaEnSprint;

public interface HistoriaEnSprintRepository {
    public void actualizar(HistoriaEnSprint historiaEnSprint);
}
