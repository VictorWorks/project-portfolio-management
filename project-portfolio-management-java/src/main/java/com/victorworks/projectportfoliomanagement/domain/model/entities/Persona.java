package com.victorworks.projectportfoliomanagement.domain.model.entities;

public class Persona {
    public PersonaId getId() {
        return id;
    }

    public void setId(PersonaId id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    PersonaId id;
    String nombre;
    ProyectoId proyectoId;
    String username;
    String password;
    RolUsuario rol;

    public ProyectoId getProyectoId() {
        return proyectoId;
    }

    public void setProyectoId(ProyectoId proyectoId) {
        this.proyectoId = proyectoId;
    }

    public Persona() {}

    public Persona(final PersonaId id, final String nombre, String nombreUsuario, String password){
        this.id = id;
        this.nombre = nombre;
        this.username = nombreUsuario;
        this.password = password;
    }
    public Persona(final PersonaId id, final String nombre, String username, String password, RolUsuario rol, ProyectoId proyectoId){
        this.id = id;
        this.nombre = nombre;
        this.username = username;
        this.password = password;
        this.rol = rol;
        this.proyectoId = proyectoId;
    }

    public RolUsuario getRol() {
        return rol;
    }

    public void setRol(RolUsuario rol) {
        this.rol = rol;
    }
    
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
