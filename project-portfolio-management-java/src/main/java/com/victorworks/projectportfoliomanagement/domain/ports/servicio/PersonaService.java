package com.victorworks.projectportfoliomanagement.domain.ports.servicio;

import com.victorworks.projectportfoliomanagement.adapters.security.entidad.UserPrincipal;
import com.victorworks.projectportfoliomanagement.domain.model.entities.Persona;
import com.victorworks.projectportfoliomanagement.domain.model.entities.PersonaId;
import com.victorworks.projectportfoliomanagement.domain.model.entities.ProyectoId;
import com.victorworks.projectportfoliomanagement.domain.model.entities.RolUsuario;
import com.victorworks.projectportfoliomanagement.domain.ports.repositorio.PersonaRepository;
import com.victorworks.projectportfoliomanagement.domain.ports.excepcion.NombreUsuarioCambiadoException;
import com.victorworks.projectportfoliomanagement.domain.ports.excepcion.UsuarioExisteException;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Qualifier("UserDetailService")
public class PersonaService implements UserDetailsService {

    private final PersonaRepository repository;
    private final BCryptPasswordEncoder encoder;

    public PersonaService(PersonaRepository repository, BCryptPasswordEncoder encoder) {
        this.repository = repository;
        this.encoder = encoder;
    }

    public List<Persona> todos() {
        return this.repository.todos();
    }

    public Persona obtenerPorId(PersonaId personaId) {
        return repository.obtenerPorId(personaId);
    }

    public Persona obtenerPorNombreUsuario(String nombreUsuario) {
        return repository.obtenerPorNombreUsuario(nombreUsuario);
    }

    public Persona crear(Persona persona) throws UsuarioExisteException {
        validarAltaPersona(persona);
        persona.setPassword(encoder.encode(persona.getPassword()));
        return repository.crear(persona);
    }

    private void validarAltaPersona(Persona persona) throws  UsuarioExisteException {
        if (repository.obtenerPorNombreUsuario(persona.getUsername()) != null)
            throw new UsuarioExisteException("Ya existe un usuario con nombre de usuario" + persona.getUsername());
    }

    public Persona registrar(Persona persona) throws  UsuarioExisteException {
        persona.setRol(RolUsuario.DESARROLLADOR);
        persona.setPassword(encoder.encode(persona.getPassword()));
        return crear(persona);
    }

    public void actualizar(PersonaId personaId, Persona persona) throws NombreUsuarioCambiadoException {
        validarActualizacionPersona(persona);
        repository.actualizar(personaId, persona);
    }

    private void validarActualizacionPersona(Persona persona) throws NombreUsuarioCambiadoException {
        Persona encontrada = repository.obtenerPorId(persona.getId());
        if (encontrada == null)
            return;

        if (!persona.getUsername().equals(encontrada.getUsername()))
            throw new NombreUsuarioCambiadoException("No se permiten cambios de nombre de usuario");
    }

    public void borrar(PersonaId personaId) {
        repository.borrar(personaId);
    }

    public List<Persona> obtenerEquipoProyecto(ProyectoId proyectoId){
        return repository.porProyecto(proyectoId);
    }

    public List<Persona> obtenerDesasignados()
    {
        return repository.porProyecto((null));
    }

    public List<Persona> obtenerPersonalNoIncluidoEnProyectoFiltradoPorNombre(String claveNombre, ProyectoId proyectoId) {
        return repository.noIncluidosEnProyectoYNombreComo(proyectoId, claveNombre);
    }

    @Override
    public UserDetails loadUserByUsername(String nombreUsuario) throws UsernameNotFoundException {
        Persona p = repository.obtenerPorNombreUsuario(nombreUsuario);
        if (p == null) {
            throw new UsernameNotFoundException("No se puede encontrar el usuario" + nombreUsuario);
        }
        UserPrincipal userPrincipal = new UserPrincipal(p);
        return userPrincipal;
    }
}
