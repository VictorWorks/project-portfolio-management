package com.victorworks.projectportfoliomanagement.domain.model.constante;

public class ConstantesPermisos {
    // Permisos de usuario
    public static final String PERMISO_VER_PROYECTO = "proyecto:ver";
    public static final String PERMISO_AÑADIR_PROYECTO = "proyecto:añadir";
    public static final String PERMISO_BORRAR_PROYECTO = "proyecto:borrar";

    public static final String PERMISO_VER_PERSONAL = "personal:ver";
    public static final String PERMISO_AÑADIR_PERSONAL = "personal:añadir";
    public static final String PERMISO_BORRAR_PERSONAL = "personal:borrar";

    public static final String[] PERMISOS_ADMINISTRADOR = {PERMISO_VER_PROYECTO, PERMISO_AÑADIR_PROYECTO, PERMISO_BORRAR_PROYECTO,
            PERMISO_VER_PERSONAL, PERMISO_AÑADIR_PERSONAL, PERMISO_BORRAR_PERSONAL};
    public static final String[] PERMISOS_RRHH = {PERMISO_VER_PERSONAL, PERMISO_AÑADIR_PERSONAL, PERMISO_BORRAR_PERSONAL};
    public static final String[] PERMISOS_MANAGER = {PERMISO_VER_PROYECTO, PERMISO_AÑADIR_PROYECTO, PERMISO_BORRAR_PROYECTO,
            PERMISO_VER_PERSONAL};
    public static final String[] PERMISOS_DESARROLLADOR = {PERMISO_VER_PROYECTO};
}
