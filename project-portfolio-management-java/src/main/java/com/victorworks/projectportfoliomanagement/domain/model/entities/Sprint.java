package com.victorworks.projectportfoliomanagement.domain.model.entities;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class Sprint {
    private  SprintId id;
    private  ProyectoId proyectoId;

    public SprintId getId() {
        return id;
    }
    private String nombre;
    private Date fechaInicio;
    private Date fechaFin;
    private int puntosHistoria;

    private Set<HistoriaEnSprint> historias = new HashSet<>();

    public Sprint(SprintId sprintId, String nombre, Date fechaInicio, Date fechaFin, int puntosHistoria, ProyectoId proyectoId) {
        this.id = sprintId;
        this.nombre = nombre;
        this.fechaFin = fechaFin;
        this.fechaInicio = fechaInicio;
        this.puntosHistoria  = puntosHistoria;
        this.proyectoId = proyectoId;
    }

    public void setId(SprintId id) {
        this.id = id;
    }

    public ProyectoId getProyectoId() {
        return proyectoId;
    }

    public void setProyectoId(ProyectoId proyectoId) {
        this.proyectoId = proyectoId;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public int getPuntosHistoria() {
        return puntosHistoria;
    }

    public void setPuntosHistoria(int puntosHistoria) {
        this.puntosHistoria = puntosHistoria;
    }

    public Set<HistoriaEnSprint> getHistoriasEnSprint() {
        return this.historias;
    }

    public void agregarHistoriaASprint(HistoriaEnSprint historia){
        this.historias.add(historia);
    }

}
