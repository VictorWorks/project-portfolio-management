package com.victorworks.projectportfoliomanagement.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Dates {
    private Dates(){ }
    public static String formatearFecha(Date d) {
        String pattern = "yyyy-MM-dd";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        return simpleDateFormat.format(d);
    }

    public static Date parseFecha(String f) {
        String pattern = "yyyy-MM-dd";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        try {
            return simpleDateFormat.parse(f);
        } catch (Exception ex) {
            return new Date();
        }
    }

    public static boolean esMismaFecha(Date fecha1, Date fecha2) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        return dateFormat.format(fecha1).equals(
          dateFormat.format(fecha2)
        );
    }
}
