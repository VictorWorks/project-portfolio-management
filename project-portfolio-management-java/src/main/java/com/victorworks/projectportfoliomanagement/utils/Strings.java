package com.victorworks.projectportfoliomanagement.utils;

public class Strings {
    public static boolean esCadenaVacia(String cadena) {
        return cadena == null || cadena.length() == 0;
    }
}
